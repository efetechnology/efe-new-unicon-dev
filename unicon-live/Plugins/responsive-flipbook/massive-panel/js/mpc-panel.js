/*----------------------------------------------------------------------------*\
	SYSTEM INFO
\*----------------------------------------------------------------------------*/

( function( $ ) {
	"use strict";

	var $show      = $( '#rfbwp_panel__show_info' ),
		$info_wrap = $( '#rfbwp_panel__system_wrap' ),
		$info_file = $( '#rfbwp_panel__info_file' ),
		$info_text = $info_wrap.find( 'textarea' ),
		_wpnonce   = $( '#_wpnonce' ).val(),
		_info      = $info_text.val();

	$show.on( 'click', function() {
		$info_wrap.css( 'max-height', 250 );

		setTimeout( function() {
			$info_wrap.css( 'max-height', '' );
		}, 250 );
	} );

	$info_file.on( 'click', function() {
		location.href = ajaxurl + '?action=rfbwp_export_info&_wpnonce=' + _wpnonce + '&system_info=' + escape( _info );
	} );

	$info_text.on( 'click', function() {
		$info_text.select();
	} );
} )( jQuery );


/*----------------------------------------------------------------------------*\
	Theme Cooperation Settings
\*----------------------------------------------------------------------------*/

window.rfbwp = {
	_is_working:      false,
	_wpnonce:         jQuery( '#_wpnonce' ).val()
};

window.rfbwp.show_error = function() {
	var $error = jQuery( '#rfbwp_panel__error' );

	$error.addClass( 'rfbwp-visible' );
	setTimeout( function() {
		$error.removeClass( 'rfbwp-visible' );
	}, 3000 );
};

window.rfbwp.progress_button = function( $button, data, duration, type, $clear_button ) {
	var $progress = $button.find( '.rfbwp-progress' );

	if ( type == 'save' ) {
		var $inputs = data.options;
	}

	$button.on( 'click', function( event ) {
		event.preventDefault();
		//console.log(ajax_cooperation_page_vars.ajax_cooperation_page_ajax_url);
		if ( window.rfbwp._is_working ) {
			return;
		}

		if ( type == 'save' ) {
			data.options = $inputs.serialize();
		}
		//console.log(data.options);
		data._wpnonce = window.rfbwp._wpnonce;
		//console.log(data._wpnonce);
		window.rfbwp._is_working = true;
		$button
			.removeClass( 'rfbwp-finished' )
			.addClass( 'rfbwp-working' );

		$progress
			.stop( true )
			.animate( {
				width: '90%'
			}, duration, 'linear' );

		jQuery.ajax( {
			url:    ajax_cooperation_page_vars.ajax_cooperation_page_ajax_url,
			method: 'POST',
			data:   data
		} ).always( function( response ) {
			if ( ! response.success ) {
				window.rfbwp.show_error();
				console.log('AJAX error on save');
			}

			$progress
				.stop( true )
				.animate( {
					width: '100%'
				}, {
					complete: function() {
						setTimeout( function() {
							window.rfbwp._is_working = false;

							$progress.animate( {
								left: '100%'
							}, {
								complete: function() {
									if ( typeof $clear_button != 'undefined' ) {
										$clear_button.trigger( 'click' );
									}

									$button
										.removeClass( 'rfbwp-working' )
										.addClass( 'rfbwp-finished' );

									$progress.css( {
										width: 0,
										left: 0
									} );

									setTimeout( function() {
										$button.removeClass( 'rfbwp-finished' );
									}, 2500 );
								}
							} )
						}, 1000 );
					}
				} );
		} );
	} );
};

/*----------------------------------------------------------------------------*\
	PANEL
\*----------------------------------------------------------------------------*/

( function( $ ) {
	"use strict";

	var $panel        = $( '#rfbwp_panel' ),
		$panel_inputs = $panel.find( ':input' ),
		$save_panel   = $( '#rfbwp_panel__save' );

	window.rfbwp.progress_button( $save_panel, {
		action:  'save_panel',
		options: $panel_inputs
	}, 1000, 'save' );

} )( jQuery );