/*-----------------------------------------------------------------------------------*/
/*  MPC Visual Composer Flipbook VC Popup Functions
/*-----------------------------------------------------------------------------------*/
(function($) {   
    var $popup = $( '#vc_ui-panel-edit-element' );

    $popup.on( 'vcPanel.shown', function() {

        if( $popup.attr( 'data-vc-shortcode' ) != 'flipbook-popup' ) {
            return '';
        }

        var $popup_id = $popup.find( '[name="popup_id"]' ),
              $fb_id = $popup.find( '[name="id"]' );

        $popup_id.attr( 'readonly', 'readonly' );
        $popup_id.val( '#' + $fb_id.val() );

        $fb_id.on( 'change', function() {
            $popup_id.val( '#' + $fb_id.val() );
        } );
    });
})(jQuery);
/*---------------------------- VC Popup Functions --------------------------------- */