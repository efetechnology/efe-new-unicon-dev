<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/* REGISTER PAGE */
add_action( 'rfbwp/panelSubmenu', 'rfbwp_register_cooperation_page' );

if ( ! function_exists( 'rfbwp_register_cooperation_page' ) ) {
	function rfbwp_register_cooperation_page() {
		$hook = add_submenu_page( MP_PAGE_BASENAME, __( 'Responsive Flipbook Theme Cooperation', 'rfbwp' ),  __( 'Theme Cooperation Settings', 'rfbwp' ), 'rfbwp_plugin_cap', 'rfbwp_cooperation_page_options', 'rfbwp_panel_cooperation' );

		// Load System Info page styles/scripts
		add_action( 'load-' . $hook, 'rfbwp_register_system_page_scripts' );

		// Register Cooperation Page Settings
		add_action( 'admin_init', 'rfbwp_cooperation_page_options' );

	}
}

if ( ! function_exists( 'rfbwp_cooperation_page_options' ) ) {
	function rfbwp_cooperation_page_options() {
		register_setting( 'rfbwp_cooperation_page_options_group', 'theme_elements' );
	}
}

if ( ! function_exists( 'rfbwp_panel_cooperation' ) ) {
	function rfbwp_panel_cooperation() { 
			
			//settings_fields( 'rfbwp_cooperation_page_options_group' );
    		//do_settings_sections( 'rfbwp_cooperation_page_options_group' ); 
    		?>

			<div id="rfbwp_panel" class="rfbwp-panel">
				<header class="rfbwp-panel__header">
					<h1 class="rfbwp-panel__name">
						<?php _e( 'Theme Cooperation Settings', 'rfbwp' ); ?>
					</h1>
				</header>
				
				<div class="rfbwp-section rfbwp-section--theme-cooperation">
					<h2 class="rfbwp-section__title"><?php _e( 'Reset CSS z-index of theme elements, which are covering your Flipbook Pop-up, Bookshelf or Fullscreen mode', 'rfbwp' ); ?></h2>
					
					<div class="rfbwp-section__content">
						<div class="rfbwp-description">
							<p><?php _e( 'Implement below <a href="https://css-tricks.com/the-difference-between-id-and-class/" target="_blank">CSS <em>"ID\'s"</em> or <em>"Class\'es"</em></a> of elements ( <em>i.e. Sticky Header, floating social icons</em> ), which are covering your Flipbook. You can add multiple elements separated with <em>","</em> ( comma ).', 'rfbwp' ); ?></p>
						</div>
						<div class="rfbwp-option">
							<span class="rfbwp-option-name"><?php _e( 'Add selectors here:' , 'rfbwp' ); ?></span>
							<span class="rfbwp-hint">?<span class="rfbwp-hint-content"><span class="rfbwp-hint-triangle"></span><?php _e( 'In example: "#main-header, .header-holder, #main-wrapper .super-sticky-footer .holder" etc.', 'rfbwp' ); ?></span></span>
							<label class="rfbwp-option-state rfbwp-text-input">
								<input type="text" class="text rfbwp-theme_elements" id="theme_elements" name="theme_elements" value="<?php echo esc_attr( get_option('theme_elements') ); ?>">
							</label>
							<div class="rfbwp-description">
								<p><em><?php _e( 'Please notice those selectors needs to be chosen carefully. You can <a href="https://developers.google.com/web/tools/chrome-devtools/inspect-styles/" target="_blank">inspect them directly in browser</a> using "Dev Tools" - each modern browser has got it under F12 keybord key. If you need help/assistance, then please contact our support at <a class="mpc-button" target="_blank" href="http://mpc.ticksy.com">Ticksy<i class="dashicons dashicons-sos"></i></a>', 'rfbwp' ); ?></em></p>
							</div>

						</div>

					</div>
		
				</div>

				<!-- FOOTER -->
				<footer class="rfbwp-panel__footer">
					<a href="#save" id="rfbwp_panel__save" class="rfbwp-panel__save rfbwp-panel__primary">
						<span class="rfbwp-default"><?php _e( 'Save panel', 'rfbwp' ); ?></span>
						<span class="rfbwp-working"><?php _e( 'Saving...', 'rfbwp' ); ?></span>
						<span class="rfbwp-finished"><?php _e( 'Saved :)', 'rfbwp' ); ?></span>
						<span class="rfbwp-save__progress rfbwp-progress"></span>
					</a>
				</footer>

			</div>

			<?php wp_nonce_field( 'rfbwp-cooperation-page-panel' ); ?>

<?php }
}

/* AJAX - SAVE PANEL */
add_action( 'wp_ajax_save_panel', 'rfbwp_ajax_save_panel' );
if ( ! function_exists( 'rfbwp_ajax_save_panel' ) ) {
	function rfbwp_ajax_save_panel() {
		if ( ! isset( $_POST[ 'options' ] )  || ! isset( $_POST[ '_wpnonce' ] ) ) {
			wp_send_json_error();
		}

		check_ajax_referer( 'rfbwp-cooperation-page-panel' );
		$theme_elements_fields = array();
		
		parse_str( $_POST[ 'options' ], $theme_elements_fields );	
		update_option( 'theme_elements', $theme_elements_fields['theme_elements']  );

		wp_send_json_success();
	}
}
