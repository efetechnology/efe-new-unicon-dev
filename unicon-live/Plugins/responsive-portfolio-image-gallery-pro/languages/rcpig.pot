#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Responsive CSS3 Portfolio Image Gallery\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-02 17:15+0000\n"
"POT-Revision-Date: Sat Aug 13 2016 04:25:18 GMT+0600 (Central Asia Standard "
"Time)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco - https://localise.biz/"

#: rcpig-shortcode.php:154
msgid "No portfolio found!"
msgstr ""

#: rcpig-shortcode.php:156
msgid "You didn't add any portfolio gallery yet!"
msgstr ""

#: rcpig-shortcode.php:158
msgid "Please <strong>Enable</strong> portfolio gallery to display portfolios!"
msgstr ""

#: responsive-gallery.php:27
msgid "Settings"
msgstr ""

#: responsive-gallery.php:28
msgid "Support"
msgstr ""

#: custom-post/rcpig-post.php:9
msgctxt "Post Type General Name"
msgid "Portfolios"
msgstr ""

#: custom-post/rcpig-post.php:10
msgctxt "Post Type Singular Name"
msgid "Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:11 custom-post/rcpig-post.php:30
msgid "Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:12
msgid "Parent Portfolio:"
msgstr ""

#: custom-post/rcpig-post.php:13
msgid "All Portfolios"
msgstr ""

#: custom-post/rcpig-post.php:14
msgid "View Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:15
msgid "Add New Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:16
msgid "New Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:17
msgid "Edit Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:18
msgid "Update Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:19
msgid "Search Portfolio"
msgstr ""

#: custom-post/rcpig-post.php:20
msgid "No Portfolio Found"
msgstr ""

#: custom-post/rcpig-post.php:21
msgid "No Portfolio in Trash"
msgstr ""

#: custom-post/rcpig-post.php:31
msgid "Lightweight portfolio gallery grid plugin"
msgstr ""

#: custom-post/rcpig-post.php:66
msgctxt "Taxonomy General Name"
msgid "Portfolio Categories"
msgstr ""

#: custom-post/rcpig-post.php:67
msgctxt "Taxonomy Singular Name"
msgid "Portfolio Category"
msgstr ""

#: custom-post/rcpig-post.php:68
msgid "Portfolio Category"
msgstr ""

#: custom-post/rcpig-post.php:69
msgid "All Categories"
msgstr ""

#: custom-post/rcpig-post.php:70
msgid "Parent Category"
msgstr ""

#: custom-post/rcpig-post.php:71
msgid "Parent Category:"
msgstr ""

#: custom-post/rcpig-post.php:72
msgid "New Category Name"
msgstr ""

#: custom-post/rcpig-post.php:73
msgid "Add New Category"
msgstr ""

#: custom-post/rcpig-post.php:74
msgid "Edit Category"
msgstr ""

#: custom-post/rcpig-post.php:75
msgid "Update Category"
msgstr ""

#: custom-post/rcpig-post.php:76
msgid "Separate categories with commas"
msgstr ""

#: custom-post/rcpig-post.php:77
msgid "Search categories"
msgstr ""

#: custom-post/rcpig-post.php:78
msgid "Add or remove Categories"
msgstr ""

#: custom-post/rcpig-post.php:79
msgid "Choose from the most used categories"
msgstr ""

#: custom-post/rcpig-post.php:80
msgid "Not Found"
msgstr ""

#: custom-post/rcpig-post.php:169
msgid "Post navigation"
msgstr ""

#: custom-post/rcpig-post.php:171
msgctxt "Previous post link"
msgid "<span class=\"meta-nav\">&larr;</span> %title"
msgstr ""

#: custom-post/rcpig-post.php:172
msgctxt "Next post link"
msgid "%title <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: custom-post/rcpig-post.php:196
msgid "Previous"
msgstr ""

#: custom-post/rcpig-post.php:197
msgid "Next"
msgstr ""

#: custom-post/rcpig-post.php:202
msgid "Posts navigation"
msgstr ""

#: custom-post/rcpig_metabox.php:16
msgid "Buttons For this Portfolio"
msgstr ""

#: custom-post/rcpig_metabox.php:49
msgid "Button Text"
msgstr ""

#: custom-post/rcpig_metabox.php:50
msgid "Button URL"
msgstr ""

#: custom-post/rcpig_metabox.php:51
msgid "New Tab"
msgstr ""

#: custom-post/rcpig_metabox.php:52
msgid "Actions"
msgstr ""

#: custom-post/rcpig_metabox.php:58 custom-post/rcpig_metabox.php:68 
#: custom-post/rcpig_metabox.php:86 custom-post/rcpig_metabox.php:96
msgid "e.g. Demo"
msgstr ""

#: custom-post/rcpig_metabox.php:59 custom-post/rcpig_metabox.php:87
msgid "Button Color"
msgstr ""

#: custom-post/rcpig_metabox.php:61 custom-post/rcpig_metabox.php:89
msgid "Button Hover"
msgstr ""

#: custom-post/rcpig_metabox.php:63 custom-post/rcpig_metabox.php:91
msgid "Font Color"
msgstr ""

#: custom-post/rcpig_metabox.php:65 custom-post/rcpig_metabox.php:93
msgid "Font Hover"
msgstr ""

#: custom-post/rcpig_metabox.php:72 custom-post/rcpig_metabox.php:75 
#: custom-post/rcpig_metabox.php:99 inc/rcpig-add-gallery.php:54 
#: inc/rcpig-add-gallery.php:64 inc/rcpig-add-gallery.php:70 
#: inc/rcpig-edit-gallery.php:144 inc/rcpig-edit-gallery.php:147 
#: inc/rcpig-edit-gallery.php:159 inc/rcpig-edit-gallery.php:162 
#: inc/rcpig-edit-gallery.php:170 inc/rcpig-edit-gallery.php:173 
#: inc/rcpig-process.php:51
msgid "Yes"
msgstr ""

#: custom-post/rcpig_metabox.php:73 custom-post/rcpig_metabox.php:76 
#: custom-post/rcpig_metabox.php:100 inc/rcpig-add-gallery.php:55 
#: inc/rcpig-add-gallery.php:65 inc/rcpig-add-gallery.php:71 
#: inc/rcpig-edit-gallery.php:145 inc/rcpig-edit-gallery.php:148 
#: inc/rcpig-edit-gallery.php:160 inc/rcpig-edit-gallery.php:163 
#: inc/rcpig-edit-gallery.php:171 inc/rcpig-edit-gallery.php:174 
#: inc/rcpig-process.php:51 inc/rcpig-process.php:65
msgid "No"
msgstr ""

#: custom-post/rcpig_metabox.php:107 inc/rcpig-process.php:10
msgid "Add New"
msgstr ""

#. Unique ID
#: custom-post/rcpig_metabox.php:185
msgid "Images For this Portfolio"
msgstr ""

#: custom-post/rcpig_metabox.php:212
msgid "Upload Images"
msgstr ""

#: custom-post/rcpig_metabox.php:213
msgid "Click on image to remove it"
msgstr ""

#: inc/rcpig-add-gallery.php:15 inc/rcpig-edit-gallery.php:17
msgid "General Settings"
msgstr ""

#: inc/rcpig-add-gallery.php:16 inc/rcpig-edit-gallery.php:18
msgid "Structure Settings"
msgstr ""

#: inc/rcpig-add-gallery.php:17 inc/rcpig-edit-gallery.php:19
msgid "Font Settings"
msgstr ""

#: inc/rcpig-add-gallery.php:18 inc/rcpig-edit-gallery.php:20
msgid "Color Settings"
msgstr ""

#: inc/rcpig-add-gallery.php:19 inc/rcpig-edit-gallery.php:21
msgid "Advanced Settings"
msgstr ""

#: inc/rcpig-add-gallery.php:23 inc/rcpig-edit-gallery.php:25
msgid "Enable Portfolio Gallery"
msgstr ""

#: inc/rcpig-add-gallery.php:25 inc/rcpig-edit-gallery.php:27
msgid "Modify Portfolio Gallery Name"
msgstr ""

#: inc/rcpig-add-gallery.php:27 inc/rcpig-edit-gallery.php:29
msgid "Number of Post"
msgstr ""

#: inc/rcpig-add-gallery.php:27 inc/rcpig-edit-gallery.php:29
msgid "Number of post to show. Default -1, means show all."
msgstr ""

#: inc/rcpig-add-gallery.php:29 inc/rcpig-edit-gallery.php:31
msgid "Select posts order in"
msgstr ""

#: inc/rcpig-add-gallery.php:29 inc/rcpig-edit-gallery.php:31
msgid ""
"Designates the ascending or descending order of the 'orderby' parameter. "
"Defaults to 'DESC'. Ascending order from lowest to highest values (1, 2, 3; "
"a, b, c). Descending order from highest to lowest values (3, 2, 1; c, b, a)."
msgstr ""

#: inc/rcpig-add-gallery.php:31 inc/rcpig-edit-gallery.php:34 
#: inc/rcpig-edit-gallery.php:37
msgid "Ascending"
msgstr ""

#: inc/rcpig-add-gallery.php:32 inc/rcpig-edit-gallery.php:35 
#: inc/rcpig-edit-gallery.php:38
msgid "Descending"
msgstr ""

#: inc/rcpig-add-gallery.php:34 inc/rcpig-edit-gallery.php:41
msgid "Display posts sorted by"
msgstr ""

#: inc/rcpig-add-gallery.php:34 inc/rcpig-edit-gallery.php:41
msgid "Sort retrieved posts by parameter. Defaults to 'date (post_date)'."
msgstr ""

#: inc/rcpig-add-gallery.php:36 inc/rcpig-edit-gallery.php:44 
#: inc/rcpig-edit-gallery.php:54 inc/rcpig-edit-gallery.php:64 
#: inc/rcpig-edit-gallery.php:74 inc/rcpig-edit-gallery.php:84 
#: inc/rcpig-edit-gallery.php:94 inc/rcpig-edit-gallery.php:104 
#: inc/rcpig-edit-gallery.php:114 inc/rcpig-edit-gallery.php:124
msgid "Post ID"
msgstr ""

#: inc/rcpig-add-gallery.php:37 inc/rcpig-edit-gallery.php:45 
#: inc/rcpig-edit-gallery.php:55 inc/rcpig-edit-gallery.php:65 
#: inc/rcpig-edit-gallery.php:75 inc/rcpig-edit-gallery.php:85 
#: inc/rcpig-edit-gallery.php:95 inc/rcpig-edit-gallery.php:105 
#: inc/rcpig-edit-gallery.php:115 inc/rcpig-edit-gallery.php:125
msgid "Author"
msgstr ""

#: inc/rcpig-add-gallery.php:38 inc/rcpig-edit-gallery.php:46 
#: inc/rcpig-edit-gallery.php:56 inc/rcpig-edit-gallery.php:66 
#: inc/rcpig-edit-gallery.php:76 inc/rcpig-edit-gallery.php:86 
#: inc/rcpig-edit-gallery.php:96 inc/rcpig-edit-gallery.php:106 
#: inc/rcpig-edit-gallery.php:116 inc/rcpig-edit-gallery.php:126
msgid "Title"
msgstr ""

#: inc/rcpig-add-gallery.php:39 inc/rcpig-edit-gallery.php:47 
#: inc/rcpig-edit-gallery.php:57 inc/rcpig-edit-gallery.php:67 
#: inc/rcpig-edit-gallery.php:77 inc/rcpig-edit-gallery.php:87 
#: inc/rcpig-edit-gallery.php:97 inc/rcpig-edit-gallery.php:107 
#: inc/rcpig-edit-gallery.php:117 inc/rcpig-edit-gallery.php:127
msgid "Post Name (post slug)"
msgstr ""

#: inc/rcpig-add-gallery.php:40 inc/rcpig-edit-gallery.php:48 
#: inc/rcpig-edit-gallery.php:58 inc/rcpig-edit-gallery.php:68 
#: inc/rcpig-edit-gallery.php:78 inc/rcpig-edit-gallery.php:88 
#: inc/rcpig-edit-gallery.php:98 inc/rcpig-edit-gallery.php:108 
#: inc/rcpig-edit-gallery.php:118 inc/rcpig-edit-gallery.php:128
msgid "Post Date"
msgstr ""

#: inc/rcpig-add-gallery.php:41 inc/rcpig-edit-gallery.php:49 
#: inc/rcpig-edit-gallery.php:59 inc/rcpig-edit-gallery.php:69 
#: inc/rcpig-edit-gallery.php:79 inc/rcpig-edit-gallery.php:89 
#: inc/rcpig-edit-gallery.php:99 inc/rcpig-edit-gallery.php:109 
#: inc/rcpig-edit-gallery.php:119 inc/rcpig-edit-gallery.php:129
msgid "Last Modified Date"
msgstr ""

#: inc/rcpig-add-gallery.php:42 inc/rcpig-edit-gallery.php:50 
#: inc/rcpig-edit-gallery.php:60 inc/rcpig-edit-gallery.php:70 
#: inc/rcpig-edit-gallery.php:80 inc/rcpig-edit-gallery.php:90 
#: inc/rcpig-edit-gallery.php:100 inc/rcpig-edit-gallery.php:110 
#: inc/rcpig-edit-gallery.php:120 inc/rcpig-edit-gallery.php:130
msgid "Post/Page Parent ID"
msgstr ""

#: inc/rcpig-add-gallery.php:43 inc/rcpig-edit-gallery.php:51 
#: inc/rcpig-edit-gallery.php:61 inc/rcpig-edit-gallery.php:71 
#: inc/rcpig-edit-gallery.php:81 inc/rcpig-edit-gallery.php:91 
#: inc/rcpig-edit-gallery.php:101 inc/rcpig-edit-gallery.php:111 
#: inc/rcpig-edit-gallery.php:121 inc/rcpig-edit-gallery.php:131
msgid "Random"
msgstr ""

#: inc/rcpig-add-gallery.php:44 inc/rcpig-edit-gallery.php:52 
#: inc/rcpig-edit-gallery.php:62 inc/rcpig-edit-gallery.php:72 
#: inc/rcpig-edit-gallery.php:82 inc/rcpig-edit-gallery.php:92 
#: inc/rcpig-edit-gallery.php:102 inc/rcpig-edit-gallery.php:112 
#: inc/rcpig-edit-gallery.php:122 inc/rcpig-edit-gallery.php:132
msgid "Comment Count"
msgstr ""

#: inc/rcpig-add-gallery.php:46 inc/rcpig-edit-gallery.php:135
msgid "Include Taxonomies"
msgstr ""

#: inc/rcpig-add-gallery.php:46 inc/rcpig-edit-gallery.php:135
msgid "You can include selected Taxonomies from portfolio."
msgstr ""

#: inc/rcpig-add-gallery.php:52 inc/rcpig-edit-gallery.php:141
msgid "Display Portfolio Description"
msgstr ""

#: inc/rcpig-add-gallery.php:58 inc/rcpig-edit-gallery.php:152
msgid "Enter Excerpt Length"
msgstr ""

#: inc/rcpig-add-gallery.php:58 inc/rcpig-edit-gallery.php:152
msgid "Number of words to show for portfolio description."
msgstr ""

#: inc/rcpig-add-gallery.php:60 inc/rcpig-edit-gallery.php:154
msgid "Enter Read More Text"
msgstr ""

#: inc/rcpig-add-gallery.php:62 inc/rcpig-edit-gallery.php:156
msgid "Display Carouel Image Description"
msgstr ""

#: inc/rcpig-add-gallery.php:68 inc/rcpig-edit-gallery.php:167
msgid "Display Carouel Image Title"
msgstr ""

#: inc/rcpig-add-gallery.php:76 inc/rcpig-edit-gallery.php:180
msgid "Portfolio Gallery Container Width"
msgstr ""

#: inc/rcpig-add-gallery.php:76 inc/rcpig-edit-gallery.php:180
msgid "Enter the total width of your portfolio gallery here."
msgstr ""

#: inc/rcpig-add-gallery.php:78 inc/rcpig-edit-gallery.php:182
msgid "Portfolio Thumbnail Width in px"
msgstr ""

#: inc/rcpig-add-gallery.php:80 inc/rcpig-edit-gallery.php:184
msgid "Portfolio Thumbnail Height in px"
msgstr ""

#: inc/rcpig-add-gallery.php:82 inc/rcpig-edit-gallery.php:186
msgid "Portfolio Thumbnail Alignment"
msgstr ""

#: inc/rcpig-add-gallery.php:84 inc/rcpig-edit-gallery.php:189 
#: inc/rcpig-edit-gallery.php:193 inc/rcpig-edit-gallery.php:197 
#: inc/rcpig-settings.php:62 inc/rcpig-settings.php:66 
#: inc/rcpig-settings.php:70 inc/rcpig-settings.php:120 
#: inc/rcpig-settings.php:124 inc/rcpig-settings.php:128
msgid "Left"
msgstr ""

#: inc/rcpig-add-gallery.php:85 inc/rcpig-edit-gallery.php:190 
#: inc/rcpig-edit-gallery.php:194 inc/rcpig-edit-gallery.php:198 
#: inc/rcpig-settings.php:63 inc/rcpig-settings.php:67 
#: inc/rcpig-settings.php:71 inc/rcpig-settings.php:121 
#: inc/rcpig-settings.php:125 inc/rcpig-settings.php:129
msgid "Right"
msgstr ""

#: inc/rcpig-add-gallery.php:86 inc/rcpig-edit-gallery.php:191 
#: inc/rcpig-edit-gallery.php:195 inc/rcpig-edit-gallery.php:199 
#: inc/rcpig-settings.php:64 inc/rcpig-settings.php:68 
#: inc/rcpig-settings.php:72 inc/rcpig-settings.php:122 
#: inc/rcpig-settings.php:126 inc/rcpig-settings.php:130
msgid "Center"
msgstr ""

#: inc/rcpig-add-gallery.php:88 inc/rcpig-edit-gallery.php:202
msgid "Portfolio Button Width"
msgstr ""

#: inc/rcpig-add-gallery.php:90 inc/rcpig-edit-gallery.php:204
msgid "Portfolio Button Height"
msgstr ""

#: inc/rcpig-add-gallery.php:92 inc/rcpig-edit-gallery.php:206
msgid "Space Between Thumbnails (Top-Bottom)"
msgstr ""

#: inc/rcpig-add-gallery.php:94 inc/rcpig-edit-gallery.php:208
msgid "Space Between Thumbnails (Left-Right)"
msgstr ""

#: inc/rcpig-add-gallery.php:99 inc/rcpig-edit-gallery.php:213
msgid ""
"Enter <a href=\"https://www.google.com/fonts\" target=\"_blank\">Google "
"Fonts</a> URL link"
msgstr ""

#: inc/rcpig-add-gallery.php:99 inc/rcpig-edit-gallery.php:213
msgid ""
"Find a Google font that you'd like to use on your gallery. When you find the "
"font that you like, click on the 'Quick-use' icon. 'Quick-use' icon will "
"redirect you to the selected font page where you'll be asked to choose the "
"styles and character sets for your selected font. Then you will find a "
"generated code in the 'Standard' tab. You don't need to copy the whole code. "
"You just need to add an URL link without http: or https: (e.g. //fonts."
"googleapis.com/css?family=Roboto+Condensed:400,700)"
msgstr ""

#: inc/rcpig-add-gallery.php:101 inc/rcpig-edit-gallery.php:215
msgid "Enter Primary Font Family"
msgstr ""

#: inc/rcpig-add-gallery.php:103 inc/rcpig-edit-gallery.php:217
msgid "Enter Secondary Font Family"
msgstr ""

#: inc/rcpig-add-gallery.php:105 inc/rcpig-edit-gallery.php:219
msgid "Portfolio Thumbnail Hover Font Size"
msgstr ""

#: inc/rcpig-add-gallery.php:107 inc/rcpig-edit-gallery.php:221
msgid "Portfolio Title Font Size"
msgstr ""

#: inc/rcpig-add-gallery.php:109 inc/rcpig-edit-gallery.php:223
msgid "Portfolio Description Font Size"
msgstr ""

#: inc/rcpig-add-gallery.php:111 inc/rcpig-edit-gallery.php:225
msgid "Portfolio Button Font Size"
msgstr ""

#: inc/rcpig-add-gallery.php:113 inc/rcpig-edit-gallery.php:227
msgid "Enter Read More Font Size"
msgstr ""

#: inc/rcpig-add-gallery.php:121 inc/rcpig-edit-gallery.php:235
msgid "Background Colors"
msgstr ""

#: inc/rcpig-add-gallery.php:124 inc/rcpig-edit-gallery.php:238
msgid "Portfolio Active Filter Button Color"
msgstr ""

#: inc/rcpig-add-gallery.php:128 inc/rcpig-edit-gallery.php:242
msgid "Portfolio Inactive Filter Button Color"
msgstr ""

#: inc/rcpig-add-gallery.php:132 inc/rcpig-edit-gallery.php:246
msgid "Portfolio Inactive Filter Button Hover"
msgstr ""

#: inc/rcpig-add-gallery.php:136 inc/rcpig-edit-gallery.php:250
msgid "Portfolio Hover Background"
msgstr ""

#: inc/rcpig-add-gallery.php:140 inc/rcpig-edit-gallery.php:254
msgid "Portfolio Hover Opacity"
msgstr ""

#: inc/rcpig-add-gallery.php:144 inc/rcpig-edit-gallery.php:258
msgid "Portfolio Thumbnail Hover Border Color"
msgstr ""

#: inc/rcpig-add-gallery.php:148 inc/rcpig-edit-gallery.php:262
msgid "Expanding Preview Background Color"
msgstr ""

#: inc/rcpig-add-gallery.php:152 inc/rcpig-edit-gallery.php:266
msgid "Slider Wrapper Background Color"
msgstr ""

#: inc/rcpig-add-gallery.php:156 inc/rcpig-edit-gallery.php:270
msgid "Read More Button Color"
msgstr ""

#: inc/rcpig-add-gallery.php:160 inc/rcpig-edit-gallery.php:274
msgid "Read More Hover Color"
msgstr ""

#: inc/rcpig-add-gallery.php:165 inc/rcpig-edit-gallery.php:279
msgid "Font Colors"
msgstr ""

#: inc/rcpig-add-gallery.php:168 inc/rcpig-edit-gallery.php:282
msgid "Portfolio Active Font Color"
msgstr ""

#: inc/rcpig-add-gallery.php:172 inc/rcpig-edit-gallery.php:286
msgid "Portfolio Inactive Font Color"
msgstr ""

#: inc/rcpig-add-gallery.php:176 inc/rcpig-edit-gallery.php:290
msgid "Portfolio Thumbnail Hover Font Color"
msgstr ""

#: inc/rcpig-add-gallery.php:180 inc/rcpig-edit-gallery.php:294
msgid "Portfolio Title Font Color"
msgstr ""

#: inc/rcpig-add-gallery.php:184 inc/rcpig-edit-gallery.php:298
msgid "Portfolio Description Font Color"
msgstr ""

#: inc/rcpig-add-gallery.php:188 inc/rcpig-edit-gallery.php:302
msgid "Read More Font Color"
msgstr ""

#: inc/rcpig-add-gallery.php:195 inc/rcpig-edit-gallery.php:308
msgid "Select Portfolio Filter Effect"
msgstr ""

#: inc/rcpig-add-gallery.php:197 inc/rcpig-add-gallery.php:245 
#: inc/rcpig-edit-gallery.php:311 inc/rcpig-edit-gallery.php:320 
#: inc/rcpig-edit-gallery.php:329 inc/rcpig-edit-gallery.php:338 
#: inc/rcpig-edit-gallery.php:347 inc/rcpig-edit-gallery.php:356 
#: inc/rcpig-edit-gallery.php:365 inc/rcpig-edit-gallery.php:374 
#: inc/rcpig-edit-gallery.php:560 inc/rcpig-edit-gallery.php:568 
#: inc/rcpig-edit-gallery.php:576 inc/rcpig-edit-gallery.php:584 
#: inc/rcpig-edit-gallery.php:592 inc/rcpig-edit-gallery.php:600 
#: inc/rcpig-edit-gallery.php:608
msgid "None"
msgstr ""

#: inc/rcpig-add-gallery.php:198 inc/rcpig-edit-gallery.php:312 
#: inc/rcpig-edit-gallery.php:321 inc/rcpig-edit-gallery.php:330 
#: inc/rcpig-edit-gallery.php:339 inc/rcpig-edit-gallery.php:348 
#: inc/rcpig-edit-gallery.php:357 inc/rcpig-edit-gallery.php:366 
#: inc/rcpig-edit-gallery.php:375
msgid "Move Up"
msgstr ""

#: inc/rcpig-add-gallery.php:199 inc/rcpig-edit-gallery.php:313 
#: inc/rcpig-edit-gallery.php:322 inc/rcpig-edit-gallery.php:331 
#: inc/rcpig-edit-gallery.php:340 inc/rcpig-edit-gallery.php:349 
#: inc/rcpig-edit-gallery.php:358 inc/rcpig-edit-gallery.php:367 
#: inc/rcpig-edit-gallery.php:376
msgid "Scale Up"
msgstr ""

#: inc/rcpig-add-gallery.php:200 inc/rcpig-edit-gallery.php:314 
#: inc/rcpig-edit-gallery.php:323 inc/rcpig-edit-gallery.php:332 
#: inc/rcpig-edit-gallery.php:341 inc/rcpig-edit-gallery.php:350 
#: inc/rcpig-edit-gallery.php:359 inc/rcpig-edit-gallery.php:368 
#: inc/rcpig-edit-gallery.php:377
msgid "Fall Perspective"
msgstr ""

#: inc/rcpig-add-gallery.php:201 inc/rcpig-edit-gallery.php:315 
#: inc/rcpig-edit-gallery.php:324 inc/rcpig-edit-gallery.php:333 
#: inc/rcpig-edit-gallery.php:342 inc/rcpig-edit-gallery.php:351 
#: inc/rcpig-edit-gallery.php:360 inc/rcpig-edit-gallery.php:369 
#: inc/rcpig-edit-gallery.php:378
msgid "Fly"
msgstr ""

#: inc/rcpig-add-gallery.php:202 inc/rcpig-edit-gallery.php:316 
#: inc/rcpig-edit-gallery.php:325 inc/rcpig-edit-gallery.php:334 
#: inc/rcpig-edit-gallery.php:343 inc/rcpig-edit-gallery.php:352 
#: inc/rcpig-edit-gallery.php:361 inc/rcpig-edit-gallery.php:370 
#: inc/rcpig-edit-gallery.php:379
msgid "Flip"
msgstr ""

#: inc/rcpig-add-gallery.php:203 inc/rcpig-edit-gallery.php:317 
#: inc/rcpig-edit-gallery.php:326 inc/rcpig-edit-gallery.php:335 
#: inc/rcpig-edit-gallery.php:344 inc/rcpig-edit-gallery.php:353 
#: inc/rcpig-edit-gallery.php:362 inc/rcpig-edit-gallery.php:371 
#: inc/rcpig-edit-gallery.php:380
msgid "Helix"
msgstr ""

#: inc/rcpig-add-gallery.php:204 inc/rcpig-edit-gallery.php:318 
#: inc/rcpig-edit-gallery.php:327 inc/rcpig-edit-gallery.php:336 
#: inc/rcpig-edit-gallery.php:345 inc/rcpig-edit-gallery.php:354 
#: inc/rcpig-edit-gallery.php:363 inc/rcpig-edit-gallery.php:372 
#: inc/rcpig-edit-gallery.php:381
msgid "Popup"
msgstr ""

#: inc/rcpig-add-gallery.php:206 inc/rcpig-edit-gallery.php:384
msgid "Display Thumbnail Hover Overlay Effect"
msgstr ""

#: inc/rcpig-add-gallery.php:208 inc/rcpig-add-gallery.php:230 
#: inc/rcpig-add-gallery.php:239 inc/rcpig-edit-gallery.php:387 
#: inc/rcpig-edit-gallery.php:390 inc/rcpig-edit-gallery.php:536 
#: inc/rcpig-edit-gallery.php:539 inc/rcpig-edit-gallery.php:549 
#: inc/rcpig-edit-gallery.php:552
msgid "On"
msgstr ""

#: inc/rcpig-add-gallery.php:209 inc/rcpig-add-gallery.php:231 
#: inc/rcpig-add-gallery.php:240 inc/rcpig-edit-gallery.php:388 
#: inc/rcpig-edit-gallery.php:391 inc/rcpig-edit-gallery.php:537 
#: inc/rcpig-edit-gallery.php:540 inc/rcpig-edit-gallery.php:550 
#: inc/rcpig-edit-gallery.php:553
msgid "Off"
msgstr ""

#: inc/rcpig-add-gallery.php:212 inc/rcpig-edit-gallery.php:395
msgid "Select Thumbnail Hover Overlay Effect"
msgstr ""

#: inc/rcpig-add-gallery.php:214 inc/rcpig-edit-gallery.php:398 
#: inc/rcpig-edit-gallery.php:410 inc/rcpig-edit-gallery.php:422 
#: inc/rcpig-edit-gallery.php:434 inc/rcpig-edit-gallery.php:446 
#: inc/rcpig-edit-gallery.php:458 inc/rcpig-edit-gallery.php:470 
#: inc/rcpig-edit-gallery.php:482 inc/rcpig-edit-gallery.php:494 
#: inc/rcpig-edit-gallery.php:506 inc/rcpig-edit-gallery.php:518
msgid "Hover Effect 1"
msgstr ""

#: inc/rcpig-add-gallery.php:215 inc/rcpig-edit-gallery.php:399 
#: inc/rcpig-edit-gallery.php:411 inc/rcpig-edit-gallery.php:423 
#: inc/rcpig-edit-gallery.php:435 inc/rcpig-edit-gallery.php:447 
#: inc/rcpig-edit-gallery.php:459 inc/rcpig-edit-gallery.php:471 
#: inc/rcpig-edit-gallery.php:483 inc/rcpig-edit-gallery.php:495 
#: inc/rcpig-edit-gallery.php:507 inc/rcpig-edit-gallery.php:519
msgid "Hover Effect 2"
msgstr ""

#: inc/rcpig-add-gallery.php:216 inc/rcpig-edit-gallery.php:400 
#: inc/rcpig-edit-gallery.php:412 inc/rcpig-edit-gallery.php:424 
#: inc/rcpig-edit-gallery.php:436 inc/rcpig-edit-gallery.php:448 
#: inc/rcpig-edit-gallery.php:460 inc/rcpig-edit-gallery.php:472 
#: inc/rcpig-edit-gallery.php:484 inc/rcpig-edit-gallery.php:496 
#: inc/rcpig-edit-gallery.php:508 inc/rcpig-edit-gallery.php:520
msgid "Hover Effect 3"
msgstr ""

#: inc/rcpig-add-gallery.php:217 inc/rcpig-edit-gallery.php:401 
#: inc/rcpig-edit-gallery.php:413 inc/rcpig-edit-gallery.php:425 
#: inc/rcpig-edit-gallery.php:437 inc/rcpig-edit-gallery.php:449 
#: inc/rcpig-edit-gallery.php:461 inc/rcpig-edit-gallery.php:473 
#: inc/rcpig-edit-gallery.php:485 inc/rcpig-edit-gallery.php:497 
#: inc/rcpig-edit-gallery.php:509 inc/rcpig-edit-gallery.php:521
msgid "Hover Effect 4"
msgstr ""

#: inc/rcpig-add-gallery.php:218 inc/rcpig-edit-gallery.php:402 
#: inc/rcpig-edit-gallery.php:414 inc/rcpig-edit-gallery.php:426 
#: inc/rcpig-edit-gallery.php:438 inc/rcpig-edit-gallery.php:450 
#: inc/rcpig-edit-gallery.php:462 inc/rcpig-edit-gallery.php:474 
#: inc/rcpig-edit-gallery.php:486 inc/rcpig-edit-gallery.php:498 
#: inc/rcpig-edit-gallery.php:510 inc/rcpig-edit-gallery.php:522
msgid "Hover Effect 5"
msgstr ""

#: inc/rcpig-add-gallery.php:219 inc/rcpig-edit-gallery.php:403 
#: inc/rcpig-edit-gallery.php:415 inc/rcpig-edit-gallery.php:427 
#: inc/rcpig-edit-gallery.php:439 inc/rcpig-edit-gallery.php:451 
#: inc/rcpig-edit-gallery.php:463 inc/rcpig-edit-gallery.php:475 
#: inc/rcpig-edit-gallery.php:487 inc/rcpig-edit-gallery.php:499 
#: inc/rcpig-edit-gallery.php:511 inc/rcpig-edit-gallery.php:523
msgid "Hover Effect 6"
msgstr ""

#: inc/rcpig-add-gallery.php:220 inc/rcpig-edit-gallery.php:404 
#: inc/rcpig-edit-gallery.php:416 inc/rcpig-edit-gallery.php:428 
#: inc/rcpig-edit-gallery.php:440 inc/rcpig-edit-gallery.php:452 
#: inc/rcpig-edit-gallery.php:464 inc/rcpig-edit-gallery.php:476 
#: inc/rcpig-edit-gallery.php:488 inc/rcpig-edit-gallery.php:500 
#: inc/rcpig-edit-gallery.php:512 inc/rcpig-edit-gallery.php:524
msgid "Hover Effect 7"
msgstr ""

#: inc/rcpig-add-gallery.php:221 inc/rcpig-edit-gallery.php:405 
#: inc/rcpig-edit-gallery.php:417 inc/rcpig-edit-gallery.php:429 
#: inc/rcpig-edit-gallery.php:441 inc/rcpig-edit-gallery.php:453 
#: inc/rcpig-edit-gallery.php:465 inc/rcpig-edit-gallery.php:477 
#: inc/rcpig-edit-gallery.php:489 inc/rcpig-edit-gallery.php:501 
#: inc/rcpig-edit-gallery.php:513 inc/rcpig-edit-gallery.php:525
msgid "Hover Effect 8"
msgstr ""

#: inc/rcpig-add-gallery.php:222 inc/rcpig-edit-gallery.php:406 
#: inc/rcpig-edit-gallery.php:418 inc/rcpig-edit-gallery.php:430 
#: inc/rcpig-edit-gallery.php:442 inc/rcpig-edit-gallery.php:454 
#: inc/rcpig-edit-gallery.php:466 inc/rcpig-edit-gallery.php:478 
#: inc/rcpig-edit-gallery.php:490 inc/rcpig-edit-gallery.php:502 
#: inc/rcpig-edit-gallery.php:514 inc/rcpig-edit-gallery.php:526
msgid "Hover Effect 9"
msgstr ""

#: inc/rcpig-add-gallery.php:223 inc/rcpig-edit-gallery.php:407 
#: inc/rcpig-edit-gallery.php:419 inc/rcpig-edit-gallery.php:431 
#: inc/rcpig-edit-gallery.php:443 inc/rcpig-edit-gallery.php:455 
#: inc/rcpig-edit-gallery.php:467 inc/rcpig-edit-gallery.php:479 
#: inc/rcpig-edit-gallery.php:491 inc/rcpig-edit-gallery.php:503 
#: inc/rcpig-edit-gallery.php:515 inc/rcpig-edit-gallery.php:527
msgid "Hover Effect 10"
msgstr ""

#: inc/rcpig-add-gallery.php:224 inc/rcpig-edit-gallery.php:408 
#: inc/rcpig-edit-gallery.php:420 inc/rcpig-edit-gallery.php:432 
#: inc/rcpig-edit-gallery.php:444 inc/rcpig-edit-gallery.php:456 
#: inc/rcpig-edit-gallery.php:468 inc/rcpig-edit-gallery.php:480 
#: inc/rcpig-edit-gallery.php:492 inc/rcpig-edit-gallery.php:504 
#: inc/rcpig-edit-gallery.php:516 inc/rcpig-edit-gallery.php:528
msgid "Hover Effect 11"
msgstr ""

#: inc/rcpig-add-gallery.php:228 inc/rcpig-edit-gallery.php:533
msgid "Thumbnail Hover Inverse"
msgstr ""

#: inc/rcpig-add-gallery.php:233 inc/rcpig-edit-gallery.php:543
msgid "Thumbnail Hover Delay"
msgstr ""

#: inc/rcpig-add-gallery.php:237 inc/rcpig-edit-gallery.php:546
msgid "Display Thumbnail Hover Image Effect"
msgstr ""

#: inc/rcpig-add-gallery.php:243 inc/rcpig-edit-gallery.php:557
msgid "Select Thumbnail Hover Image Effect"
msgstr ""

#: inc/rcpig-add-gallery.php:246 inc/rcpig-edit-gallery.php:561 
#: inc/rcpig-edit-gallery.php:569 inc/rcpig-edit-gallery.php:577 
#: inc/rcpig-edit-gallery.php:585 inc/rcpig-edit-gallery.php:593 
#: inc/rcpig-edit-gallery.php:601 inc/rcpig-edit-gallery.php:609
msgid "Zoom and Pan"
msgstr ""

#: inc/rcpig-add-gallery.php:247 inc/rcpig-edit-gallery.php:562 
#: inc/rcpig-edit-gallery.php:570 inc/rcpig-edit-gallery.php:578 
#: inc/rcpig-edit-gallery.php:586 inc/rcpig-edit-gallery.php:594 
#: inc/rcpig-edit-gallery.php:602 inc/rcpig-edit-gallery.php:610
msgid "Slide Out"
msgstr ""

#: inc/rcpig-add-gallery.php:248 inc/rcpig-edit-gallery.php:563 
#: inc/rcpig-edit-gallery.php:571 inc/rcpig-edit-gallery.php:579 
#: inc/rcpig-edit-gallery.php:587 inc/rcpig-edit-gallery.php:595 
#: inc/rcpig-edit-gallery.php:603 inc/rcpig-edit-gallery.php:611
msgid "Shrink Rotate"
msgstr ""

#: inc/rcpig-add-gallery.php:249 inc/rcpig-edit-gallery.php:564 
#: inc/rcpig-edit-gallery.php:572 inc/rcpig-edit-gallery.php:580 
#: inc/rcpig-edit-gallery.php:588 inc/rcpig-edit-gallery.php:596 
#: inc/rcpig-edit-gallery.php:604 inc/rcpig-edit-gallery.php:612
msgid "Zoom and Hide"
msgstr ""

#: inc/rcpig-add-gallery.php:250 inc/rcpig-edit-gallery.php:565 
#: inc/rcpig-edit-gallery.php:573 inc/rcpig-edit-gallery.php:581 
#: inc/rcpig-edit-gallery.php:589 inc/rcpig-edit-gallery.php:597 
#: inc/rcpig-edit-gallery.php:605 inc/rcpig-edit-gallery.php:613
msgid "Shrink"
msgstr ""

#: inc/rcpig-add-gallery.php:251 inc/rcpig-edit-gallery.php:566 
#: inc/rcpig-edit-gallery.php:574 inc/rcpig-edit-gallery.php:582 
#: inc/rcpig-edit-gallery.php:590 inc/rcpig-edit-gallery.php:598 
#: inc/rcpig-edit-gallery.php:606 inc/rcpig-edit-gallery.php:614
msgid "Morph into Circle"
msgstr ""

#: inc/rcpig-add-gallery.php:254 inc/rcpig-edit-gallery.php:618
msgid "Show Multiple Image Carousel"
msgstr ""

#: inc/rcpig-add-gallery.php:256 inc/rcpig-edit-gallery.php:621 
#: inc/rcpig-edit-gallery.php:624
msgid "Show"
msgstr ""

#: inc/rcpig-add-gallery.php:257 inc/rcpig-edit-gallery.php:622 
#: inc/rcpig-edit-gallery.php:625
msgid "Hide"
msgstr ""

#: inc/rcpig-add-gallery.php:259 inc/rcpig-edit-gallery.php:628
msgid "Expanding Preview Speed"
msgstr ""

#: inc/rcpig-add-gallery.php:261 inc/rcpig-edit-gallery.php:630
msgid "Expanding Preview Height"
msgstr ""

#: inc/rcpig-add-gallery.php:269 inc/rcpig-process.php:18
msgid "Add Gallery"
msgstr ""

#: inc/rcpig-admin.php:9 inc/rcpig-admin.php:9
msgid "Generate portfolio"
msgstr ""

#: inc/rcpig-admin.php:10 inc/rcpig-admin.php:10
msgid "Portfolio Settings"
msgstr ""

#: inc/rcpig-admin.php:30 inc/rcpig-admin.php:36
msgid "You do not have sufficient permissions to access this page."
msgstr ""

#: inc/rcpig-admin.php:45
msgid "Portfolio Featured Image"
msgstr ""

#: inc/rcpig-admin.php:46
msgid "Date"
msgstr ""

#: inc/rcpig-admin.php:59
msgid "This portfolio has no featured image"
msgstr ""

#: inc/rcpig-admin.php:163 inc/rcpig-process.php:46
msgid "Edit Gallery"
msgstr ""

#: inc/rcpig-edit-gallery.php:638
msgid "Update Gallery"
msgstr ""

#: inc/rcpig-process.php:10
msgid "Portfolio Gallery"
msgstr ""

#: inc/rcpig-process.php:15
msgid "Portfolio Gallery Name"
msgstr ""

#: inc/rcpig-process.php:16
msgid "Enter Portfolio Gallery Name"
msgstr ""

#: inc/rcpig-process.php:28
msgid "ID"
msgstr ""

#: inc/rcpig-process.php:29
msgid "Gallery Name"
msgstr ""

#: inc/rcpig-process.php:30
msgid "Shortcode"
msgstr ""

#: inc/rcpig-process.php:31
msgid "Visible"
msgstr ""

#: inc/rcpig-process.php:32
msgid "Copy"
msgstr ""

#: inc/rcpig-process.php:47
msgid "Preview"
msgstr ""

#: inc/rcpig-process.php:48 inc/rcpig-process.php:62
msgid "Delete"
msgstr ""

#: inc/rcpig-process.php:61
msgid "Set Gallery"
msgstr ""

#: inc/rcpig-process.php:64
msgid ""
"Mouseover on the gallery name in the left and clicked on <strong>Set "
"Gallery</strong> link. After setting portfolio gallery you will get the "
"<strong>SHORTCODE</strong> here."
msgstr ""

#: inc/rcpig-process.php:76
msgid ""
"You didn't add any portfolio gallery yet! Click on <strong>Add New</strong> "
"button to get started. If you feel trouble to understand how to create "
"portfolio gallery, then follow the documentation that included with the "
"attached zip file.<br /><br />If you have any questions that are beyond the "
"scope of the help documentation, please feel free to open a ticket to any of "
"our support team <a href=\"http://www.realwebcare.com/submitticket.php\" "
"target=\"_blank\">here</a>. We&#39;d be happy to help you."
msgstr ""

#: inc/rcpig-settings.php:8
msgid "Portfolio Gallery Settings"
msgstr ""

#: inc/rcpig-settings.php:9
msgid "Portfolio Settings Updated."
msgstr ""

#: inc/rcpig-settings.php:12
msgid "Global Settings"
msgstr ""

#: inc/rcpig-settings.php:13
msgid "Custom Style"
msgstr ""

#: inc/rcpig-settings.php:14
msgid "Export Gallery"
msgstr ""

#: inc/rcpig-settings.php:15
msgid "Import Gallery"
msgstr ""

#: inc/rcpig-settings.php:36
msgid "Display Custom Template for Single Portfolio Page"
msgstr ""

#: inc/rcpig-settings.php:43
msgid "Enter Images Description Length"
msgstr ""

#: inc/rcpig-settings.php:48
msgid "Display Custom Template for Archive Portfolio Page"
msgstr ""

#: inc/rcpig-settings.php:55
msgid "Enter Portfolio Description Length"
msgstr ""

#: inc/rcpig-settings.php:59
msgid "Select Pagination Alignment"
msgstr ""

#: inc/rcpig-settings.php:79
msgid "Enter Portfolio Image Width"
msgstr ""

#: inc/rcpig-settings.php:83
msgid "Enter Portfolio Image Height"
msgstr ""

#: inc/rcpig-settings.php:87
msgid "Number of Portfolio Columns to Show"
msgstr ""

#: inc/rcpig-settings.php:90 inc/rcpig-settings.php:95 
#: inc/rcpig-settings.php:100 inc/rcpig-settings.php:105
msgid "1 Column"
msgstr ""

#: inc/rcpig-settings.php:91 inc/rcpig-settings.php:96 
#: inc/rcpig-settings.php:101 inc/rcpig-settings.php:106
msgid "2 Columns"
msgstr ""

#: inc/rcpig-settings.php:92 inc/rcpig-settings.php:97 
#: inc/rcpig-settings.php:102 inc/rcpig-settings.php:107
msgid "3 Columns"
msgstr ""

#: inc/rcpig-settings.php:93 inc/rcpig-settings.php:98 
#: inc/rcpig-settings.php:103 inc/rcpig-settings.php:108
msgid "4 Columns"
msgstr ""

#: inc/rcpig-settings.php:113
msgid "Enter Space Between Portfolios"
msgstr ""

#: inc/rcpig-settings.php:117
msgid "Select Content Alignment"
msgstr ""

#: inc/rcpig-settings.php:136
msgid "Display Sidebar on Template Page"
msgstr ""

#: inc/rcpig-settings.php:142
msgid "Enter Template Page Containae Width"
msgstr ""

#: inc/rcpig-settings.php:150
msgid ""
"/* \n"
"Welcome to the Custom CSS editor!\n"
"\n"
"Please add all your custom CSS here and avoid modifying the core plugin "
"files, since that'll make upgrading the plugin problematic. Your custom CSS "
"will be loaded after the plugin's stylesheets, which means that your rules "
"will take precedence. Just add your CSS here for what you want to change.\n"
"*/"
msgstr ""

#: inc/rcpig-settings.php:161
msgid "Mode"
msgstr ""

#: inc/rcpig-settings.php:183
msgid "Export"
msgstr ""

#: inc/rcpig-settings.php:184
msgid ""
"When you click <strong>Download Export File</strong> button, WordPress will "
"generate a JSON file for you to save to your computer."
msgstr ""

#: inc/rcpig-settings.php:185
msgid ""
"This backup file contains all the configuration and setting options of your "
"portfolio galleries. Note that it do <b>NOT</b> contain posts, pages, or any "
"relevant data, just your all portfolio gallery options."
msgstr ""

#: inc/rcpig-settings.php:186
msgid ""
"To create a backup for all of your portfolios, just go to <strong>Tools >> "
"Export</strong> and click on <strong>Download Export File</strong> there. "
"When you click the button WordPress will create an XML file for you to save "
"to your computer."
msgstr ""

#: inc/rcpig-settings.php:187
msgid ""
"After exporting, you can either use the backup file to restore your settings "
"on this site again or another WordPress site."
msgstr ""

#: inc/rcpig-settings.php:191
msgid "Download Export File"
msgstr ""

#: inc/rcpig-settings.php:214
msgid "Import"
msgstr ""

#: inc/rcpig-settings.php:227
msgid "All options are imported successfully."
msgstr ""

#: inc/rcpig-settings.php:228
msgid "Invalid file or file size too big."
msgstr ""

#: inc/rcpig-settings.php:231
msgid ""
"Choose a json file to upload that you backup before, then click <strong>"
"Upload file and import</strong>. Wordpress do the rest for you."
msgstr ""

#: inc/rcpig-settings.php:234
msgid "Choose a file from your computer:"
msgstr ""

#: inc/rcpig-settings.php:234
msgid "(Maximum size: 450 MB)"
msgstr ""

#: inc/rcpig-settings.php:235
msgid "Upload file and import"
msgstr ""

#: inc/rcpig-sidebar.php:10
msgid "Plugin Info"
msgstr ""

#: inc/rcpig-sidebar.php:12
msgid "Version: 1.0"
msgstr ""

#: inc/rcpig-sidebar.php:13
msgid "Requires: Wordpress 3.5+"
msgstr ""

#: inc/rcpig-sidebar.php:14
msgid "First release: 2 September, 2016"
msgstr ""

#: inc/rcpig-sidebar.php:15
msgid "Last Update: 2 September, 2016"
msgstr ""

#: inc/rcpig-sidebar.php:16
msgid "By"
msgstr ""

#: inc/rcpig-sidebar.php:16
msgid "Real Web Care"
msgstr ""

#: inc/rcpig-sidebar.php:17
msgid "Facebook"
msgstr ""

#: inc/rcpig-sidebar.php:17
msgid "Realwebcare"
msgstr ""

#: custom-post/templates/single-rcpig.php:68
msgid "%s"
msgstr ""

#: custom-post/templates/single-rcpig.php:72
msgid "View all posts by %s <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#. Name of the plugin
msgid "Responsive Portfolio Image Gallery Pro"
msgstr ""

#. Description of the plugin
msgid ""
"Responsive image gallery plugin to build powerful, lightweight, filterable "
"portfolio galleries on different posts or pages by SHORTCODE."
msgstr ""

#. URI of the plugin
msgid ""
"http://code.realwebcare.com/item/responsive-portfolio-image-gallery-pro/"
msgstr ""

#. Author URI of the plugin
msgid "http://www.realwebcare.com/"
msgstr ""
