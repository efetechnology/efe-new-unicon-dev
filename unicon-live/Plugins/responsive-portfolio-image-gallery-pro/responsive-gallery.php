<?php
/*
****** This is the latest update version ******
Plugin Name: Responsive Portfolio Image Gallery Pro
Plugin URI: http://code.realwebcare.com/item/responsive-portfolio-image-gallery-pro/
Description: Responsive image gallery plugin to build powerful, lightweight, filterable portfolio galleries on different posts or pages by SHORTCODE.
Version: 1.0.1
Author: Real Web Care
Author URI: http://www.realwebcare.com/
Text Domain: rcpig
Domain Path: /languages/
License: http://www.realwebcare.com/licenses.php (Standard License)
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
ob_start();
define('RCPIG_PLUGIN_PATH', plugin_dir_path( __FILE__ ));

/* Internationalization */
function rcpig_textdomain() {
	$domain = 'rcpig';
	$locale = apply_filters( 'plugin_locale', get_locale(), $domain );
	load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' );
	load_plugin_textdomain( $domain, FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'rcpig_textdomain' );
/* Add plugin action links */
function rcpig_plugin_actions( $links ) {
	$links[] = '<a href="'.menu_page_url('rcpig-settings', false).'">'. __('Settings','rcpig') .'</a>';
	$links[] = '<a href="http://www.realwebcare.com/supporttickets.php" target="_blank">'. __('Support','rcpig') .'</a>';
	return $links;
}
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'rcpig_plugin_actions' );
/* Adding Custom styles */
add_action( 'wp_head', 'rcpig_custom_style' );
function rcpig_custom_style() {
	$custom_css = get_option( 'custom_css' );
	if ( isset($custom_css) && !empty( $custom_css ) ) { ?>
<style type="text/css">
<?php echo $custom_css; ?>
</style><?php
	}
}
/* Enqueue CSS & JS For Admin */
function rcpig_admin_adding_style() {
	$custom_css_option = get_option('custom_css_option');
	wp_register_script( 'rcpig-admin', plugin_dir_url( __FILE__ ) . 'assets/js/rcpig-admin.min.js', array( 'jquery', 'ace_code_highlighter_js' ), '1.0.1', true );
	wp_enqueue_script( 'rcpig-admin' );
	wp_enqueue_script( 'jquery-ui-tabs' );
	wp_enqueue_script( 'wp-color-picker' );
	wp_enqueue_media();
	wp_localize_script( 'rcpig-admin', 'rcpigajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'ace_code_highlighter_js', plugin_dir_url( __FILE__ ) . 'assets/js/ace/ace.js', '', '1.0.1', true );
	wp_enqueue_script( 'ace_mode_js', plugin_dir_url( __FILE__ ) . 'assets/js/ace/mode-css.js', array( 'ace_code_highlighter_js' ), '1.0.1', true );
	wp_enqueue_style( 'jquery-ui-css', plugins_url( 'assets/css/jquery-ui.css', __FILE__ ), '', '1.12.0', false );
	wp_enqueue_style( 'rcpig_admin_style', plugins_url('assets/css/rcpig-admin.min.css', __FILE__),'','1.0.1', false );
}
add_action( 'admin_enqueue_scripts', 'rcpig_admin_adding_style' );
require_once dirname( __FILE__ ) . '/rcpig-shortcode.php';
require_once dirname( __FILE__ ) . '/custom-post/rcpig-post.php';
require_once dirname( __FILE__ ) . '/class/rcpig_aq_resizer.php';
require_once dirname( __FILE__ ) . '/inc/rcpig-admin.php';
require_once dirname( __FILE__ ) . '/inc/rcpig-sidebar.php';
require_once dirname( __FILE__ ) . '/custom-post/rcpig_metabox.php';
?>
