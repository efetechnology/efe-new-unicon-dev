<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function rcpig_add_new_gallery() {
	$portfolio_gallery = $_POST['portfolio_gallery'] != '' ? trim(preg_replace('/[^A-Za-z0-9-\w_]+/', '_', sanitize_text_field( $_POST['portfolio_gallery'] ))) : '';
	if($portfolio_gallery) {
		$portfolio_table = get_option('portfolioTables');
		$gallery_lists = explode(', ', $portfolio_table);
		if(!isset($portfolio_table)) {
			add_option('portfolioTables', $portfolio_gallery);
		} elseif(empty($portfolio_table)){
			update_option('portfolioTables', $portfolio_gallery);
		} else {
			if(in_array($portfolio_gallery, $gallery_lists)) {
				$new_portfolio_gallery = 'another_' . $portfolio_gallery;
				$portfolio_gallery_lists = $portfolio_table . ', ' . $new_portfolio_gallery;
				update_option('portfolioTables', $portfolio_gallery_lists);
			} else {
				$portfolio_gallery_lists = $portfolio_table . ', ' . $portfolio_gallery;
				update_option('portfolioTables', $portfolio_gallery_lists);
			}
		}
	}
}
function rcpig_edit_portfolio_gallery($edited_gallery, $portfolio_gallery) {
	if($portfolio_gallery && $portfolio_gallery != $edited_gallery) {
		$portfolio_table = get_option('portfolioTables');
		$table_item = explode(', ', $portfolio_table);
		foreach($table_item as $key => $value) {
			if($value == $edited_gallery) {
				if(in_array($portfolio_gallery, $table_item)) {
					$portfolio_gallery = 'another_' . $portfolio_gallery;
					$new_portfolio_table[$key] = $portfolio_gallery;
				} else {
					$new_portfolio_table[$key] = $portfolio_gallery;
				}
			} else {
				$new_portfolio_table[$key] = $value;
			}
		}
		$new_portfolio_table = implode(', ', $new_portfolio_table);
		update_option('portfolioTables', $new_portfolio_table);
		$edited_gallery_value = get_option($edited_gallery);
		if($edited_gallery_value) {
			delete_option($edited_gallery);
			add_option($portfolio_gallery, $edited_gallery_value);
		}
		$edited_option_value = get_option($edited_gallery.'_options');
		if($edited_option_value) {
			delete_option($edited_gallery.'_options');
			add_option($portfolio_gallery.'_options', $edited_option_value);
		}
		return $portfolio_gallery;
	} else {
		return $edited_gallery;
	}
}
function rcpig_delete_portfolio_gallery() {
	$portfolio_gallery = $_POST['portgallery'];
	$portfolio_gallery_lists = get_option('portfolioTables');
	$gallery_lists = get_option($portfolio_gallery);
	if(isset($gallery_lists)) {
		if($gallery_lists) {
			$list_galleries = explode(', ', $gallery_lists);
			foreach($list_galleries as $gallery) { delete_option($gallery); }
		}
		delete_option($portfolio_gallery);
	}
	$gallery_option_lists = get_option($portfolio_gallery.'_options');
	$gallery_category_lists = get_option($portfolio_gallery.'_category');
	if(isset($gallery_option_lists)) { delete_option($portfolio_gallery.'_options'); }
	if(isset($gallery_category_lists)) { delete_option($portfolio_gallery.'_category'); }
	$portfolio_gallery_lists = explode(', ', $portfolio_gallery_lists);
	$portfolio_gallery_diff = array_diff($portfolio_gallery_lists, array($portfolio_gallery));
	if($portfolio_gallery_diff) {
		$new_portfolio_gallery_lists = implode(', ', $portfolio_gallery_diff);
		update_option('portfolioTables', $new_portfolio_gallery_lists);
	} else {
		delete_option('portfolioTables');
	}
	die;
}
add_action( 'wp_ajax_nopriv_rcpig_delete_portfolio_gallery', 'rcpig_delete_portfolio_gallery' );
add_action( 'wp_ajax_rcpig_delete_portfolio_gallery', 'rcpig_delete_portfolio_gallery' );
function rcpig_process_portfolio_gallery() {
	$checkbox_value = $_POST['checkbox_value'] != '' ? $_POST['checkbox_value'] : 'add';
	$portfolio_gallery = $_POST['portfolio_gallery'] != '' ? $_POST['portfolio_gallery'] : '';
	$portfolio_gallery_name = $_POST['portfolio_gallery_name'] != '' ? trim(preg_replace('/[^A-Za-z0-9-\w_]+/', '_', sanitize_text_field( $_POST['portfolio_gallery_name'] ))) : $portfolio_gallery;
	$portfolio_gallery = rcpig_edit_portfolio_gallery($portfolio_gallery, $portfolio_gallery_name);
	$portfolio_text_values = array('cwidth' => 'container_width', 'maxpost' => 'max_post', 'asdes' => 'order_asc_desc', 'param' => 'sort_param', 'navpos' => 'navigation_align', 'twidth' => 'thumb_width', 'theight' => 'thumb_height', 'portd' => 'portfolio_align', 'ctitle' => 'carousel_title', 'cdescp' => 'carousel_descp', 'hvdir' => 'hover_direction', 'swrap' => 'show_wrapper', 'hvinv' => 'hover_inverse', 'hvdla' => 'hover_delay', 'imgeff' => 'hover_img_effect', 'htxeff' => 'hover_text_effect', 'hvreff' => 'hover_effect', 'expspd' => 'expand_speed', 'exphgt' => 'expand_height', 'flteff' => 'filter_effect', 'acfltr' => 'active_filter', 'infltr' => 'inactive_filter', 'infhvr' => 'inactive_filter_hover', 'phover' => 'portfolio_hover', 'phopc' => 'ph_opacity', 'thbclr' => 'thmb_hover_border', 'expclr' => 'expander_color', 'sldwrp' => 'slide_wrapper', 'rbclr' => 'rm_bg_color', 'rhclr' => 'rm_hv_color', 'acfont' => 'active_font', 'infont' => 'inactive_font', 'thfclr' => 'thmb_hover_font', 'ptfclr' => 'pt_font_color', 'pdfclr' => 'pd_font_color', 'rtclr' => 'rm_font_color', 'gfont' => 'google_font', 'gfinl' => 'initial_font', 'gfmnr' => 'minor_font', 'thfsz' => 'thumb_font_size', 'ptfsz' => 'pt_font_size', 'pdfsz' => 'pd_font_size', 'pbfsz' => 'pb_font_size', 'pbwidth' => 'pbutton_width', 'pbheight' => 'pbutton_height', 'pdesc' => 'port_desc', 'excln' => 'excerpt_length', 'exctx' => 'excerpt_text', 'rmfsz' => 'rm_font_size', 'tbspc' => 'tb_column_space', 'lrspc' => 'lr_column_space');
	foreach($portfolio_text_values as $key => $value) {
		if( isset( $_POST[$value] ) ) {
			$optionValue_text[$key] = sanitize_text_field( $_POST[$value] );
		}
	}
	$rcpig_option = isset($_POST['rcpig_option']) ? $_POST['rcpig_option'] : 'no';
	$portfolio_categories = $_POST['include_cats'] != '' ? $_POST['include_cats'] : '';
	$gallery_option = $portfolio_gallery.'_options';
	$portfolio_cats = $portfolio_gallery.'_category';
	$optionValue_check = array( 'enable' => $rcpig_option );
	$optionValue = array_merge($optionValue_text, $optionValue_check);
	if($portfolio_categories) {
		foreach($portfolio_categories as $cat => $category) {
			$tc = $cat+1;
			$selectCategory['cat-'.$tc] = $category;
		}
	}
	if($checkbox_value == 'add') {
		add_option($gallery_option, $optionValue);
		add_option($portfolio_cats, $selectCategory);
	} elseif($checkbox_value == 'update') {
		update_option($gallery_option, $optionValue);
		update_option($portfolio_cats, $selectCategory);
	} else {
		add_option($gallery_option, $optionValue);
		add_option($portfolio_cats, $selectCategory);
	}
}
function rcpig_copy_portfolio_gallery() {
	$portfolio_gallery = $_POST['portgallery'];
	if($portfolio_gallery) {
		$gallery_table = get_option('portfolioTables');
		$gallery_lists = explode(', ', $gallery_table);
		if(in_array($portfolio_gallery, $gallery_lists)) {
			$gallery_count = count($gallery_lists) + 1;
			$new_portfolio_gallery = $portfolio_gallery . '_' . $gallery_count . rand(1,1000);
			$portfolio_gallery_lists = $gallery_table . ', ' . $new_portfolio_gallery;
			update_option('portfolioTables', $portfolio_gallery_lists);
			$portfolio_category = get_option($portfolio_gallery.'_category');
			$new_portfolio_category = $new_portfolio_gallery . '_category';
			add_option($new_portfolio_category, $portfolio_category);
			$portfolio_option = get_option($portfolio_gallery.'_options');
			$new_portfolio_option = $new_portfolio_gallery . '_options';
			add_option($new_portfolio_option, $portfolio_option);
		}
	}
}
add_action( 'wp_ajax_nopriv_rcpig_copy_portfolio_gallery', 'rcpig_copy_portfolio_gallery' );
add_action( 'wp_ajax_rcpig_copy_portfolio_gallery', 'rcpig_copy_portfolio_gallery' );
?>