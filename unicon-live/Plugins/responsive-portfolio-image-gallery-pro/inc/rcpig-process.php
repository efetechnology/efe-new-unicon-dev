<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
$portfolio_table = get_option('portfolioTables'); ?>
<div class="wrap">
	<div id="add_new_portfolio" class="postbox-container">
	<h2 class="portfolio-header"><?php _e('Portfolio Gallery', 'rcpig'); ?> <a href="#" id="new_gallery" class="add-new-h2"><?php _e('Add New', 'rcpig'); ?></a><span id="rcpig-loading-image"></span></h2>
	<form id='rcpig_new' method="post" action="">
		<input type="hidden" name="new_portfolio_gallery" value="newgallery" />
		<div id="portfolionamediv">
			<div class="portfolionamewrap">
				<h3><?php _e('Portfolio Gallery Name', 'rcpig'); ?></h3>
				<input type="text" name="portfolio_gallery" size="30" value="" id="portfolio_gallery" autocomplete="off" placeholder="<?php _e('Enter Portfolio Gallery Name','rcpig'); ?>" required />
			</div>
			<input type="submit" id="rcpig_add_new" name="rcpig_add_new" class="button-primary" value="<?php _e('Add Gallery', 'rcpig'); ?>" />
		</div>
	</form><?php
	if($portfolio_table) { ?>
		<div class="gallery_list">
			<form id='rcpig_edit_form' method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="rcpig_edit_process" value="editprocess" />
				<table id="rcpig_list" class="form-table">
					<thead>
						<tr>
							<th><?php _e('ID', 'rcpig'); ?></th>
							<th><?php _e('Gallery Name', 'rcpig'); ?></th>
							<th><?php _e('Shortcode', 'rcpig'); ?></th>
							<th><?php _e('Visible', 'rcpig'); ?></th>
							<th><?php _e('Copy', 'rcpig'); ?></th>
						</tr>
					</thead><?php
					$gallery_lists = explode(', ', $portfolio_table);
					foreach($gallery_lists as $key => $list) {
						$list_item = ucwords(str_replace('_', ' ', $list));
						$portfolio_options = get_option($list.'_options');
						$galleryId = $key+1;
						if($portfolio_options != '') { ?>
							<tbody id="rcpig_<?php echo $list; ?>">
								<tr <?php if($galleryId % 2 == 0) { echo 'class="alt"'; } ?>>
									<td><?php echo $galleryId; ?></td>
									<td class="gallery_name" id="<?php echo $list; ?>">
										<div onclick="rcpigprocessportfolios('<?php echo $list; ?>', 'rcpig_modify_portfolio_gallery')"><?php echo $list_item; ?></div>
										<span id="edit_portfolio" onclick="rcpigprocessportfolios('<?php echo $list; ?>', 'rcpig_modify_portfolio_gallery')"><?php _e('Edit Gallery', 'rcpig'); ?></span>
										<span id="view_portfolio" onclick="rcpigpreviewgallery(<?php echo $galleryId; ?>, '<?php echo $list; ?>')"><?php _e('Preview', 'rcpig'); ?></span>
										<span id="remTable" onclick="rcpigdeletegallery('<?php echo $list; ?>')"><?php _e('Delete', 'rcpig'); ?></span>
									</td>
									<td><input type="text" name="rcpig_shortcode" class="rcpig_shortcode" value="<?php echo esc_html('[rcpig-gallery pid="'.$galleryId.'"]'); ?>" /></td>
									<td><?php if($portfolio_options['enable'] == 'yes') {_e('Yes', 'rcpig');} else {_e('No', 'rcpig');} ?></td>
									<td><div class="duplicate_gallery" onclick="rcpigcopygallery('<?php echo $list; ?>')"></div></td>
								</tr>
							</tbody><?php
						} else { ?>
							<tbody id="rcpig_<?php echo $list; ?>">
								<tr <?php if($galleryId % 2 == 0) { echo 'class="alt"'; } ?>>
									<td><?php echo $galleryId; ?></td>
									<td class="gallery_name" id="<?php echo $list; ?>">
										<div onclick="rcpigprocessportfolios('<?php echo $list; ?>', 'rcpig_add_portfolio_gallery')"><?php echo $list_item; ?></div>
										<span id="add_portfolio" onclick="rcpigprocessportfolios('<?php echo $list; ?>', 'rcpig_add_portfolio_gallery')"><?php _e('Set Gallery', 'rcpig'); ?></span>
										<span id="remTable" onclick="rcpigdeletegallery('<?php echo $list; ?>')"><?php _e('Delete', 'rcpig'); ?></span>
									</td>
									<td class="rcpig_notice"><span><?php _e('Mouseover on the gallery name in the left and clicked on <strong>Set Gallery</strong> link. After setting portfolio gallery you will get the <strong>SHORTCODE</strong> here.', 'rcpig'); ?></span></td>
									<td><?php _e('No', 'rcpig'); ?></td>
									<td><span class="no_duplicate"></span></td>
								</tr>
							</tbody><?php
						}
					} ?>
				</table>
			</form>
		</div><?php
	} else { ?>
		<div class="gallery_list">
			<p class="get_started"><?php _e('You didn\'t add any portfolio gallery yet! Click on <strong>Add New</strong> button to get started. If you feel trouble to understand how to create portfolio gallery, then follow the documentation that included with the attached zip file.<br /><br />If you have any questions that are beyond the scope of the help documentation, please feel free to open a ticket to any of our support team <a href="http://www.realwebcare.com/submitticket.php" target="_blank">here</a>. We&#39;d be happy to help you.', 'rcpig'); ?></p>
		</div><?php
	} ?>
	</div><!-- End postbox-container --><?php
	rcpig_sidebar(); ?>
</div>