<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function rcpig_modify_portfolio_gallery() {
	$portfolio_gallery = $_POST['portgallery'];
	$portfolio_gallery_name = ucwords(str_replace('_', ' ', $portfolio_gallery));
	$portfolio_options = get_option($portfolio_gallery.'_options');
	$portfolio_cats = get_option($portfolio_gallery.'_category');
	$checkValue = 'update';
	$rcpig_includes = rcpig_include_categories(); ?>
	<div id="settingportfoliodiv">
		<div id="tabs">
			<ul>
				<li><a href="#general"><?php _e('General Settings', 'rcpig'); ?></a></li>
				<li><a href="#structure"><?php _e('Structure Settings', 'rcpig'); ?></a></li>
				<li><a href="#font"><?php _e('Font Settings', 'rcpig'); ?></a></li>
				<li><a href="#color"><?php _e('Color Settings', 'rcpig'); ?></a></li>
				<li><a href="#advance"><?php _e('Advanced Settings', 'rcpig'); ?></a></li>
			</ul>

			<div id="general" class="advance-input">
				<label class="input-check"><?php _e('Enable Portfolio Gallery', 'rcpig'); ?>:
				<input type="checkbox" name="rcpig_option" class="tickbox" id="rcpig_option" value="yes" <?php if($portfolio_options['enable'] == 'yes') { ?> checked="checked" <?php } ?> /></label>
				<label id="modify-table" class="input-title"><?php _e('Modify Portfolio Gallery Name', 'rcpig'); ?></label>
				<input type="text" name="portfolio_gallery_name" class="medium" id="portfolio_gallery_name" value="<?php echo $portfolio_gallery_name; ?>" />
				<label class="input-title"><?php _e('Number of Post', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Number of post to show. Default -1, means show all.', 'rcpig'); ?>"></a></label>
				<input type="number" name="max_post" class="medium" id="max_post" value="<?php echo $portfolio_options['maxpost']; ?>" min="-1" max="100" placeholder="e.g. 10" />
				<label class="input-title"><?php _e('Select posts order in', 'rcpig'); ?>:<a href="#" class="rcpig_tooltip" rel="<?php _e('Designates the ascending or descending order of the \'orderby\' parameter. Defaults to \'DESC\'. Ascending order from lowest to highest values (1, 2, 3; a, b, c). Descending order from highest to lowest values (3, 2, 1; c, b, a).', 'rcpig'); ?>"></a></label>
				<select name="order_asc_desc" id="order_asc_desc" class="port-dir">
					<?php if($portfolio_options['asdes'] == 'DESC') { ?>
					<option value="ASC"><?php _e('Ascending', 'rcpig'); ?></option>
					<option value="DESC" selected="selected"><?php _e('Descending', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="ASC" selected="selected"><?php _e('Ascending', 'rcpig'); ?></option>
					<option value="DESC"><?php _e('Descending', 'rcpig'); ?></option>
					<?php } ?>
				</select>
				<label class="input-title"><?php _e('Display posts sorted by', 'rcpig'); ?>:<a href="#" class="rcpig_tooltip" rel="<?php _e('Sort retrieved posts by parameter. Defaults to \'date (post_date)\'.', 'rcpig'); ?>"></a></label>
				<select name="sort_param" id="sort_param" class="port-dir">
					<?php if($portfolio_options['param'] == 'ID') { ?>
					<option value="ID" selected="selected"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['param'] == 'author') { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author" selected="selected"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['param'] == 'title') { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title" selected="selected"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['param'] == 'name') { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name" selected="selected"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['param'] == 'modified') { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified" selected="selected"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['param'] == 'parent') { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent" selected="selected"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['param'] == 'rand') { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand" selected="selected"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['param'] == 'comment_count') { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count" selected="selected"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date" selected="selected"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
					<?php } ?>
				</select><hr />
				<label class="input-title-check"><?php _e('Include Taxonomies', 'rcpig'); ?>:<a href="#" class="rcpig_tooltip" rel="<?php _e('You can include selected Taxonomies from portfolio.', 'rcpig'); ?>"></a></label>
				<div class="include_taxo"><?php
				foreach($rcpig_includes as $key => $include) { ?>
					<label for="<?php echo $key; ?>" class="taxoinclude"><input type="checkbox" name="include_cats[]" class="catbox" id="include_cats" value="<?php echo $include; ?>" <?php if(in_array($include, $portfolio_cats)) { ?> checked="checked"  <?php } ?> /><?php echo $include; ?></label><?php
				} ?>
				</div><hr />
				<label class="input-title"><?php _e('Display Portfolio Description', 'rcpig'); ?>:</label>
				<select name="port_desc" id="port_desc" class="port-dir">
					<?php if($portfolio_options['pdesc'] == 'true') { ?>
					<option value="true" id="desc_on" selected="selected"><?php _e('Yes', 'rcpig'); ?></option>
					<option value="false"><?php _e('No', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="true" id="desc_on"><?php _e('Yes', 'rcpig'); ?></option>
					<option value="false" selected="selected"><?php _e('No', 'rcpig'); ?></option>
					<?php } ?>
				</select>
				<div id="portfolio_desc_setup">
					<label class="input-title"><?php _e('Enter Excerpt Length', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Number of words to show for portfolio description.', 'rcpig'); ?>"></a></label>
					<input type="number" name="excerpt_length" class="medium" id="excerpt_length" value="<?php echo $portfolio_options['excln']; ?>" min="10" max="100" placeholder="e.g. 30" />
					<label class="input-title"><?php _e('Enter Read More Text', 'rcpig'); ?></label>
					<input type="text" name="excerpt_text" class="medium" id="excerpt_text" value="<?php echo $portfolio_options['exctx']; ?>" placeholder="e.g. Read More" />
					<label class="input-title"><?php _e('Display Carouel Image Description', 'rcpig'); ?>:</label>
					<select name="carousel_descp" id="carousel_descp" class="port-dir">
						<?php if($portfolio_options['cdescp'] == 'yes') { ?>
						<option value="yes" selected="selected"><?php _e('Yes', 'rcpig'); ?></option>
						<option value="no"><?php _e('No', 'rcpig'); ?></option>
						<?php } else { ?>
						<option value="yes"><?php _e('Yes', 'rcpig'); ?></option>
						<option value="no" selected="selected"><?php _e('No', 'rcpig'); ?></option>
						<?php } ?>
					</select>
				</div><hr />
				<label class="input-title"><?php _e('Display Carouel Image Title', 'rcpig'); ?>:</label>
				<select name="carousel_title" id="carousel_title" class="port-dir">
					<?php if($portfolio_options['ctitle'] == 'yes') { ?>
					<option value="yes" selected="selected"><?php _e('Yes', 'rcpig'); ?></option>
					<option value="no"><?php _e('No', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="yes"><?php _e('Yes', 'rcpig'); ?></option>
					<option value="no" selected="selected"><?php _e('No', 'rcpig'); ?></option>
					<?php } ?>
				</select>
			</div>

			<div id="structure" class="advance-input">
				<label class="input-title"><?php _e('Portfolio Gallery Container Width', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Enter the total width of your portfolio gallery here.', 'rcpig'); ?>"></a></label>
				<input type="text" name="container_width" class="medium" id="container_width" value="<?php echo $portfolio_options['cwidth']; ?>" placeholder="e.g. 100%" />
				<label class="input-title"><?php _e('Portfolio Navigation Alignment', 'rcpig'); ?>:</label>
				<select name="navigation_align" id="navigation_align" class="port-dir">
					<?php if($portfolio_options['navpos'] == 'left') { ?>
					<option value="left" selected="selected"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center"><?php _e('Center', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['navpos'] == 'right') { ?>
					<option value="left"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right" selected="selected"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center"><?php _e('Center', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="left"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center" selected="selected"><?php _e('Center', 'rcpig'); ?></option>
					<?php } ?>
				</select><hr />
				<label class="input-title"><?php _e('Portfolio Thumbnail Width in px', 'rcpig'); ?></label>
				<input type="text" name="thumb_width" class="medium" id="thumb_width" value="<?php echo $portfolio_options['twidth']; ?>" placeholder="e.g. 200px" />
				<label class="input-title"><?php _e('Portfolio Thumbnail Height in px', 'rcpig'); ?></label>
				<input type="text" name="thumb_height" class="medium" id="thumb_height" value="<?php echo $portfolio_options['theight']; ?>" placeholder="e.g. 200px" />
				<label class="input-title"><?php _e('Portfolio Thumbnail Alignment', 'rcpig'); ?>:</label>
				<select name="portfolio_align" id="portfolio_align" class="port-dir">
					<?php if($portfolio_options['portd'] == 'left') { ?>
					<option value="left" selected="selected"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center"><?php _e('Center', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['portd'] == 'right') { ?>
					<option value="left"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right" selected="selected"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center"><?php _e('Center', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="left"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center" selected="selected"><?php _e('Center', 'rcpig'); ?></option>
					<?php } ?>
				</select><hr />
				<label class="input-title"><?php _e('Portfolio Button Width', 'rcpig'); ?></label>
				<input type="text" name="pbutton_width" class="medium" id="pbutton_width" value="<?php echo $portfolio_options['pbwidth']; ?>" placeholder="e.g. 120px" />
				<label class="input-title"><?php _e('Portfolio Button Height', 'rcpig'); ?></label>
				<input type="text" name="pbutton_height" class="medium" id="pbutton_height" value="<?php echo $portfolio_options['pbheight']; ?>" placeholder="e.g. 34px" /><hr />
				<label class="input-title"><?php _e('Space Between Thumbnails (Top-Bottom)', 'rcpig'); ?></label>
				<input type="text" name="tb_column_space" class="medium" id="tb_column_space" value="<?php echo $portfolio_options['tbspc']; ?>" placeholder="e.g. 10px" />
				<label class="input-title"><?php _e('Space Between Thumbnails (Left-Right)', 'rcpig'); ?></label>
				<input type="text" name="lr_column_space" class="medium" id="lr_column_space" value="<?php echo $portfolio_options['lrspc']; ?>" placeholder="e.g. 5px" />
			</div>

			<div id="font" class="advance-input">
				<label class="input-title"><?php _e('Enter <a href="https://www.google.com/fonts" target="_blank">Google Fonts</a> URL link', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Find a Google font that you\'d like to use on your gallery. When you find the font that you like, click on the \'Quick-use\' icon. \'Quick-use\' icon will redirect you to the selected font page where you\'ll be asked to choose the styles and character sets for your selected font. Then you will find a generated code in the \'Standard\' tab. You don\'t need to copy the whole code. You just need to add an URL link without http: or https: (e.g. //fonts.googleapis.com/css?family=Roboto+Condensed:400,700)', 'rcpig'); ?>"></a></label>
				<input type="text" name="google_font" class="medium" id="google_font" value="<?php echo $portfolio_options['gfont']; ?>" placeholder="e.g. //fonts.googleapis.com/css?family=Roboto+Condensed:400,700" />
				<label class="input-title"><?php _e('Enter Primary Font Family', 'rcpig'); ?></label>
				<input type="text" name="initial_font" class="medium" id="initial_font" value="<?php echo $portfolio_options['gfinl']; ?>" placeholder="e.g. 'Roboto Condensed', serif" />
				<label class="input-title"><?php _e('Enter Secondary Font Family', 'rcpig'); ?></label>
				<input type="text" name="minor_font" class="medium" id="minor_font" value="<?php echo $portfolio_options['gfmnr']; ?>" placeholder="e.g. 'Open Sans', sans-serif" /><hr />
				<label class="input-title"><?php _e('Portfolio Thumbnail Hover Font Size', 'rcpig'); ?></label>
				<input type="text" name="thumb_font_size" class="medium" id="thumb_font_size" value="<?php echo $portfolio_options['thfsz']; ?>" placeholder="e.g. 14px" />
				<label class="input-title"><?php _e('Portfolio Title Font Size', 'rcpig'); ?></label>
				<input type="text" name="pt_font_size" class="medium" id="pt_font_size" value="<?php echo $portfolio_options['ptfsz']; ?>" placeholder="e.g. 26px" />
				<label class="input-title"><?php _e('Portfolio Description Font Size', 'rcpig'); ?></label>
				<input type="text" name="pd_font_size" class="medium" id="pd_font_size" value="<?php echo $portfolio_options['pdfsz']; ?>" placeholder="e.g. 13px" /><hr />
				<label class="input-title"><?php _e('Portfolio Button Font Size', 'rcpig'); ?></label>
				<input type="text" name="pb_font_size" class="medium" id="pb_font_size" value="<?php echo $portfolio_options['pbfsz']; ?>" placeholder="e.g. 14px" />
				<label class="input-title"><?php _e('Enter Read More Font Size', 'rcpig'); ?></label>
				<input type="text" name="rm_font_size" class="medium" id="rm_font_size" value="<?php echo $portfolio_options['rmfsz']; ?>" placeholder="e.g. 12px" />
			</div>

			<div id="color" class="advance-input">
				<table>
					<!--Background Color -->
					<tr class="table-header">
						<td colspan="2"><?php _e('Background Colors', 'rcpig'); ?></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Active Filter Button Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="active_filter" class="active_filter" id="active_filter" value="<?php echo $portfolio_options['acfltr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Inactive Filter Button Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="inactive_filter" class="inactive_filter" id="inactive_filter" value="<?php echo $portfolio_options['infltr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Inactive Filter Button Hover', 'rcpig'); ?></label></th>
						<td><input type="text" name="inactive_filter_hover" class="inactive_filter_hover" id="inactive_filter_hover" value="<?php echo $portfolio_options['infhvr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Hover Background', 'rcpig'); ?></label></th>
						<td><input type="text" name="portfolio_hover" class="portfolio_hover" id="portfolio_hover" value="<?php echo $portfolio_options['phover']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Hover Opacity', 'rcpig'); ?></label></th>
						<td><input type="number" name="ph_opacity" class="medium" id="ph_opacity" value="<?php echo $portfolio_options['phopc']; ?>" min="0" max="10" placeholder="e.g. 7" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Thumbnail Hover Border Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="thmb_hover_border" class="thmb_hover_border" id="thmb_hover_border" value="<?php echo $portfolio_options['thbclr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Expanding Preview Background Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="expander_color" class="expander_color" id="expander_color" value="<?php echo $portfolio_options['expclr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Slider Wrapper Background Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="slide_wrapper" class="slide_wrapper" id="slide_wrapper" value="<?php echo $portfolio_options['sldwrp']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Read More Button Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="rm_bg_color" class="rm_bg_color" id="rm_bg_color" value="<?php echo $portfolio_options['rbclr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Read More Hover Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="rm_hv_color" class="rm_hv_color" id="rm_hv_color" value="<?php echo $portfolio_options['rhclr']; ?>" /></td>
					</tr>
					<!--Font Color -->
					<tr class="table-header">
						<td colspan="2"><?php _e('Font Colors', 'rcpig'); ?></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Active Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="active_font" class="active_font" id="active_font" value="<?php echo $portfolio_options['acfont']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Inactive Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="inactive_font" class="inactive_font" id="inactive_font" value="<?php echo $portfolio_options['infont']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Thumbnail Hover Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="thmb_hover_font" class="thmb_hover_font" id="thmb_hover_font" value="<?php echo $portfolio_options['thfclr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Title Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="pt_font_color" class="pt_font_color" id="pt_font_color" value="<?php echo $portfolio_options['ptfclr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Description Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="pd_font_color" class="pd_font_color" id="pd_font_color" value="<?php echo $portfolio_options['pdfclr']; ?>" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Read More Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="rm_font_color" class="rm_font_color" id="rm_font_color" value="<?php echo $portfolio_options['rtclr']; ?>" /></td>
					</tr>
				</table>
			</div>
			<div id="advance" class="advance-input">
				<label class="input-title"><?php _e('Select Portfolio Filter Effect', 'rcpig'); ?>:</label>
				<select name="filter_effect" id="filter_effect" class="port-dir">
					<?php if($portfolio_options['flteff'] == 'moveup') { ?>
					<option value="none"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup" selected="selected"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['flteff'] == 'scaleup') { ?>
					<option value="none"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup" selected="selected"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['flteff'] == 'fallperspective') { ?>
					<option value="none"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective" selected="selected"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['flteff'] == 'fly') { ?>
					<option value="none"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly" selected="selected"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['flteff'] == 'flip') { ?>
					<option value="none"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip" selected="selected"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['flteff'] == 'helix') { ?>
					<option value="none"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix" selected="selected"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } elseif($portfolio_options['flteff'] == 'popup') { ?>
					<option value="none"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup" selected="selected"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="none" selected="selected"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
					<?php } ?>
				</select>
				<label class="input-title"><?php _e('Display Thumbnail Hover Overlay Effect', 'rcpig'); ?>:</label>
				<select name="hover_direction" id="hover_direction" class="port-dir">
					<?php if($portfolio_options['hvdir'] == 'true') { ?>
					<option value="true" id="hover_on" selected="selected"><?php _e('On', 'rcpig'); ?></option>
					<option value="false"><?php _e('Off', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="true" id="hover_on"><?php _e('On', 'rcpig'); ?></option>
					<option value="false" selected="selected"><?php _e('Off', 'rcpig'); ?></option>
					<?php } ?>
				</select>
				<div id="hover_overlay_option">
					<label class="input-title"><?php _e('Select Thumbnail Hover Overlay Effect', 'rcpig'); ?>:</label>
					<select name="hover_text_effect" id="hover_text_effect" class="port-dir">
						<?php if($portfolio_options['htxeff'] == 'effectb') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb" selected="selected"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effectc') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc" selected="selected"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effectd') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd" selected="selected"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effecte') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte" selected="selected"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effectf') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf" selected="selected"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effectg') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg" selected="selected"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effecth') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth" selected="selected"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effecti') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti" selected="selected"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effectj') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj" selected="selected"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['htxeff'] == 'effectk') { ?>
						<option value="effecta" id="effecta"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk" selected="selected"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } else { ?>
						<option value="effecta" id="effecta" selected="selected"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
						<?php } ?>
					</select>
				</div>
				<div id="effecta_hover_option">
					<label class="input-title"><?php _e('Thumbnail Hover Inverse', 'rcpig'); ?>:</label>
					<select name="hover_inverse" id="hover_inverse" class="port-dir">
						<?php if($portfolio_options['hvinv'] == 'true') { ?>
						<option value="true" selected="selected"><?php _e('On', 'rcpig'); ?></option>
						<option value="false"><?php _e('Off', 'rcpig'); ?></option>
						<?php } else { ?>
						<option value="true"><?php _e('On', 'rcpig'); ?></option>
						<option value="false" selected="selected"><?php _e('Off', 'rcpig'); ?></option>
						<?php } ?>
					</select>
					<label class="input-title"><?php _e('Thumbnail Hover Delay', 'rcpig'); ?></label>
					<input type="number" name="hover_delay" class="medium" id="hover_delay" value="<?php echo $portfolio_options['hvdla']; ?>" min="0" max="10000" placeholder="e.g. 10" />
				</div>
				<label class="input-title"><?php _e('Display Thumbnail Hover Image Effect', 'rcpig'); ?>:</label>
				<select name="hover_img_effect" id="hover_img_effect" class="port-dir">
					<?php if($portfolio_options['imgeff'] == 'true') { ?>
					<option value="true" id="img_effect" selected="selected"><?php _e('On', 'rcpig'); ?></option>
					<option value="false"><?php _e('Off', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="true" id="img_effect"><?php _e('On', 'rcpig'); ?></option>
					<option value="false" selected="selected"><?php _e('Off', 'rcpig'); ?></option>
					<?php } ?>
				</select>
				<div id="hover_image_effect">
					<label class="input-title"><?php _e('Select Thumbnail Hover Image Effect', 'rcpig'); ?>:</label>
					<select name="hover_effect" id="hover_effect" class="port-dir">
						<?php if($portfolio_options['hvreff'] == 'zoompan') { ?>
						<option value="none"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan" selected="selected"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph"><?php _e('Morph into Circle', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['hvreff'] == 'slideout') { ?>
						<option value="none"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout" selected="selected"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph"><?php _e('Morph into Circle', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['hvreff'] == 'shrinkrotate') { ?>
						<option value="none"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate" selected="selected"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph"><?php _e('Morph into Circle', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['hvreff'] == 'zoomhide') { ?>
						<option value="none"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide" selected="selected"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph"><?php _e('Morph into Circle', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['hvreff'] == 'shrink') { ?>
						<option value="none"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink" selected="selected"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph"><?php _e('Morph into Circle', 'rcpig'); ?></option>
						<?php } elseif($portfolio_options['hvreff'] == 'morph') { ?>
						<option value="none"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph" selected="selected"><?php _e('Morph into Circle', 'rcpig'); ?></option>
						<?php } else { ?>
						<option value="none" selected="selected"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph"><?php _e('Morph into Circle', 'rcpig'); ?></option>
						<?php } ?>
					</select>
				</div>
				<label class="input-title"><?php _e('Show Multiple Image Carousel', 'rcpig'); ?>:</label>
				<select name="show_wrapper" id="show_wrapper" class="port-dir">
					<?php if($portfolio_options['swrap'] == 'true') { ?>
					<option value="true" selected="selected"><?php _e('Show', 'rcpig'); ?></option>
					<option value="false"><?php _e('Hide', 'rcpig'); ?></option>
					<?php } else { ?>
					<option value="true"><?php _e('Show', 'rcpig'); ?></option>
					<option value="false" selected="selected"><?php _e('Hide', 'rcpig'); ?></option>
					<?php } ?>
				</select>
				<label class="input-title"><?php _e('Expanding Preview Speed', 'rcpig'); ?></label>
				<input type="number" name="expand_speed" class="medium" id="expand_speed" value="<?php echo $portfolio_options['expspd']; ?>" min="0" max="10000" placeholder="e.g. 500" />
				<label class="input-title"><?php _e('Expanding Preview Height', 'rcpig'); ?></label>
				<input type="number" name="expand_height" class="medium" id="expand_height" value="<?php echo $portfolio_options['exphgt']; ?>" min="0" max="10000" placeholder="e.g. 500" />
			</div>
		</div>
	</div>	<!--settingportfoliodiv -->
	<div class="rcpig-clear"></div>
	<input type="hidden" name="portfolio_gallery" value="<?php echo $portfolio_gallery; ?>" />
	<input type="hidden" name="checkbox_value" value="<?php echo $checkValue; ?>" />
	<input type="submit" id="rcpig_edit" name="rcpig_edit" class="button-primary" value="<?php _e('Update Gallery', 'rcpig'); ?>" />
<?php
	die;
}
add_action( 'wp_ajax_nopriv_rcpig_modify_portfolio_gallery', 'rcpig_modify_portfolio_gallery' );
add_action( 'wp_ajax_rcpig_modify_portfolio_gallery', 'rcpig_modify_portfolio_gallery' );

if(isset($_POST['rcpig_edit_process']) && $_POST['rcpig_edit_process'] == "editprocess") {
	if( isset( $_POST['rcpig_edit'] ) ) { rcpig_process_portfolio_gallery(); }
}
?>