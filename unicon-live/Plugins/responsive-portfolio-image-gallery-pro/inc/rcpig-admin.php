<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit;
add_action( 'admin_menu', 'rcpig_register_menu' );
function rcpig_register_menu() {
	add_submenu_page( 'edit.php?post_type=rcpig', __( 'Generate portfolio','rcpig' ), __( 'Generate portfolio','rcpig' ), 'delete_posts', 'rcpig-process', 'rcpig_option_page' );
	add_submenu_page( 'edit.php?post_type=rcpig', __( 'Portfolio Settings','rcpig' ), __( 'Portfolio Settings','rcpig' ), 'delete_posts', 'rcpig-settings', 'rcpig_settings_config' );
}
add_action( 'admin_init', 'register_custom_css_setting' );
function register_custom_css_setting() {
	register_setting( 'rcpig-settings-css', 'custom_css', 'custom_css_validation');
}
function custom_css_validation( $input ) {
	if ( ! empty( $input['custom_css'] ) )
		$input['custom_css'] = trim( $input['custom_css'] );
	return $input;
}
add_action( 'admin_init', 'register_custom_option_setting' );
function register_custom_option_setting() {
	register_setting( 'rcpig-settings-group', 'single_template' );
	register_setting( 'rcpig-settings-group', 'archive_template' );
	register_setting( 'rcpig-settings-group', 'template_sidebar' );
}
function rcpig_option_page() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.', 'rcpig' ) );
	}
	include ( plugin_dir_path( __FILE__ ) . 'rcpig-process.php' );
}
function rcpig_settings_config() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.', 'rcpig' ) );
	}
	include ( plugin_dir_path( __FILE__ ) . 'rcpig-settings.php' );
}
//manage the columns of the "portfolio" post type
function rcpig_manage_columns_for_portfolio($columns) {
	//remove columns
	unset($columns['date']);
	//add new columns
	$columns['portfolio_featured_image'] = __('Portfolio Featured Image','rcpig');
	$columns['date'] = __('Date','rcpig');
	return $columns;
}
add_action('manage_rcpig_posts_columns','rcpig_manage_columns_for_portfolio');
//Populate custom columns for "portfolio" post type
function rcpig_populate_portfolio_columns($column,$post_id) {
	//featured image column
	if($column == 'portfolio_featured_image') {
		//if this portfolio has a featured image
		if(has_post_thumbnail($post_id)) {
			$portfolio_featured_image = get_the_post_thumbnail($post_id,array(100,100));
			echo $portfolio_featured_image;
		} else {
			echo __('This portfolio has no featured image','rcpig');
		}
	}
}
add_action('manage_rcpig_posts_custom_column','rcpig_populate_portfolio_columns',10,2);
if ( ! function_exists('rcpig_include_categories') ) {
	// Include selected categiry form portfolio.
	function rcpig_include_categories() {
		$terms = $category_link = array();
		$rcpig_post_type = 'rcpig';
		$taxonomy_objects = get_object_taxonomies( $rcpig_post_type, 'objects' );
		if( isset($taxonomy_objects) && !empty($taxonomy_objects) ) {
			$rcpig_taxonomy = 'rcpig-category';
			$terms = get_terms($rcpig_taxonomy);
			foreach ( $terms as $term ) {
				$category_link[$term->term_id] = $term->name;
			}
		}
		return $category_link;
	}
}
/* Convert hexdec color string to rgb(a) string */
function hex2rgba($color, $opacity = false) {
	$default = 'rgb(0,0,0)';
	//Return default if no color provided
	if(empty($color))
		return $default;

	//Sanitize $color if "#" is provided
	if ($color[0] == '#' ) {
		$color = substr( $color, 1 );
	}
	//Check if color has 6 or 3 characters and get values
	if (strlen($color) == 6) {
		$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
	} elseif ( strlen( $color ) == 3 ) {
		$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
	} else {
		return $default;
	}
	//Convert hexadec to rgb
	$rgb =  array_map('hexdec', $hex);
	//Check if opacity is set(rgba or rgb)
	if($opacity) {
		if(abs($opacity) > 1)
			$opacity = 1.0;

		$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
	} else {
		$output = 'rgb('.implode(",",$rgb).')';
	}
	//Return rgb(a) color string
	return $output;
}
function rcpig_adjustBrightness($hex, $steps) {
	$steps = max(-255, min(255, $steps));
	$hex = str_replace('#', '', $hex);
	if (strlen($hex) == 3) { $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2); }
	$r = hexdec(substr($hex,0,2));
	$g = hexdec(substr($hex,2,2));
	$b = hexdec(substr($hex,4,2));
	$r = max(0,min(255,$r + $steps));
	$g = max(0,min(255,$g + $steps));
	$b = max(0,min(255,$b + $steps));
	$r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
	$g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
	$b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);
	return '#'.$r_hex.$g_hex.$b_hex;
}
/* Custom Excerpts */
function rcpig_excerpt($length, $more='') {
	global $post;
	$rcpig_excerpt = explode(' ', get_the_content(), $length);
	if($more != '') {
		$rcpig_more = '<a class="rcpig-more" href="'. get_permalink($post->ID) . '">' . $more . '</a>';
	}
	else { $rcpig_more = ''; }
	if (count($rcpig_excerpt) >= $length) {
		array_pop($rcpig_excerpt);
		$rcpig_excerpt = implode(" ",$rcpig_excerpt);
		$rcpig_excerpt = sanitize_text_field($rcpig_excerpt);
		$rcpig_excerpt = $rcpig_excerpt . ' ...<br />' . $rcpig_more;
	} else {
		$rcpig_excerpt = implode(" ",$rcpig_excerpt);
		$rcpig_excerpt = sanitize_text_field($rcpig_excerpt);
	}
	$rcpig_excerpt = apply_filters('get_the_content', $rcpig_excerpt);
	$rcpig_excerpt = str_replace(']]>', ']]&gt;', $rcpig_excerpt);
	$rcpig_excerpt = str_replace("'", "\'", $rcpig_excerpt);
	return $rcpig_excerpt;
}
function wp_get_attachment( $attachment_id ) {
	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}
function rcpig_preview_portfolio_gallery() {
	$i = 1;
	$portfolio_gallery = $_POST['portgallery'];
	$galleryId = $_POST['galleryid']; ?>
	<style type="text/css">*,*:after,*:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0;margin:0}</style>
	<div id="gallerydisplaydiv">
		<h3><span class="button button-large" onclick="rcpigprocessportfolios('<?php echo $portfolio_gallery; ?>', 'rcpig_modify_portfolio_gallery')"><?php _e('Edit Gallery', 'rcpig'); ?></span></h3>
		<?php echo do_shortcode('[rcpig-gallery pid="'.$galleryId.'"]'); ?>
	</div>
<?php
	die;
}
add_action( 'wp_ajax_nopriv_rcpig_preview_portfolio_gallery', 'rcpig_preview_portfolio_gallery' );
add_action( 'wp_ajax_rcpig_preview_portfolio_gallery', 'rcpig_preview_portfolio_gallery' );
include ( RCPIG_PLUGIN_PATH . 'lib/process_gallery-option.php' );
include ( RCPIG_PLUGIN_PATH . 'inc/rcpig-add-gallery.php' );
include ( RCPIG_PLUGIN_PATH . 'inc/rcpig-edit-gallery.php' );
if(isset($_POST['new_portfolio_gallery']) && $_POST['new_portfolio_gallery'] == "newgallery") {
	if( isset( $_POST['rcpig_add_new'] ) ) { rcpig_add_new_gallery(); }
} if(isset($_POST['temp_page_width']) && !empty($_POST['temp_page_width'])) {
	update_option( 'temp_page_width',  $_POST['temp_page_width']);
} if(isset($_POST['temp_page_align']) && !empty($_POST['temp_page_align'])) {
	update_option( 'temp_page_align',  $_POST['temp_page_align']);
} if(isset($_POST['archive_img_width']) && !empty($_POST['archive_img_width'])) {
	update_option( 'archive_img_width',  $_POST['archive_img_width']);
} if(isset($_POST['archive_img_height']) && !empty($_POST['archive_img_height'])) {
	update_option( 'archive_img_height',  $_POST['archive_img_height']);
}if(isset($_POST['portfolio_columns']) && !empty($_POST['portfolio_columns'])) {
	update_option( 'portfolio_columns',  $_POST['portfolio_columns']);
} if(isset($_POST['portfolio_space']) && !empty($_POST['portfolio_space'])) {
	update_option( 'portfolio_space',  $_POST['portfolio_space']);
} if(isset($_POST['single_desc_limit']) && !empty($_POST['single_desc_limit'])) {
	update_option( 'single_desc_limit',  $_POST['single_desc_limit']);
} if(isset($_POST['archive_desc_limit']) && !empty($_POST['archive_desc_limit'])) {
	update_option( 'archive_desc_limit',  $_POST['archive_desc_limit']);
} if(isset($_POST['temp_pagination_align']) && !empty($_POST['temp_pagination_align'])) {
	update_option( 'temp_pagination_align',  $_POST['temp_pagination_align']);
}
?>