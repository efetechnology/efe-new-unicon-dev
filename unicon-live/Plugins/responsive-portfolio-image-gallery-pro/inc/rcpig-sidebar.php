<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function rcpig_sidebar() { ?>
	<div id="rcpig-sidebar" class="postbox-container">
		<div id="rcpigusage-info" class="rcpigusage-sidebar">
			<h3><?php _e('Plugin Info', 'rcpig'); ?></h3>
			<ul class="rcpigusage-list">
				<li><?php _e('Version: 1.0.1', 'rcpig'); ?></li>
				<li><?php _e('Requires: Wordpress 3.5+', 'rcpig'); ?></li>
				<li><?php _e('First release: September 03, 2016', 'rcpig'); ?></li>
				<li><?php _e('Last Update: October 01, 2016', 'rcpig'); ?></li>
				<li><?php _e('By', 'rcpig'); ?>: <a href="http://www.realwebcare.com/" target="_blank"><?php _e('Real Web Care', 'rcpig'); ?></a></li>
				<li><?php _e('Facebook', 'rcpig'); ?>: <a href="http://www.facebook.com/realwebcare" target="_blank"><?php _e('Realwebcare', 'rcpig'); ?></a></li>
			</ul>
		</div>
	</div><?php
}
add_action( 'rcpig_settings_content', 'rcpig_sidebar' );
?>