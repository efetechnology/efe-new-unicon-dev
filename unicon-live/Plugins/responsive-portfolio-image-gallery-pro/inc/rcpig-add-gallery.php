<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit;
function rcpig_add_portfolio_gallery() {
	$portfolio_gallery = $_POST['portgallery'];
	$portfolio_gallery_name = ucwords(str_replace('_', ' ', $portfolio_gallery));
	$checkValue = 'add';
	$rcpig_includes = rcpig_include_categories(); ?>
	<div id="settingportfoliodiv">
		<div id="tabs">
			<ul>
				<li><a href="#general"><?php _e('General Settings', 'rcpig'); ?></a></li>
				<li><a href="#structure"><?php _e('Structure Settings', 'rcpig'); ?></a></li>
				<li><a href="#font"><?php _e('Font Settings', 'rcpig'); ?></a></li>
				<li><a href="#color"><?php _e('Color Settings', 'rcpig'); ?></a></li>
				<li><a href="#advance"><?php _e('Advanced Settings', 'rcpig'); ?></a></li>
			</ul>

			<div id="general" class="advance-input">
				<label class="input-check"><?php _e('Enable Portfolio Gallery', 'rcpig'); ?>:
				<input type="checkbox" name="rcpig_option" class="tickbox" id="rcpig_option" value="yes" checked="checked" /></label>
				<label id="modify-table" class="input-title"><?php _e('Modify Portfolio Gallery Name', 'rcpig'); ?></label>
				<input type="text" name="portfolio_gallery_name" class="medium" id="portfolio_gallery_name" value="<?php echo $portfolio_gallery_name; ?>" />
				<label class="input-title"><?php _e('Number of Post', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Number of post to show. Default -1, means show all.', 'rcpig'); ?>"></a></label>
				<input type="number" name="max_post" class="medium" id="max_post" value="-1" min="-1" max="100" placeholder="e.g. 10" />
				<label class="input-title"><?php _e('Select posts order in', 'rcpig'); ?>:<a href="#" class="rcpig_tooltip" rel="<?php _e('Designates the ascending or descending order of the \'orderby\' parameter. Defaults to \'DESC\'. Ascending order from lowest to highest values (1, 2, 3; a, b, c). Descending order from highest to lowest values (3, 2, 1; c, b, a).', 'rcpig'); ?>"></a></label>
				<select name="order_asc_desc" id="order_asc_desc" class="port-dir">
					<option value="ASC"><?php _e('Ascending', 'rcpig'); ?></option>
					<option value="DESC" selected="selected"><?php _e('Descending', 'rcpig'); ?></option>
				</select>
				<label class="input-title"><?php _e('Display posts sorted by', 'rcpig'); ?>:<a href="#" class="rcpig_tooltip" rel="<?php _e('Sort retrieved posts by parameter. Defaults to \'date (post_date)\'.', 'rcpig'); ?>"></a></label>
				<select name="sort_param" id="sort_param" class="port-dir">
					<option value="ID"><?php _e('Post ID', 'rcpig'); ?></option>
					<option value="author"><?php _e('Author', 'rcpig'); ?></option>
					<option value="title"><?php _e('Title', 'rcpig'); ?></option>
					<option value="name"><?php _e('Post Name (post slug)', 'rcpig'); ?></option>
					<option value="date" selected="selected"><?php _e('Post Date', 'rcpig'); ?></option>
					<option value="modified"><?php _e('Last Modified Date', 'rcpig'); ?></option>
					<option value="parent"><?php _e('Post/Page Parent ID', 'rcpig'); ?></option>
					<option value="rand"><?php _e('Random', 'rcpig'); ?></option>
					<option value="comment_count"><?php _e('Comment Count', 'rcpig'); ?></option>
				</select><hr />
				<label class="input-title-check"><?php _e('Include Taxonomies', 'rcpig'); ?>:<a href="#" class="rcpig_tooltip" rel="<?php _e('You can include selected Taxonomies from portfolio.', 'rcpig'); ?>"></a></label>
				<div class="include_taxo"><?php
				foreach($rcpig_includes as $key => $include) { ?>
					<label for="<?php echo $include; ?>" class="taxoinclude"><input type="checkbox" name="include_cats[]" class="catbox" id="include_cats" value="<?php echo $include; ?>" /><?php echo $include; ?></label><?php
				} ?>
				</div><hr />
				<label class="input-title"><?php _e('Display Portfolio Description', 'rcpig'); ?>:</label>
				<select name="port_desc" id="port_desc" class="port-dir">
					<option value="true" id="desc_on" selected="selected"><?php _e('Yes', 'rcpig'); ?></option>
					<option value="false"><?php _e('No', 'rcpig'); ?></option>
				</select>
				<div id="portfolio_desc_setup">
					<label class="input-title"><?php _e('Enter Excerpt Length', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Number of words to show for portfolio description.', 'rcpig'); ?>"></a></label>
					<input type="number" name="excerpt_length" class="medium" id="excerpt_length" value="30" min="10" max="100" placeholder="e.g. 30" />
					<label class="input-title"><?php _e('Enter Read More Text', 'rcpig'); ?></label>
					<input type="text" name="excerpt_text" class="medium" id="excerpt_text" value="" placeholder="e.g. Read More" />
					<label class="input-title"><?php _e('Display Carouel Image Description', 'rcpig'); ?>:</label>
					<select name="carousel_descp" id="carousel_descp" class="port-dir">
						<option value="yes"><?php _e('Yes', 'rcpig'); ?></option>
						<option value="no" selected="selected"><?php _e('No', 'rcpig'); ?></option>
					</select>
				</div><hr />
				<label class="input-title"><?php _e('Display Carouel Image Title', 'rcpig'); ?>:</label>
				<select name="carousel_title" id="carousel_title" class="port-dir">
					<option value="yes"><?php _e('Yes', 'rcpig'); ?></option>
					<option value="no" selected="selected"><?php _e('No', 'rcpig'); ?></option>
				</select>
			</div>

			<div id="structure" class="advance-input">
				<label class="input-title"><?php _e('Portfolio Gallery Container Width', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Enter the total width of your portfolio gallery here.', 'rcpig'); ?>"></a></label>
				<input type="text" name="container_width" class="medium" id="container_width" value="" placeholder="e.g. 100%" />
				<label class="input-title"><?php _e('Portfolio Navigation Alignment', 'rcpig'); ?>:</label>
				<select name="navigation_align" id="navigation_align" class="port-dir">
					<option value="left"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center" selected="selected"><?php _e('Center', 'rcpig'); ?></option>
				</select><hr />
				<label class="input-title"><?php _e('Portfolio Thumbnail Width in px', 'rcpig'); ?></label>
				<input type="text" name="thumb_width" class="medium" id="thumb_width" value="" placeholder="e.g. 200px" />
				<label class="input-title"><?php _e('Portfolio Thumbnail Height in px', 'rcpig'); ?></label>
				<input type="text" name="thumb_height" class="medium" id="thumb_height" value="" placeholder="e.g. 200px" />
				<label class="input-title"><?php _e('Portfolio Thumbnail Alignment', 'rcpig'); ?>:</label>
				<select name="portfolio_align" id="portfolio_align" class="port-dir">
					<option value="left"><?php _e('Left', 'rcpig'); ?></option>
					<option value="right"><?php _e('Right', 'rcpig'); ?></option>
					<option value="center" selected="selected"><?php _e('Center', 'rcpig'); ?></option>
				</select><hr />
				<label class="input-title"><?php _e('Portfolio Button Width', 'rcpig'); ?></label>
				<input type="text" name="pbutton_width" class="medium" id="pbutton_width" value="" placeholder="e.g. 120px" />
				<label class="input-title"><?php _e('Portfolio Button Height', 'rcpig'); ?></label>
				<input type="text" name="pbutton_height" class="medium" id="pbutton_height" value="" placeholder="e.g. 34px" /><hr />
				<label class="input-title"><?php _e('Space Between Thumbnails (Top-Bottom)', 'rcpig'); ?></label>
				<input type="text" name="tb_column_space" class="medium" id="tb_column_space" value="" placeholder="e.g. 10px" />
				<label class="input-title"><?php _e('Space Between Thumbnails (Left-Right)', 'rcpig'); ?></label>
				<input type="text" name="lr_column_space" class="medium" id="lr_column_space" value="" placeholder="e.g. 5px" />
			</div>

			<div id="font" class="advance-input">
				<label class="input-title"><?php _e('Enter <a href="https://www.google.com/fonts" target="_blank">Google Fonts</a> URL link', 'rcpig'); ?><a href="#" class="rcpig_tooltip" rel="<?php _e('Find a Google font that you\'d like to use on your gallery. When you find the font that you like, click on the \'Quick-use\' icon. \'Quick-use\' icon will redirect you to the selected font page where you\'ll be asked to choose the styles and character sets for your selected font. Then you will find a generated code in the \'Standard\' tab. You don\'t need to copy the whole code. You just need to add an URL link without http: or https: (e.g. //fonts.googleapis.com/css?family=Roboto+Condensed:400,700)', 'rcpig'); ?>"></a></label>
				<input type="text" name="google_font" class="medium" id="google_font" value="" placeholder="e.g. //fonts.googleapis.com/css?family=Roboto+Condensed:400,700" />
				<label class="input-title"><?php _e('Enter Primary Font Family', 'rcpig'); ?></label>
				<input type="text" name="initial_font" class="medium" id="initial_font" value="" placeholder="e.g. 'Roboto Condensed', serif" />
				<label class="input-title"><?php _e('Enter Secondary Font Family', 'rcpig'); ?></label>
				<input type="text" name="minor_font" class="medium" id="minor_font" value="" placeholder="e.g. 'Open Sans', sans-serif" /><hr />
				<label class="input-title"><?php _e('Portfolio Thumbnail Hover Font Size', 'rcpig'); ?></label>
				<input type="text" name="thumb_font_size" class="medium" id="thumb_font_size" value="" placeholder="e.g. 14px" />
				<label class="input-title"><?php _e('Portfolio Title Font Size', 'rcpig'); ?></label>
				<input type="text" name="pt_font_size" class="medium" id="pt_font_size" value="" placeholder="e.g. 26px" />
				<label class="input-title"><?php _e('Portfolio Description Font Size', 'rcpig'); ?></label>
				<input type="text" name="pd_font_size" class="medium" id="pd_font_size" value="" placeholder="e.g. 13px" /><hr />
				<label class="input-title"><?php _e('Portfolio Button Font Size', 'rcpig'); ?></label>
				<input type="text" name="pb_font_size" class="medium" id="pb_font_size" value="" placeholder="e.g. 14px" />
				<label class="input-title"><?php _e('Enter Read More Font Size', 'rcpig'); ?></label>
				<input type="text" name="rm_font_size" class="medium" id="rm_font_size" value="" placeholder="e.g. 12px" />
			</div>

			<div id="color" class="advance-input">
				<table>
					<!--Background Color -->
					<tr class="table-header">
						<td colspan="2"><?php _e('Background Colors', 'rcpig'); ?></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Active Filter Button Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="active_filter" class="active_filter" id="active_filter" value="#666" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Inactive Filter Button Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="inactive_filter" class="inactive_filter" id="inactive_filter" value="#fff" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Inactive Filter Button Hover', 'rcpig'); ?></label></th>
						<td><input type="text" name="inactive_filter_hover" class="inactive_filter_hover" id="inactive_filter_hover" value="#EEEEEE" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Hover Background', 'rcpig'); ?></label></th>
						<td><input type="text" name="portfolio_hover" class="portfolio_hover" id="portfolio_hover" value="#333" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Hover Opacity', 'rcpig'); ?></label></th>
						<td><input type="number" name="ph_opacity" class="medium" id="ph_opacity" value="7" min="0" max="10" placeholder="e.g. 7" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Thumbnail Hover Border Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="thmb_hover_border" class="thmb_hover_border" id="thmb_hover_border" value="#fff" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Expanding Preview Background Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="expander_color" class="expander_color" id="expander_color" value="#222" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Slider Wrapper Background Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="slide_wrapper" class="slide_wrapper" id="slide_wrapper" value="#313131" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Read More Button Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="rm_bg_color" class="rm_bg_color" id="rm_bg_color" value="#262626" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Read More Hover Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="rm_hv_color" class="rm_hv_color" id="rm_hv_color" value="#3A3A3A" /></td>
					</tr>
					<!--Font Color -->
					<tr class="table-header">
						<td colspan="2"><?php _e('Font Colors', 'rcpig'); ?></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Active Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="active_font" class="active_font" id="active_font" value="#fff" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Inactive Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="inactive_font" class="inactive_font" id="inactive_font" value="#444" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Thumbnail Hover Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="thmb_hover_font" class="thmb_hover_font" id="thmb_hover_font" value="#fff" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Title Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="pt_font_color" class="pt_font_color" id="pt_font_color" value="#d6d6d6" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Portfolio Description Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="pd_font_color" class="pd_font_color" id="pd_font_color" value="#999" /></td>
					</tr>
					<tr class="table-input">
						<th><label class="input-title"><?php _e('Read More Font Color', 'rcpig'); ?></label></th>
						<td><input type="text" name="rm_font_color" class="rm_font_color" id="rm_font_color" value="#9f9f9f" /></td>
					</tr>
				</table>
			</div>

			<div id="advance" class="advance-input">
				<label class="input-title"><?php _e('Select Portfolio Filter Effect', 'rcpig'); ?>:</label>
				<select name="filter_effect" id="filter_effect" class="port-dir">
					<option value="none" selected="selected"><?php _e('None', 'rcpig'); ?></option>
					<option value="moveup"><?php _e('Move Up', 'rcpig'); ?></option>
					<option value="scaleup"><?php _e('Scale Up', 'rcpig'); ?></option>
					<option value="fallperspective"><?php _e('Fall Perspective', 'rcpig'); ?></option>
					<option value="fly"><?php _e('Fly', 'rcpig'); ?></option>
					<option value="flip"><?php _e('Flip', 'rcpig'); ?></option>
					<option value="helix"><?php _e('Helix', 'rcpig'); ?></option>
					<option value="popup"><?php _e('Popup', 'rcpig'); ?></option>
				</select>
				<label class="input-title"><?php _e('Display Thumbnail Hover Overlay Effect', 'rcpig'); ?>:</label>
				<select name="hover_direction" id="hover_direction" class="port-dir">
					<option value="true" id="hover_on" selected="selected"><?php _e('On', 'rcpig'); ?></option>
					<option value="false"><?php _e('Off', 'rcpig'); ?></option>
				</select>
				<div id="hover_overlay_option">
					<label class="input-title"><?php _e('Select Thumbnail Hover Overlay Effect', 'rcpig'); ?>:</label>
					<select name="hover_text_effect" id="hover_text_effect" class="port-dir">
						<option value="effecta" id="effecta" selected="selected"><?php _e('Hover Effect 1', 'rcpig'); ?></option>
						<option value="effectb"><?php _e('Hover Effect 2', 'rcpig'); ?></option>
						<option value="effectc"><?php _e('Hover Effect 3', 'rcpig'); ?></option>
						<option value="effectd"><?php _e('Hover Effect 4', 'rcpig'); ?></option>
						<option value="effecte"><?php _e('Hover Effect 5', 'rcpig'); ?></option>
						<option value="effectf"><?php _e('Hover Effect 6', 'rcpig'); ?></option>
						<option value="effectg"><?php _e('Hover Effect 7', 'rcpig'); ?></option>
						<option value="effecth"><?php _e('Hover Effect 8', 'rcpig'); ?></option>
						<option value="effecti"><?php _e('Hover Effect 9', 'rcpig'); ?></option>
						<option value="effectj"><?php _e('Hover Effect 10', 'rcpig'); ?></option>
						<option value="effectk"><?php _e('Hover Effect 11', 'rcpig'); ?></option>
					</select>
				</div>
				<div id="effecta_hover_option">
					<label class="input-title"><?php _e('Thumbnail Hover Inverse', 'rcpig'); ?>:</label>
					<select name="hover_inverse" id="hover_inverse" class="port-dir">
						<option value="true"><?php _e('On', 'rcpig'); ?></option>
						<option value="false" selected="selected"><?php _e('Off', 'rcpig'); ?></option>
					</select>
					<label class="input-title"><?php _e('Thumbnail Hover Delay', 'rcpig'); ?></label>
					<input type="number" name="hover_delay" class="medium" id="hover_delay" value="0" min="0" max="10000" placeholder="e.g. 10" />
				</div>

				<label class="input-title"><?php _e('Display Thumbnail Hover Image Effect', 'rcpig'); ?>:</label>
				<select name="hover_img_effect" id="hover_img_effect" class="port-dir">
					<option value="true" id="img_effect" selected="selected"><?php _e('On', 'rcpig'); ?></option>
					<option value="false"><?php _e('Off', 'rcpig'); ?></option>
				</select>
				<div id="hover_image_effect">
					<label class="input-title"><?php _e('Select Thumbnail Hover Image Effect', 'rcpig'); ?>:</label>
					<select name="hover_effect" id="hover_effect" class="port-dir">
						<option value="none" selected="selected"><?php _e('None', 'rcpig'); ?></option>
						<option value="zoompan"><?php _e('Zoom and Pan', 'rcpig'); ?></option>
						<option value="slideout"><?php _e('Slide Out', 'rcpig'); ?></option>
						<option value="shrinkrotate"><?php _e('Shrink Rotate', 'rcpig'); ?></option>
						<option value="zoomhide"><?php _e('Zoom and Hide', 'rcpig'); ?></option>
						<option value="shrink"><?php _e('Shrink', 'rcpig'); ?></option>
						<option value="morph"><?php _e('Morph into Circle', 'rcpig'); ?></option>
					</select>
				</div>
				<label class="input-title"><?php _e('Show Multiple Image Carousel', 'rcpig'); ?>:</label>
				<select name="show_wrapper" id="show_wrapper" class="port-dir">
					<option value="true" selected="selected"><?php _e('Show', 'rcpig'); ?></option>
					<option value="false"><?php _e('Hide', 'rcpig'); ?></option>
				</select>
				<label class="input-title"><?php _e('Expanding Preview Speed', 'rcpig'); ?></label>
				<input type="number" name="expand_speed" class="medium" id="expand_speed" value="500" min="0" max="10000" placeholder="e.g. 500" />
				<label class="input-title"><?php _e('Expanding Preview Height', 'rcpig'); ?></label>
				<input type="number" name="expand_height" class="medium" id="expand_height" value="500" min="0" max="1000" placeholder="e.g. 500" />
			</div>
		</div>
	</div>	<!--settingportfoliodiv -->
	<div class="rcpig-clear"></div>
	<input type="hidden" name="portfolio_gallery" value="<?php echo $portfolio_gallery; ?>" />
	<input type="hidden" name="checkbox_value" value="<?php echo $checkValue; ?>" />
	<input type="submit" id="rcpig_add" name="rcpig_add" class="button-primary" value="<?php _e('Add Gallery', 'rcpig'); ?>" />
<?php
	die;
}
add_action( 'wp_ajax_nopriv_rcpig_add_portfolio_gallery', 'rcpig_add_portfolio_gallery' );
add_action( 'wp_ajax_rcpig_add_portfolio_gallery', 'rcpig_add_portfolio_gallery' );

if(isset($_POST['rcpig_edit_process']) && $_POST['rcpig_edit_process'] == "editprocess") {
	if( isset( $_POST['rcpig_add'] ) ) { rcpig_process_portfolio_gallery(); }
}
?>