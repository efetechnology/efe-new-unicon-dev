<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit; ?>
<div class="wrap">
	<h2><?php _e( 'Portfolio Gallery Settings', 'rcpig'); ?></h2><?php
	if(!empty($_GET['settings-updated'])) echo '<div id="message" class="updated"><p><strong>'.__( 'Portfolio Settings Updated.', 'rcpig' ).'</strong></p></div>'; ?>
	<div id="settings-tabs" class="rcpig_exip_settings" style="visibility:hidden">
		<ul>
			<li><a href="#global"><?php _e('Global Settings', 'rcpig'); ?></a></li>
			<li><a href="#cstyle"><?php _e('Custom Style', 'rcpig'); ?></a></li>
			<li><a href="#export"><?php _e('Export Gallery', 'rcpig'); ?></a></li>
			<li><a href="#import"><?php _e('Import Gallery', 'rcpig'); ?></a></li>
		</ul>
		
		<div id="global">
			<h2><?php _e( 'Template Settings' ); ?></h2>
			<form id="rcpig_template_form" method="post" action="options.php"><?php
				settings_fields( 'rcpig-settings-group' );
				$single_template = get_option( 'single_template' );
				$single_desc_limit = get_option( 'single_desc_limit' );
				$archive_template = get_option( 'archive_template' );
				$archive_desc_limit = get_option( 'archive_desc_limit' );
				$temp_pagination_align = get_option( 'temp_pagination_align' );
				$archive_img_width = get_option( 'archive_img_width' );
				$archive_img_height = get_option( 'archive_img_height' );
				$temp_page_align = get_option( 'temp_page_align' );
				$portfolio_columns = get_option( 'portfolio_columns' );
				$portfolio_space = get_option( 'portfolio_space' );
				$template_sidebar = get_option( 'template_sidebar' );
				$temp_page_width = get_option( 'temp_page_width' ); ?>
				<input type="hidden" name="rcpig_process" value="process" />
				<div class="rcpig_options">
					<label class="input-check"><?php _e('Display Custom Template for Single Portfolio Page', 'rcpig'); ?>:</label>
					<label for="single_template" class="global_checkbox rcpig_checkbox">
					<input type="checkbox" name="single_template" class="tickbox" id="single_template" value="yes" <?php if($single_template == "yes") echo('checked="checked"'); ?>/>
					</label>
				</div>
				<div id="single_options">
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Enter Images Description Length', 'rcpig'); ?></label>
						<input type="number" name="single_desc_limit" class="medium" id="single_desc_limit" value="<?php echo $single_desc_limit; ?>" min="10" max="500" />
					</div>
				</div>
				<div class="rcpig_options">
					<label class="input-check"><?php _e('Display Custom Template for Archive Portfolio Page', 'rcpig'); ?>:</label>
					<label for="archive_template" class="global_checkbox rcpig_checkbox">
					<input type="checkbox" name="archive_template" class="tickbox" id="archive_template" value="yes" <?php if($archive_template == "yes") echo('checked="checked"'); ?>/>
					</label>
				</div>
				<div id="archive_options">
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Enter Portfolio Description Length', 'rcpig'); ?></label>
						<input type="number" name="archive_desc_limit" class="medium" id="archive_desc_limit" value="<?php echo $archive_desc_limit; ?>" min="10" max="500" />
					</div>
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Select Pagination Alignment', 'rcpig'); ?>:</label>
						<select name="temp_pagination_align" id="temp_pagination_align" class="port-dir">
							<?php if($temp_pagination_align == 'left') { ?>
							<option value="left" selected="selected"><?php _e('Left', 'rcpig'); ?></option>
							<option value="right"><?php _e('Right', 'rcpig'); ?></option>
							<option value="center"><?php _e('Center', 'rcpig'); ?></option>
							<?php } elseif($temp_pagination_align == 'right') { ?>
							<option value="left"><?php _e('Left', 'rcpig'); ?></option>
							<option value="right" selected="selected"><?php _e('Right', 'rcpig'); ?></option>
							<option value="center"><?php _e('Center', 'rcpig'); ?></option>
							<?php } else { ?>
							<option value="left"><?php _e('Left', 'rcpig'); ?></option>
							<option value="right"><?php _e('Right', 'rcpig'); ?></option>
							<option value="center" selected="selected"><?php _e('Center', 'rcpig'); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div id="template-all">
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Enter Portfolio Image Width', 'rcpig'); ?></label>
						<input type="number" name="archive_img_width" class="medium" id="archive_img_width" value="<?php echo $archive_img_width; ?>" min="200" max="1000" /> px
					</div>
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Enter Portfolio Image Height', 'rcpig'); ?></label>
						<input type="number" name="archive_img_height" class="medium" id="archive_img_height" value="<?php echo $archive_img_height; ?>" min="200" max="1000" /> px
					</div>
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Number of Portfolio Columns to Show', 'rcpig'); ?>:</label>
						<select name="portfolio_columns" id="portfolio_columns" class="port-dir">
							<?php if($portfolio_columns == '1') { ?>
							<option value="1" selected="selected"><?php _e('1 Column', 'rcpig'); ?></option>
							<option value="2"><?php _e('2 Columns', 'rcpig'); ?></option>
							<option value="3"><?php _e('3 Columns', 'rcpig'); ?></option>
							<option value="4"><?php _e('4 Columns', 'rcpig'); ?></option>
							<?php } elseif($portfolio_columns == '3') { ?>
							<option value="1"><?php _e('1 Column', 'rcpig'); ?></option>
							<option value="2"><?php _e('2 Columns', 'rcpig'); ?></option>
							<option value="3" selected="selected"><?php _e('3 Columns', 'rcpig'); ?></option>
							<option value="4"><?php _e('4 Columns', 'rcpig'); ?></option>
							<?php } elseif($portfolio_columns == '4') { ?>
							<option value="1"><?php _e('1 Column', 'rcpig'); ?></option>
							<option value="2"><?php _e('2 Columns', 'rcpig'); ?></option>
							<option value="3"><?php _e('3 Columns', 'rcpig'); ?></option>
							<option value="4" selected="selected"><?php _e('4 Columns', 'rcpig'); ?></option>
							<?php } else { ?>
							<option value="1"><?php _e('1 Column', 'rcpig'); ?></option>
							<option value="2" selected="selected"><?php _e('2 Columns', 'rcpig'); ?></option>
							<option value="3"><?php _e('3 Columns', 'rcpig'); ?></option>
							<option value="4"><?php _e('4 Columns', 'rcpig'); ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Enter Space Between Portfolios', 'rcpig'); ?></label>
						<input type="number" name="portfolio_space" class="medium" id="portfolio_space" value="<?php echo $portfolio_space; ?>" min="0" max="20" /> %
					</div>
					<div class="rcpig_options">
						<label class="input-title"><?php _e('Select Content Alignment', 'rcpig'); ?>:</label>
						<select name="temp_page_align" id="temp_page_align" class="port-dir">
							<?php if($temp_page_align == 'left') { ?>
							<option value="left" selected="selected"><?php _e('Left', 'rcpig'); ?></option>
							<option value="right"><?php _e('Right', 'rcpig'); ?></option>
							<option value="center"><?php _e('Center', 'rcpig'); ?></option>
							<?php } elseif($temp_page_align == 'right') { ?>
							<option value="left"><?php _e('Left', 'rcpig'); ?></option>
							<option value="right" selected="selected"><?php _e('Right', 'rcpig'); ?></option>
							<option value="center"><?php _e('Center', 'rcpig'); ?></option>
							<?php } else { ?>
							<option value="left"><?php _e('Left', 'rcpig'); ?></option>
							<option value="right"><?php _e('Right', 'rcpig'); ?></option>
							<option value="center" selected="selected"><?php _e('Center', 'rcpig'); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="rcpig_options">
					<label class="input-check"><?php _e('Display Sidebar on Template Page', 'rcpig'); ?>:</label>
					<label for="template_sidebar" class="global_checkbox rcpig_checkbox">
					<input type="checkbox" name="template_sidebar" class="tickbox" id="template_sidebar" value="yes" <?php if($template_sidebar == "yes") echo('checked="checked"'); ?>/>
					</label>
				</div>
				<div class="rcpig_options">
					<label class="input-title"><?php _e('Enter Template Page Containae Width', 'rcpig'); ?></label>
					<input type="number" name="temp_page_width" class="medium" id="temp_page_width" value="<?php echo $temp_page_width; ?>" min="25" max="100" /> %
				</div>
				<p><input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ) ?>" /></p>
			</form>
		</div>

		<div id="cstyle"><?php
			$custom_css_default = __( '/* 
Welcome to the Custom CSS editor!

Please add all your custom CSS here and avoid modifying the core plugin files, since that\'ll make upgrading the plugin problematic. Your custom CSS will be loaded after the plugin\'s stylesheets, which means that your rules will take precedence. Just add your CSS here for what you want to change.
*/', 'rcpig' );
			$custom_css = get_option( 'custom_css', $custom_css_default ); ?>
			<div id="icon-themes" class="icon32"></div>
			<h2><?php _e( 'Custom CSS' ); ?></h2>
			<form id="custom_css_form" method="post" action="options.php">
				<?php settings_fields( 'rcpig-settings-css' ); ?>
				<div id="custom_css_container">
					<div name="custom_css" id="custom_css" style="border: 1px solid #DFDFDF; -moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px; width: 100%; height: 400px; position: relative;"></div>
				</div>
				<textarea id="custom_css_textarea" name="custom_css" style="display: none;"><?php echo $custom_css; ?></textarea>
				<p><input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ) ?>" /></p>
			</form>
		</div>

		<div id="export"><?php
			$package_gallery = get_option('portfolioTables');
			$gallery_lists = explode(', ', $package_gallery);
			$temp_option = 'portfolioTables, ' . $package_gallery;
			foreach($gallery_lists as $list) {
				$merge_option  = $temp_option . ', ' . $list . '_category' . ', ' . $list . '_options';
				$temp_option = $merge_option;
			}
			if (!isset($_POST['rcpig_export'])) { ?>
				<div id="icon-tools" class="icon32"><br /></div>
				<h2><?php _e( 'Export', 'rcpig'); ?></h2>
				<p><?php echo __('When you click <strong>Download Export File</strong> button, WordPress will generate a JSON file for you to save to your computer.', 'rcpig') ?></p>
				<p><?php echo __('This backup file contains all the configuration and setting options of your portfolio galleries. Note that it do <b>NOT</b> contain posts, pages, or any relevant data, just your all portfolio gallery options.', 'rcpig') ?></p>
				<p><?php echo __('To create a backup for all of your portfolios, just go to <strong>Tools >> Export</strong> and click on <strong>Download Export File</strong> there. When you click the button WordPress will create an XML file for you to save to your computer.', 'rcpig') ?></p>
				<p><?php echo __('After exporting, you can either use the backup file to restore your settings on this site again or another WordPress site.', 'rcpig') ?></p>
				<form method="post" id="export-rcpig">
					<p class="submit">
						<?php wp_nonce_field('rcpig-export'); ?>
						<input type="submit" name="rcpig_export" id="rcpig_export" class="button button-primary" value="<?php echo __('Download Export File', 'rcpig') ?>" />
					</p>
				</form><?php
			} elseif (check_admin_referer('rcpig-export')) {
				$date = date("m-d-Y");
				$json_name = "responsive-portfolio-image-gallery-".$date; // Namming the filename will be generated.
			
				$option_lists = explode(', ', $merge_option);
				foreach($option_lists as $oplist) {
					$options[] = array($oplist => get_option($oplist));
				}
				$json_file = json_encode($options);
			
				ob_clean();
				echo $json_file;
				header("Content-Type: text/json; charset=" . get_option( 'blog_charset'));
				header("Content-Disposition: attachment; filename=$json_name.json");
				exit();
			} ?>
		</div>

		<div id="import">
			<div id="icon-tools" class="icon32"><br /></div>
			<h2><?php _e( 'Import', 'rcpig'); ?></h2><?php
			if (isset($_FILES['import']) && check_admin_referer('rcpig-import')) {
				if ($_FILES['import']['error'] > 0) { wp_die("Error happens"); }
				else {
					$file_name = $_FILES['import']['name'];
					$file_ext = strtolower(end(explode(".", $file_name)));
					$file_size = $_FILES['import']['size'];
					if (($file_ext == "json") && ($file_size < 500000)) {
						$encode_options = file_get_contents($_FILES['import']['tmp_name']);
						$option_lists = json_decode($encode_options, true);
						foreach ($option_lists as $key => $options) {
							foreach ($options as $key => $value) { update_option($key, $value); }
						}
						echo "<div class='updated'><p>".__('All options are imported successfully.', 'rcpig')."</p></div>";
					} else { echo "<div class='error'><p>".__('Invalid file or file size too big.', 'rcpig')."</p></div>"; }
				}
			} ?>
			<p><?php echo __('Choose a json file to upload that you backup before, then click <strong>Upload file and import</strong>. Wordpress do the rest for you.', 'rcpig') ?></p>
			<form method="post" enctype="multipart/form-data" id="import-rcpig">
				<?php wp_nonce_field('rcpig-import'); ?>
				<p><label for="rcpig_upload"><?php echo __('Choose a file from your computer:', 'rcpig') ?></label> <?php echo __('(Maximum size: 450 MB)', 'rcpig') ?><input type="file" id="rcpig_upload" name="import" size="25" /></p>
				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php echo __('Upload file and import', 'rcpig') ?>" /></p>
			</form>
		</div>
	</div>
</div><?php
rcpig_sidebar();
?>