<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 *
 * The template for displaying archive portfolio items
 */
get_header();
require_once plugin_dir_path( __FILE__ ) . 'template-options.php'; ?>
<div id="primary" class="rcpig_single_items">
	<div id="content" class="rcpig-content" role="main"><?php
	if ( have_posts() ) : ?>
		<div class="rcpig-gallery-container gallery-archive"<?php echo $text_align; ?>><?php
			while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="rcpig-portfolio-gallery"><?php
					if ( has_post_thumbnail() && ! post_password_required() ) :
						$thumb_id = get_post_thumbnail_id();
						$img_url = wp_get_attachment_url( $thumb_id,'full' );
						$image_thumb = aq_resize( $img_url, $image_width, $image_height, true, true, true );
					endif; ?>
					<figure class="rcpig_overlay">
						<div class="rcmig-media">
							<img src="<?php echo $image_thumb; ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>" />
						</div>
						<figcaption>
							<h5 class="rcpig-title"><?php the_title(); ?></h5>
							<span class="rcpig-caption"><?php echo wp_trim_words( get_the_content(), $desc_limit, ' ...' ); ?></span>
							<a href="<?php the_permalink(); ?>" class="rcpig-more" target="_blank">Read More</a>
						</figcaption>
					</figure>
				</div>
			</article><?php
			endwhile;
			wp_reset_query(); ?>
		</div><?php
	endif; ?>
	</div><?php
	$template_sidebar = get_option( 'template_sidebar' );
	if ( isset($template_sidebar) && $template_sidebar == 'yes' ) {
		get_sidebar();
	} ?>
</div><?php
rcpig_template_paging_link();
get_footer();
?>