<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 *
 * The template for displaying single portfolio items
 */
get_header();
require_once plugin_dir_path( __FILE__ ) . 'template-options.php'; ?>
<div id="primary" class="rcpig_single_items">
	<div id="content" class="rcpig-content" role="main"><?php
		while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="rcpig_media"><?php
					if ( has_post_thumbnail() && ! post_password_required() ) : ?>
					<div class="entry-thumbnail"><?php
						the_post_thumbnail(); ?>
					</div><?php
					else :
					endif; ?>
				</div>
				<h1 class="rcpig-single-portfolio-title">
					<span><?php the_title(); ?></span>
				</h1>
				<header class="rcpig-header">
					<ul class="rcpig-meta">
						<li><?php the_time('M j, Y'); ?></li>
						<li><strong>Genre: </strong><?php the_terms( $post->ID, 'rcpig-category' ,  ' ' ); ?></li>
						<li><?php comments_popup_link('0 comment', '1 comment', '% comments'); ?></li>
					</ul>
				</header>
				<div class="rcpig-entry-content"><?php
					the_content(); ?>
				</div><?php
				$rcpig_portfolio = get_post_meta( $post->ID, '_multi_img_array', true );
				$portfolio_images = explode(",", $rcpig_portfolio); ?>

				<div class="rcpig-gallery-container"<?php echo $text_align; ?>><?php
					foreach( $portfolio_images as $gallery ) {
						$gallery_vdo_url = get_post_meta($gallery, '_rcpig_video_url', true);
						$gallery_url = wp_get_attachment_url( $gallery, 'full' );
						$gallery_thumb = aq_resize( $gallery_url, $image_width, $image_height, true, true, true );
						$attachment_meta = wp_get_attachment($gallery); ?>

						<div class="rcpig-portfolio-gallery gallery-single">
							<a href="<?php if($gallery_vdo_url != '') { echo $gallery_vdo_url; } else { echo $gallery_url; } ?>" target="_blank">
								<figure class="rcpig_overlay">
									<div class="rcmig-media">
										<img src="<?php echo $gallery_thumb; ?>" title="<?php echo $attachment_meta['title']; ?>" alt="<?php echo $attachment_meta['title']; ?>" />
									</div>
									<figcaption>
										<h5 class="rcpig-title"><?php echo $attachment_meta['title']; ?></h5>
										<span class="rcpig-caption"><?php echo wp_trim_words( $attachment_meta['description'], $imgdesc_limit, ' ...' ); ?></span>
									</figcaption>
								</figure>
							</a>
						</div><?php
					} ?>
				</div><?php
				if ( get_the_author_meta( 'description' ) ) : ?>
				<footer class="rcpig-author-info">

					<div class="rcpig-author-avatar"><?php
						$author_bio_avatar_size = apply_filters( 'rcpig_author_bio_avatar_size', 74 );
						echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size ); ?>
					</div><!-- .author-avatar -->
					<div class="rcpig-author-description">
						<h2 class="rcpig-author-title"><?php printf( __( '%s', 'rcpig' ), get_the_author() ); ?></h2>
						<p class="rcpig-author-bio">
							<?php the_author_meta( 'description' ); ?>
							<a class="rcpig-author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
								<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'rcpig' ), get_the_author() ); ?>
							</a>
						</p>
					</div><!-- .author-description -->
				</footer><?php
				endif; ?>
			</article><?php
			rcpig_portfolio_nav();
			comments_template();
		endwhile;
		wp_reset_query(); ?>
	</div><?php
	$template_sidebar = get_option( 'template_sidebar' );
	if ( isset($template_sidebar) && $template_sidebar == 'yes' ) {
		get_sidebar();
	} ?>
</div><?php
get_footer();
?>