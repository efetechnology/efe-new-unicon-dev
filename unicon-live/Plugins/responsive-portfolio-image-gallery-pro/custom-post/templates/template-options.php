<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
$temp_page_width = get_option( 'temp_page_width' );
$temp_page_align = get_option( 'temp_page_align' );
$archive_img_width = get_option( 'archive_img_width' );
$archive_img_height = get_option( 'archive_img_height' );
$portfolio_space = get_option('portfolio_space') != '' ? get_option('portfolio_space') : 0;
$portfolio_columns = get_option('portfolio_columns') != '' ? get_option('portfolio_columns') : 2;
$single_desc_limit = get_option( 'single_desc_limit' );
$archive_desc_limit = get_option( 'archive_desc_limit' );
if ( isset( $temp_page_width ) && !empty( $temp_page_width ) ) { $container_width = $temp_page_width . '%'; }
else { $container_width = ''; }
if ( isset( $temp_page_align ) && !empty( $temp_page_align ) ) {
	$text_align = ' style="text-align:'.$temp_page_align.'"';
	if($temp_page_align == 'left' || $temp_page_align == 'center') { $align = 'right'; }
	else { $align = 'left'; }
} else { $text_align = ''; }
if ( isset( $archive_img_width ) && !empty( $archive_img_width ) ) { $image_width = $archive_img_width; }
else { $image_width = 640; }
if ( isset( $archive_img_height ) && !empty( $archive_img_height ) ) { $image_height = $archive_img_height; }
else { $image_height = 426; }
if ( isset( $single_desc_limit ) && !empty( $single_desc_limit ) ) { $imgdesc_limit = $single_desc_limit; }
else { $imgdesc_limit = 30; }
if ( isset( $archive_desc_limit ) && !empty( $archive_desc_limit ) ) { $desc_limit = $archive_desc_limit; }
else { $desc_limit = 10; }
if(isset($portfolio_space) && !empty($portfolio_space)) {
	$single_col_width = (100 - ($portfolio_space * (12-1)))/12;
	$width = ($single_col_width * (12 / $portfolio_columns)) + ($portfolio_space * ((12 / $portfolio_columns) - 1));
} else { $width = ($total_width)/$portfolio_columns; }
$tab_width = (((100 - ($portfolio_columns * (12-1)))/12) * (12 / 2)) + ($portfolio_columns * ((12 / 2) - 1));
$mob_width = ((100/12 * (12 / 1)));
?>
<style type="text/css">
.rcpig_single_items .rcpig-content{width:<?php echo $container_width; ?>}.rcpig-gallery-container .rcpig,.rcpig-gallery-container .gallery-single{width:<?php echo $width; ?>%;margin-<?php echo $align; ?>:<?php echo $portfolio_space/2; ?>%}.rcpig-gallery-container .rcpig:nth-of-type(<?php echo $portfolio_columns; ?>n+<?php echo $portfolio_columns; ?>),.rcpig-gallery-container .gallery-single:nth-of-type(<?php echo $portfolio_columns; ?>n+<?php echo $portfolio_columns; ?>){margin-right:0}<?php if($temp_page_align == 'center') { ?>.rcpig-gallery-container .rcpig:nth-of-type(<?php echo $portfolio_columns + 1; ?>n+<?php echo $portfolio_columns + 1; ?>),.rcpig-gallery-container .gallery-single:nth-of-type(<?php echo $portfolio_columns + 1; ?>n+<?php echo $portfolio_columns + 1; ?>){margin-left:<?php echo $portfolio_space/2; ?>%}<?php } ?>.rcpig-portfolio-gallery .rcmig-media img{width:100%}@media(min-width:481px) and (max-width:979px){.rcpig_single_items .rcpig-content{width:100%}.rcpig-gallery-container .rcpig,.rcpig-gallery-container .gallery-single{width:<?php echo $tab_width; ?>%}}@media(max-width : 480px){.rcpig_single_items .rcpig-content{width:100%}.rcpig-gallery-container .rcpig,.rcpig-gallery-container .gallery-single{width:<?php echo $mob_width; ?>%}}
</style>