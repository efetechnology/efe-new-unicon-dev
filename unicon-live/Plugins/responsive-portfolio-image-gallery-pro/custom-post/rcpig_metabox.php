<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
add_action( 'add_meta_boxes', 'rcpig_add_meta_button' );
add_action( 'save_post', 'rcpig_save_meta_boxes_button' );
add_action( 'add_meta_boxes', 'rcpig_add_meta_box' );
add_action( 'wp_insert_post', 'rcpig_insert_postdata' );
add_action( 'save_post', 'rcpig_insert_postdata' );
function rcpig_add_meta_button(){
	$rcpig_post_type = 'rcpig';
	add_meta_box(
		'rcpig_portfolio_button',
		esc_html__( 'Buttons For this Portfolio', 'rcpig' ),
		'rcpig_render_buttons_meta_box',
		$rcpig_post_type,
		'normal',
		'default'
	);
}
function rcpig_render_buttons_meta_box() {
	global $post;
	wp_nonce_field( plugin_basename( __FILE__ ), 'rcpig_meta_box_nonce' );
	$read_more = get_post_meta( $post->ID, '_read_more', true ) != '' ? get_post_meta( $post->ID, '_read_more', true ) : '';
	$button_text = get_post_meta( $post->ID, '_button_text', true ) != '' ? get_post_meta( $post->ID, '_button_text', true ) : '';
	$button_link = get_post_meta( $post->ID, '_button_link', true ) != '' ? get_post_meta( $post->ID, '_button_link', true ) : '';
	$button_ntab = get_post_meta( $post->ID, '_button_ntab', true ) != '' ? get_post_meta( $post->ID, '_button_ntab', true ) : '';
	$button_color = get_post_meta( $post->ID, '_button_color', true ) != '' ? get_post_meta( $post->ID, '_button_color', true ) : '';
	$button_hover = get_post_meta( $post->ID, '_button_hover', true ) != '' ? get_post_meta( $post->ID, '_button_hover', true ) : '';
	$font_color = get_post_meta( $post->ID, '_button_font_color', true ) != '' ? get_post_meta( $post->ID, '_button_font_color', true ) : '';
	$font_hover = get_post_meta( $post->ID, '_button_font_hover', true ) != '' ? get_post_meta( $post->ID, '_button_font_hover', true ) : '';
	$text_temp = explode(",", $button_text);
	$link_temp = explode(",", $button_link);
	$ntab_temp = explode(",", $button_ntab);
	$bcolor_temp = explode(",", $button_color);
	$bhover_temp = explode(",", $button_hover);
	$fcolor_temp = explode(",", $font_color);
	$fhover_temp = explode(",", $font_hover);
	$button_count = count($text_temp); ?>
	<div id="portbuttondiv">
		<label for="read_more" class="show_more">
			<span>Read More</span>
			<input type="checkbox" name="read_more" class="tickbox" id="read_more" value="true" <?php if($read_more == 'true') { ?> checked="checked" <?php } ?> />
		</label>
		<table id="rcpig_addbutton" cellspacing="0">
			<tr class="button_header">
				<th width="35%"><?php _e('Button Text', 'rcpig'); ?></th>
				<th width="45%"><?php _e('Button URL', 'rcpig'); ?></th>
				<th><?php _e('New Tab', 'rcpig'); ?></th>
				<th><?php _e('Actions', 'rcpig'); ?></th>
			</tr><?php
			if($button_count > 0 && $text_temp[0] != '') {
				for( $i = 0; $i < $button_count; $i++ ) { ?>
				<tr class="button_body">
					<td>
						<input type="text" name="button_text[]" class="medium" value="<?php echo $text_temp[$i]; ?>" placeholder="<?php _e('e.g. Demo', 'rcpig'); ?>" />
						<label class="input-title"><?php _e('Button Color', 'rcpig'); ?></label>
						<input type="text" name="button_color[]" class="button_color" id="button_color" value="<?php echo $bcolor_temp[$i]; ?>" />
						<label class="input-title"><?php _e('Button Hover', 'rcpig'); ?></label>
						<input type="text" name="button_hover[]" class="button_hover" id="button_hover" value="<?php echo $bhover_temp[$i]; ?>" />
						<label class="input-title"><?php _e('Font Color', 'rcpig'); ?></label>
						<input type="text" name="button_font_color[]" class="button_font_color" id="button_font_color" value="<?php echo $fcolor_temp[$i]; ?>" />
						<label class="input-title"><?php _e('Font Hover', 'rcpig'); ?></label>
						<input type="text" name="button_font_hover[]" class="button_font_hover" id="button_font_hover" value="<?php echo $fhover_temp[$i]; ?>" />
					</td>
					<td><input type="text" name="button_link[]" class="medium" value="<?php echo $link_temp[$i]; ?>" placeholder="<?php _e('e.g. Demo', 'rcpig'); ?>" /></td>
					<td>
						<select name="button_ntab[]" id="button_ntab"><?php
						if($ntab_temp[$i] == 'true') { ?>
							<option value="true" selected="selected"><?php _e('Yes', 'rcpig'); ?></option>
							<option value="false"><?php _e('No', 'rcpig'); ?></option><?php
						} else { ?>
							<option value="true"><?php _e('Yes', 'rcpig'); ?></option>
							<option value="false" selected="selected"><?php _e('No', 'rcpig'); ?></option><?php
						} ?>
						</select>
					</td>
					<td><span id="remButton"></span></td>
				</tr><?php
				}
			} else { ?>
				<tr class="button_body">
					<td>
						<input type="text" name="button_text[]" class="medium" value="" placeholder="<?php _e('e.g. Demo', 'rcpig'); ?>" />
						<label class="input-title"><?php _e('Button Color', 'rcpig'); ?></label>
						<input type="text" name="button_color[]" class="button_color" id="button_color" value="#262626" />
						<label class="input-title"><?php _e('Button Hover', 'rcpig'); ?></label>
						<input type="text" name="button_hover[]" class="button_hover" id="button_hover" value="#262626" />
						<label class="input-title"><?php _e('Font Color', 'rcpig'); ?></label>
						<input type="text" name="button_font_color[]" class="button_font_color" id="button_font_color" value="#ffffff" />
						<label class="input-title"><?php _e('Font Hover', 'rcpig'); ?></label>
						<input type="text" name="button_font_hover[]" class="button_font_hover" id="button_font_hover" value="#ffffff" />
					</td>
					<td><input type="text" name="button_link[]" class="medium" value="" placeholder="<?php _e('e.g. Demo', 'rcpig'); ?>" /></td>
					<td>
						<select name="button_ntab[]" id="button_ntab">
							<option value="true"><?php _e('Yes', 'rcpig'); ?></option>
							<option value="false" selected="selected"><?php _e('No', 'rcpig'); ?></option>
						</select>
					</td>
					<td><span id="remDisable"></span></td>
				</tr><?php
			} ?>
		</table>
		<input type="button" id="addbutton" class="button-primary" value="<?php _e('Add New', 'rcpig'); ?>" />
	</div><?php
}
function rcpig_save_meta_boxes_button( $post_id ){
	// verify meta box nonce
	if ( !isset( $_POST['rcpig_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['rcpig_meta_box_nonce'], plugin_basename( __FILE__ ) ) ) { return; }
	// return if autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }
	// Check the user's permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
	// store custom fields values
	if ( isset( $_POST['read_more'] ) && $_POST['read_more'] == 'true' ) {
		update_post_meta( $post_id, '_read_more', $_POST['read_more'] );
	} else {
		update_post_meta( $post_id, '_read_more', 'false' );
	}
	if ( isset( $_REQUEST['button_text'] ) ) {
		$cur_text = implode(',', $_POST['button_text']);
		if(!(empty($cur_text))) {
			update_post_meta( $post_id, '_button_text', sanitize_text_field( $cur_text ) );
		} else {
			add_post_meta( $post_id, '_button_text', sanitize_text_field( $cur_text ), true );
		}
	}
	if ( isset( $_REQUEST['button_link'] ) ) {
		$cur_link = implode(',', $_POST['button_link']);
		if(!(empty($cur_link))) {
			update_post_meta( $post_id, '_button_link', sanitize_text_field( $cur_link ) );
		} else {
			add_post_meta( $post_id, '_button_link', sanitize_text_field( $cur_link ), true );
		}
	}
	if ( isset( $_REQUEST['button_ntab'] ) ) {
		$cur_ntab = implode(',', $_POST['button_ntab']);
		if(!(empty($cur_ntab))) {
			update_post_meta( $post_id, '_button_ntab', sanitize_text_field( $cur_ntab ) );
		} else {
			add_post_meta( $post_id, '_button_ntab', sanitize_text_field( $cur_ntab ), true );
		}
	}
	if ( isset( $_REQUEST['button_color'] ) ) {
		$cur_bcolor = implode(',', $_POST['button_color']);
		if(!(empty($cur_bcolor))) {
			update_post_meta( $post_id, '_button_color', sanitize_text_field( $cur_bcolor ) );
		} else {
			add_post_meta( $post_id, '_button_color', sanitize_text_field( $cur_bcolor ), true );
		}
	}
	if ( isset( $_REQUEST['button_hover'] ) ) {
		$cur_bhover = implode(',', $_POST['button_hover']);
		if(!(empty($cur_bhover))) {
			update_post_meta( $post_id, '_button_hover', sanitize_text_field( $cur_bhover ) );
		} else {
			add_post_meta( $post_id, '_button_hover', sanitize_text_field( $cur_bhover ), true );
		}
	}
	if ( isset( $_REQUEST['button_font_color'] ) ) {
		$cur_fcolor = implode(',', $_POST['button_font_color']);
		if(!(empty($cur_fcolor))) {
			update_post_meta( $post_id, '_button_font_color', sanitize_text_field( $cur_fcolor ) );
		} else {
			add_post_meta( $post_id, '_button_font_color', sanitize_text_field( $cur_fcolor ), true );
		}
	}
	if ( isset( $_REQUEST['button_font_hover'] ) ) {
		$cur_fhover = implode(',', $_POST['button_font_hover']);
		if(!(empty($cur_fhover))) {
			update_post_meta( $post_id, '_button_font_hover', sanitize_text_field( $cur_fhover ) );
		} else {
			add_post_meta( $post_id, '_button_font_hover', sanitize_text_field( $cur_fhover ), true );
		}
	}
}
/* Adds a box to the main column on the Post and Page edit screens */
function rcpig_add_meta_box() {
	$rcpig_post_type_select = 'rcpig';
	add_meta_box(
		'rcpig_portfolio_meta',      // Unique ID
		esc_html__( 'Images For this Portfolio', 'rcpig' ),    // Title
		'rcpig_render_images_meta_box',   // Callback function
		$rcpig_post_type_select, // Admin page (or post type)
		'side',          // Context
		'default'         // Priority
	);
}
/* Prints the box content */
function rcpig_render_images_meta_box() {
	global $wpdb;
	global $post;
	// Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'rcpig_noncename' );
	// The actual fields for data entry
	// Use get_post_meta to retrieve an existing value from the database and use the value for the form
	$value = get_post_meta($post->ID, '_multi_img_array', true);
	$temp = explode(",", $value);
	if ($temp) {
		foreach ( $temp as $t_val ) {
			$image_attributes = wp_get_attachment_image_src( $t_val , array(63,63) );
			echo '<img src="'.$image_attributes[0].'" width="'.$image_attributes[1].'" height="'.$image_attributes[2].'" data-id="'.$t_val.'">';
		}
	} else {	// this else is interupting to delete all images. Need to work with this letter.
		$image_attributes = wp_get_attachment_image_src( $value , array(63,63) );
		echo '<img src="'.$image_attributes[0].'" width="'.$image_attributes[1].'" height="'.$image_attributes[2].'" data-id="'.$value.'">';
	}
	echo "<input type='hidden' name='image_upload_val' id='image_upload_val' value='".$value."' />";
	echo "<div class='rcpig_upload_media' id='rcpig_image_upload'>" . __('Upload Images', 'rcpig') . "</div>";
	echo "<span class='remove_image'>" . __('Click on image to remove it', 'rcpig') . "</span>";
}
/* When the post is saved, saves our custom data */
function rcpig_insert_postdata( $post_id ) {
	global $wpdb;
	// First we need to check if the current user is authorised to do this action. 
	if ( ! current_user_can( 'edit_page', $post_id ) )
		return;

	if ( ! current_user_can( 'edit_post', $post_id ) )
		return;

	// Secondly we need to check if the user intended to change this value.
	if ( ! isset( $_POST['rcpig_noncename'] ) || ! wp_verify_nonce( $_POST['rcpig_noncename'], plugin_basename( __FILE__ ) ) )
		return;

	// Thirdly we can save the value to the database
	//if saving in a custom table, get post_ID
	$post_ID = $_POST['post_ID'];
	//sanitize user input
	// $mydata = sanitize_text_field( $_POST['myplugin_priceCode'] );
	$mydata = $_POST['image_upload_val'];
	// Do something with $mydata 
	// either using 
	if($mydata) {
		$cur_data = get_post_meta($post_ID, '_multi_img_array', true);
		if(!(empty($cur_data))) {
			// $cur_data .=",".$mydata;
			update_post_meta($post_ID, '_multi_img_array', $mydata);
		} else {
			add_post_meta($post_id, '_multi_img_array', $mydata, true);
		}
	}
}
/* Add Video URL fields to media uploader */
function rcpig_attachment_field_credit( $form_fields, $post ) {
	$form_fields['rcpig-video-url'] = array(
		'label' => 'Video URL',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, '_rcpig_video_url', true ),
		'helps' => 'Add Youtube or Vimeo URL',
	);
	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'rcpig_attachment_field_credit', 10, 2 );
/* Save values of Video URL in media uploader */
function rcpig_attachment_field_credit_save( $post, $attachment ) {
	if( isset( $attachment['rcpig-video-url'] ) )
		update_post_meta( $post['ID'], '_rcpig_video_url', esc_url( $attachment['rcpig-video-url'] ) );
	return $post;
}
add_filter( 'attachment_fields_to_save', 'rcpig_attachment_field_credit_save', 10, 2 );
?>