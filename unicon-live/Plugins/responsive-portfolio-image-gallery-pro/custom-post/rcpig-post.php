<?php
/*
 * Responsive Portfolio Image Gallery Pro 1.0.1
 * By @realwebcare - http://www.realwebcare.com
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
function register_rcp_image_gallery() {
	$rcpig_labels =  apply_filters( 'rcpig_labels', array(
		'name'                => _x( 'Portfolios', 'Post Type General Name', 'rcpig' ),
		'singular_name'       => _x( 'Portfolio', 'Post Type Singular Name', 'rcpig' ),
		'menu_name'           => __( 'Portfolio', 'rcpig' ),
		'parent_item_colon'   => __( 'Parent Portfolio:', 'rcpig' ),
		'all_items'           => __( 'All Portfolios', 'rcpig' ),
		'view_item'           => __( 'View Portfolio', 'rcpig' ),
		'add_new_item'        => __( 'Add New Portfolio', 'rcpig' ),
		'add_new'             => __( 'New Portfolio', 'rcpig' ),
		'edit_item'           => __( 'Edit Portfolio', 'rcpig' ),
		'update_item'         => __( 'Update Portfolio', 'rcpig' ),
		'search_items'        => __( 'Search Portfolio', 'rcpig' ),
		'not_found'           => __( 'No Portfolio Found', 'rcpig' ),
		'not_found_in_trash'  => __( 'No Portfolio in Trash', 'rcpig' ),
	) );
	$rewrite = array(
		'slug'                => 'portfolio',
		'with_front'          => false,
		'pages'               => true,
		'feeds'               => true,
	);
	$rcpig_args = array(
		'label'					=> __( 'Portfolio', 'rcpig' ),
		'description'			=> __( 'Lightweight portfolio gallery grid plugin', 'rcpig' ),
		'labels' 				=> $rcpig_labels,
		'supports' 				=> apply_filters('rcpig_supports', array( 'title', 'editor', 'thumbnail', 'author', 'comments' ) ),
		'taxonomies'			=> array( 'post_tag', 'rcpig-category' ),	
		'hierarchical' 			=> false,
		'public' 				=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> 'dashicons-format-image',
		'can_export'			=> true,
		'has_archive' 			=> true,
		'exclude_from_search'	=> false,
		'publicly_queryable'	=> true,
		'rewrite'				=> $rewrite,
		'query_var'		 		=> true,
		'capability_type'		=> 'page',
	);
	register_post_type( 'rcpig', apply_filters( 'rcpig_post_type_args', $rcpig_args ) );
}
add_action('init', 'register_rcp_image_gallery', 0);

// Register Theme Features (feature image for portfolio)
if ( ! function_exists('rcpig_theme_support') ) {
	function rcpig_theme_support()  {
		// Add theme support for Featured Images
		add_theme_support( 'post-thumbnails', array( 'rcpig' ) );
	}
	// Hook into the 'after_setup_theme' action
	add_action( 'after_setup_theme', 'rcpig_theme_support' );
}

function rcp_image_gallery_taxonomies() {
	$labels = array(
		'name'                       => _x( 'Portfolio Categories', 'Taxonomy General Name', 'rcpig' ),
		'singular_name'              => _x( 'Portfolio Category', 'Taxonomy Singular Name', 'rcpig' ),
		'menu_name'                  => __( 'Portfolio Category', 'rcpig' ),
		'all_items'                  => __( 'All Categories', 'rcpig' ),
		'parent_item'                => __( 'Parent Category', 'rcpig' ),
		'parent_item_colon'          => __( 'Parent Category:', 'rcpig' ),
		'new_item_name'              => __( 'New Category Name', 'rcpig' ),
		'add_new_item'               => __( 'Add New Category', 'rcpig' ),
		'edit_item'                  => __( 'Edit Category', 'rcpig' ),
		'update_item'                => __( 'Update Category', 'rcpig' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'rcpig' ),
		'search_items'               => __( 'Search categories', 'rcpig' ),
		'add_or_remove_items'        => __( 'Add or remove Categories', 'rcpig' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories', 'rcpig' ),
		'not_found'                  => __( 'Not Found', 'rcpig' ),
	);
	$rewrite = array(
		'slug'                       => 'portfolio-category',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'has_archive' 				 => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'rcpig-category', array( 'rcpig' ), $args );
}
add_action( 'init', 'rcp_image_gallery_taxonomies', 0 );
/* Creating Filters With Custom Taxonomy */
add_action( 'restrict_manage_posts', 'rcpig_filter_list' );
function rcpig_filter_list() {
	global $typenow;
	$post_type = 'rcpig';
	$taxonomy  = 'rcpig-category';
	if ($typenow == $post_type) {
		$selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'hierarchical'	  => false,
			'depth'			  => 3,
			'show_count'      => false,
			'hide_empty'      => true,
		));
	};
}
/* Display Filtered Results and Implementation of the Display Function */
add_filter( 'parse_query','rcpig_perform_filtering' );
function rcpig_perform_filtering( $query ) {
	global $pagenow;
	$post_type = 'rcpig';
	$taxonomy  = 'rcpig-category';
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}
/* Register a Function to Force the Dedicated Template and Implementation of the function */
add_filter( 'template_include', 'rcpig_include_template_function', 1 );
function rcpig_include_template_function( $template_path ) {
	$single_template = get_option( 'single_template' );
	$archive_template = get_option( 'archive_template' );
	if ( get_post_type() == 'rcpig' ) {
		if ( is_single() ) {
			// checks if the file exists in the theme first,
			// otherwise serve the file from the plugin
			if ( isset($single_template) && $single_template == 'yes' ) {
				$template_path = plugin_dir_path( __FILE__ ) . 'templates/single-rcpig.php';
			} else {
				$template_path = locate_template( array ( 'single.php' ) );
			}
		} elseif ( is_archive() ) {
			if ( isset($archive_template) && $archive_template == 'yes' ) {
				$template_path = plugin_dir_path( __FILE__ ) . 'templates/archive-rcpig.php';
			} else {
				$template_path = locate_template( array ( 'archive.php' ) );
			}
		}
	}
	return $template_path;
}
/* Display navigation to next/previous post when applicable */
if ( ! function_exists( 'rcpig_portfolio_nav' ) ) :
function rcpig_portfolio_nav() {
	global $post;
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next = get_adjacent_post( false, '', false );
	if ( ! $next && ! $previous )
		return; ?>
	<nav class="navigation portfolio-navigation" role="navigation">
		<h3 class="rcpig-screen-reader"><?php _e( 'Post navigation', 'rcpig' ); ?></h3>
		<div class="rcpig-nav-links"><?php
			previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'rcpig' ) );
			next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'rcpig' ) ); ?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
<?php
}
endif;
function rcpig_template_paging_link() {
	global $wp_query;
	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;

	$temp_pagination_align = get_option( 'temp_pagination_align' );
	if ( isset( $temp_pagination_align ) && !empty( $temp_pagination_align ) ) {
		$pagination_align = ' style="text-align:'.$temp_pagination_align.'"';
	} else { $pagination_align = ''; }
	$large = 999999999; // need an unlikely integer
	$pagingLink = paginate_links( array(
		'base' => str_replace( $large, '%#%', get_pagenum_link( $large ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'type' => 'list',
		'prev_next' => true,
		'prev_text'    => '<span class="text">&laquo; ' . __( 'Previous', 'rcpig' ) . '</span>',
		'next_text'    => '<span class="text">' . __( 'Next', 'rcpig' ) . ' &raquo;</span>',
		'add_args' => false					
	));
	if ( $pagingLink ) { ?>
		<nav class="navigation rcpig-paging-navigation" role="navigation"<?php echo $pagination_align; ?>>
			<h3 class="rcpig-screen-reader"><?php _e( 'Posts navigation', 'rcpig' ); ?></h3>
			<div class="nav-links">
				<div id="posts-nav" class="navigation">
					<?php echo $pagingLink; ?>
				</div><!-- #posts-nav -->
			</div><!-- .nav-links -->
		</nav><!-- .navigation --><?php
	}
}
?>