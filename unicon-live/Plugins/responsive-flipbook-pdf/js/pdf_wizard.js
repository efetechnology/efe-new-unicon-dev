/* PDF Wizard */

jQuery( document ).ready( function( $ ) {
	function display_progress_step() {
		$step_options.addClass( 'rfbwp-disable' );
		$step_pdf.addClass( 'rfbwp-disable' );
		$step_convert.addClass( 'rfbwp-disable' );

		$step_info.addClass( 'rfbwp-active' );

		_estimation_time = Date.now();
		$progress_uploading._time.show();
	}

	function restart_form() {
		$input_name.val( '' );
		$input_pdf_overlay.text( $input_pdf_overlay.attr( 'data-text' ) )
			.parent().removeClass( 'rfbwp-active' );

		set_progress_value( $progress_uploading, '0%' );
		set_progress_value( $progress_converting, '0%' );
		set_progress_value( $progress_importing, '0%' );

		set_error_value( '' );

		$step_options.removeClass( 'rfbwp-disable' );
		$step_pdf.removeClass( 'rfbwp-disable rfbwp-active' );
		$step_convert.removeClass( 'rfbwp-disable rfbwp-active' );
		$step_info.removeClass( 'rfbwp-active' );

		$step_options.addClass( 'rfbwp-active' );
	}

	function check_server_connection() {
		$.ajax( {
			type: 'POST',
			url: 'https://wizard.mpcreation.net/api/status.json',
			dataType: 'json',
			success: function( response ) {
				_server_url = 'https://wizard.mpcreation.net/wizard/';

				$window.trigger( 'rfbwp-pdf-init' );
				$step_options.addClass( 'rfbwp-active' );
			},
			error: function() {
				set_error_value( 'server' );

				$( '.rfbwp-error-server .rfbwp-restart' ).one( 'click', function( event ) {
					event.preventDefault();

					check_server_connection();
				} );
			}
		} );
	}

	function set_error_value( type ) {
		$error_box.attr( 'data-error', type );

		if ( type != '' ) {
			$error_box.slideDown();
			clear_upload_files();
		} else {
			$error_box.slideUp();
		}
	}

	function set_progress_value( $target, value ) {
		$target._value.text( value );
		$target._line.width( value );
	}

	function get_upload_info() {
		$.ajax( {
			type: 'POST',
			url: _server_url + 'info.php',
			data: {
				pdf_path: _uploader_result.name,
				new_ID:   'true'
			},
			dataType: 'json',
			success: function( response ) {
				_pdf_pages_count = parseInt( response.pages );
				_images_path = response.dir;

				if ( _pdf_pages_count < 3 )
					set_error_value( 'pages' );
				else
					begin_convertion_process();
			},
			error: function() {
				set_error_value( 'info' );
			}
		} );
	}

	function begin_convertion_process() {
		_convertion_loop = true;
		_convertion_loop_timer = setTimeout( check_convertion_progress, 100 );
		_convertion_faux_progress = 0;
		_convertion_last_progress = 0;

		_estimation_time = Date.now();
		$progress_converting._time.show();

		$.ajax( {
			type: 'POST',
			url: _server_url + 'convert.php',
			data: {
				pdf_path: _uploader_result.name,
				images_path: _images_path,
				zoom: _enable_zoom,
				options: _options
			},
			complete: function( response ) {
				_convertion_loop = false;
				clearTimeout( _convertion_loop_timer );

				if ( response.responseText == 'done' ) {
					set_progress_value( $progress_converting, '100%' );

					$progress_converting._time.hide();
					$progress_converting._time.text( $progress_converting._time.attr( 'data-text' ) );

					get_converted_images();
				} else {
					set_error_value( 'convertion' );
				}
			}
		} );
	}

	function check_convertion_progress() {
		$.ajax( {
			type: 'POST',
			url: _server_url + 'progress.php',
			data: {
				images_path: _images_path
			},
			complete: function( response ) {
				if ( _convertion_loop ) {
					var _convertion_progress = parseInt( response.responseText );
					var _progress = parseInt( ( _convertion_progress / _pdf_pages_count * ( 100 - _convertion_faux_progress ) ) + _convertion_faux_progress, 10 );

					if ( _convertion_progress == 0 )
						_convertion_faux_progress += .25;

					if ( _progress > 100 )
						_progress = 100;

					set_progress_value( $progress_converting, _progress + '%' );

					if ( _convertion_last_progress != _progress ) {
						var _converted = _progress / 100,
							_elapsed_time = ( Date.now() - _estimation_time ) / 1000,
							_end_time = ( _elapsed_time / _converted ) * ( 1 - _converted );

						if ( _elapsed_time > 5 )
							$progress_converting._time.text( '( ' + ( _end_time >> 0 ) + 'sec )' );

						_convertion_last_progress = _progress;
					}

					_convertion_loop_timer = setTimeout( check_convertion_progress, 250 );
				}
			}
		} );
	}

	function get_converted_images() {
		$.ajax( {
			type: 'POST',
			url: _server_url + 'images.php',
			data: {
				images_path: _images_path,
				zoom: _enable_zoom
			},
			dataType: 'json',
			success: function( response ) {
				_converted_images = response;

				if ( _converted_images.length ) {
					_estimation_time = Date.now();
					$progress_importing._time.show();

					import_single_image();
				} else {
					set_error_value( 'images' );
				}
			}
		} );
	}

	function import_single_image() {
		if ( _converted_images_index < _converted_images.length ) {
			$.post( ajaxurl, {
				action: 'rfbwp_import_single_image',
				fb_name: _flipbook_name,
				image_path: _server_url + 'images/' + _images_path + '/' + _converted_images[ _converted_images_index ]
			}, function( response ) {
				_uploaded_images.push( response );

				if ( ! _enable_zoom || ( _enable_zoom && _uploaded_images.length == _uploaded_images_zoom.length ) ) {
					var _progress = parseInt( _converted_images_index / _converted_images.length * 100, 10 ) + '%';
					set_progress_value( $progress_importing, _progress );

					_converted_images_index++;
					import_single_image();
				}
			}, 'json' ).fail( function() {
				set_error_value( 'import' );
			} );

			if ( _enable_zoom ) {
				$.post( ajaxurl, {
					action: 'rfbwp_import_single_image',
					fb_name: _flipbook_name,
					zoom: 1,
					image_path: _server_url + 'images/' + _images_path + '_zoom/' + _converted_images[ _converted_images_index ]
				}, function( response ) {
					_uploaded_images_zoom.push( response );

					if ( _uploaded_images.length == _uploaded_images_zoom.length ) {
						var _progress = parseInt( _converted_images_index / _converted_images.length * 100, 10 ) + '%';
						set_progress_value( $progress_importing, _progress );

						_converted_images_index++;
						import_single_image();
					}
				}, 'json' ).fail( function() {
					set_error_value( 'import' );
				} );
			}

			var _imported = _converted_images_index / _converted_images.length,
				_elapsed_time = ( Date.now() - _estimation_time ) / 1000,
				_end_time = ( _elapsed_time / _imported ) * ( 1 - _imported );

			if ( _elapsed_time > 5 )
				$progress_importing._time.text( '( ' + ( _end_time >> 0 ) + 'sec )' );
		} else {
			$progress_importing._time.hide();
			$progress_importing._time.text( $progress_importing._time.attr( 'data-text' ) );

			if ( _uploaded_images.length >= 3 ) {
				set_progress_value( $progress_importing, '100%' );
				generate_flipbook();
			} else {
				set_error_value( 'generate' );
			}
		}
	}

	function generate_flipbook() {
		_flipbook.rfbwp_fb_name = _flipbook_name;
		_flipbook.rfbwp_fb_width = _uploaded_images[ 0 ].width;
		_flipbook.rfbwp_fb_height = _uploaded_images[ 0 ].height;

		var _single_width = _uploaded_images[ 0 ].width,
			_page_index = 1;

		_flipbook.pages.push( {
			'rfbwp_fb_page_type': 'Single Page',
			'rfbwp_fb_page_bg_image': _uploaded_images[ 0 ].url,
			'rfbwp_fb_page_bg_image_zoom': _enable_zoom ? _uploaded_images_zoom[ 0 ].url : '',
			'rfbwp_fb_page_index': 0,
			'rfbwp_fb_page_custom_class': '',
			'rfbwp_page_css': '',
			'rfbwp_page_html': '',
			'rfbwp_fb_page_url': ''
		} );

		for ( var index = 1; index < _uploaded_images.length - 1; index++ ) {
			_flipbook.pages.push( {
				'rfbwp_fb_page_type': _single_width >= _uploaded_images[ index ].width ? 'Single Page' : 'Double Page',
				'rfbwp_fb_page_bg_image': _uploaded_images[ index ].url,
				'rfbwp_fb_page_bg_image_zoom': _enable_zoom ? _uploaded_images_zoom[ index ].url : '',
				'rfbwp_fb_page_index': _page_index,
				'rfbwp_fb_page_custom_class': '',
				'rfbwp_page_css': '',
				'rfbwp_page_html': '',
				'rfbwp_fb_page_url': ''
			} );

			_page_index += _single_width >= _uploaded_images[ index ].width ? 1 : 2;
		}

		_flipbook.pages.push( {
			'rfbwp_fb_page_type': 'Single Page',
			'rfbwp_fb_page_bg_image': _uploaded_images[ _uploaded_images.length - 1 ].url,
			'rfbwp_fb_page_bg_image_zoom': _enable_zoom ? _uploaded_images_zoom[ _uploaded_images.length - 1 ].url : '',
			'rfbwp_fb_page_index': _page_index,
			'rfbwp_fb_page_custom_class': '',
			'rfbwp_page_css': '',
			'rfbwp_page_html': '',
			'rfbwp_fb_page_url': ''
		} );

		add_new_flipbook();
	}

	function add_new_flipbook() {
		$.post( ajaxurl, {
			action: 'rfbwp_create_new_flipbook',
			flipbook: _flipbook
		}, function() {
			clear_upload_files();
			
			$window.unbind( 'beforeunload' );
			
			setTimeout( function() {
				location.reload();
			}, 100 );
		} );
	}

	function clear_upload_files() {
		if ( _server_url != '' && _images_path != '' ) {
			$.ajax( {
				type: 'DELETE',
				url: _server_url + 'clear.php?images_path=' + _images_path
			} );
			if ( _enable_zoom ) {
				$.ajax( {
					type: 'DELETE',
					url: _server_url + 'clear.php?images_path=' + _images_path + '_zoom'
				} );
			}
		}

		if ( typeof _uploader_result != 'undefined' && typeof _uploader_result.deleteUrl != 'undefined' ) {
			$.ajax( {
				type: 'DELETE',
				url: _uploader_result.deleteUrl
			} );
		}
	}

	/* Form elements */
	var $window              = $( window ),
		$step_options        = $( '#rfbwp_step_options' ),
		$step_pdf            = $( '#rfbwp_step_pdf' ),
		$step_convert        = $( '#rfbwp_step_convert' ),
		$step_info           = $( '#rfbwp_step_info' ),
		$input_name          = $( '#rfbwp_name' ),
		$input_pdf           = $( '#rfbwp_pdf' ),
		$input_pdf_overlay   = $( '#rfbwp_pdf_overlay' ),
		$input_convert       = $( '#rfbwp_convert' ),
		$progress_uploading  = $( '#rfbwp_uploading' ),
		$progress_converting = $( '#rfbwp_converting' ),
		$progress_importing  = $( '#rfbwp_importing' ),
		$error_box           = $( '#rfbwp_error_box' ),
		$zoom_wrap           = $( '#rfbwp_zoom_wrap' ),
		$zoom                = $( '#rfbwp_zoom' ),
		$options             = $( '.rfbwp-validate-option' ),
		$restart_form        = $( '.rfbwp-restart' );

	/* Global variables */
	var _server_url               = '',
		_flipbook_name            = '',
		_enable_zoom              = 1,
		_options                  = {},
		_uploader_data,
		_uploader_result,
		_pdf_pages_count          = 0,
		_images_path              = '',
		_convertion_loop          = false,
		_convertion_loop_timer,
		_estimation_time,
		_convertion_faux_progress = 0,
		_convertion_last_progress = 0,
		_converted_images         = [],
		_converted_images_index   = 0,
		_uploaded_images          = [],
		_uploaded_images_zoom     = [];

	/* Flipbook base */
	var _flipbook = {
		rfbwp_fb_name:   'Flipbook',
		rfbwp_fb_width:  '400',
		rfbwp_fb_height: '500',
		pages:           []
	};

	/* Init PDF Wizard */
	$window.on( 'rfbwp-init-pdf-wizard', function() {
		$window              = $( window );
		$step_options        = $( '#rfbwp_step_options' );
		$step_pdf            = $( '#rfbwp_step_pdf' );
		$step_convert        = $( '#rfbwp_step_convert' );
		$step_info           = $( '#rfbwp_step_info' );
		$input_name          = $( '#rfbwp_name' );
		$input_pdf           = $( '#rfbwp_pdf' );
		$input_pdf_overlay   = $( '#rfbwp_pdf_overlay' );
		$input_convert       = $( '#rfbwp_convert' );
		$progress_uploading  = $( '#rfbwp_uploading' );
		$progress_converting = $( '#rfbwp_converting' );
		$progress_importing  = $( '#rfbwp_importing' );
		$error_box           = $( '#rfbwp_error_box' );
		$zoom_wrap           = $( '#rfbwp_zoom_wrap' );
		$zoom                = $( '#rfbwp_zoom' );
		$options             = $( '.rfbwp-validate-option' );
		$restart_form        = $( '.rfbwp-restart' );

		/* Preparing info step progress bars */
		$progress_uploading._value  = $progress_uploading.find( '.rfbwp-value' );
		$progress_uploading._line   = $progress_uploading.find( '.rfbwp-line' );
		$progress_uploading._time   = $progress_uploading.find( '.rfbwp-time' );
		$progress_converting._value = $progress_converting.find( '.rfbwp-value' );
		$progress_converting._line  = $progress_converting.find( '.rfbwp-line' );
		$progress_converting._time  = $progress_converting.find( '.rfbwp-time' );
		$progress_importing._value  = $progress_importing.find( '.rfbwp-value' );
		$progress_importing._line   = $progress_importing.find( '.rfbwp-line' );
		$progress_importing._time   = $progress_importing.find( '.rfbwp-time' );

		_convertion_faux_progress = 0;
		_convertion_last_progress = 0;
		_converted_images_index   = 0;
		_uploaded_images          = [];
		_uploaded_images_zoom     = [];
		_flipbook.pages           = [];

		restart_form();

		check_server_connection();
	} );

	/* Init after server response */
	$window.on( 'rfbwp-pdf-init', function() {
		/* Flipbook name */
		$input_name.focus();
		$input_name.on( 'keyup', function() {
			_flipbook_name = $input_name.val().trim();

			if ( _flipbook_name.length >= 3 ) {
				$step_pdf.addClass( 'rfbwp-active' );
			} else {
				$step_pdf.removeClass( 'rfbwp-active' );
				$step_convert.removeClass( 'rfbwp-active' );
			}
		} );

		/* File upload */
		$input_pdf.fileupload( {
			url: _server_url + 'upload.php',
			dataType: 'json',
			autoUpload: false,
			add: function( event, data ) {
				var _pdf_size = data.files[ 0 ].size / 1024 / 1024;

				$input_pdf_overlay.text( data.files[ 0 ].name )
					.parent().addClass( 'rfbwp-active' );

				if ( _pdf_size <= 100 ) {
					$step_convert.addClass( 'rfbwp-active' );

					set_error_value( '' );

					_uploader_data = data;
				} else {
					$step_convert.removeClass( 'rfbwp-active' );

					set_error_value( 'size' );
				}
			},
			start: display_progress_step,
			progress: function( event, data ) {
				var _loaded = data.loaded / data.total,
					_progress = parseInt( _loaded * 100, 10 ) + '%';

				set_progress_value( $progress_uploading, _progress );

				var _elapsed_time = ( Date.now() - _estimation_time ) / 1000,
					_end_time = ( _elapsed_time / _loaded ) * ( 1 - _loaded );

				if ( _elapsed_time > 5 )
					$progress_uploading._time.text( '( ' + ( _end_time >> 0 ) + 'sec )' );
			},
			done: function( event, data ) {
				set_progress_value( $progress_uploading, '100%' );
				_uploader_result = data.result.files[ 0 ];

				$progress_uploading._time.hide();
				$progress_uploading._time.text( $progress_uploading._time.attr( 'data-text' ) );

				if ( typeof _uploader_result.error != 'undefined' )
					set_error_value( 'upload' );
				else
					get_upload_info();
			},
			fail: function() {
				set_error_value( 'upload' );
			}
		} );

		/* Zoom options */
		$zoom.on( 'change', function() {
			if ( $zoom.is( ':checked' ) ) {
				_enable_zoom = 1;
				$zoom_wrap.find( 'input[type=text]' ).prop( 'disabled', false );
			} else {
				_enable_zoom = 0;
				$zoom_wrap.find( 'input[type=text]' ).prop( 'disabled', true );
			}
		} );

		/* Validate options */
		$options.on( 'blur', function() {
			var $this = $( this ),
				_base = $this.val(),
				_default = parseInt( $this.attr( 'data-default' ) ),
				_min = parseInt( $this.attr( 'data-min' ) ),
				_max = parseInt( $this.attr( 'data-max' ) ),
				_value = isNaN( parseInt( _base ) ) ? _default : Math.max( Math.min( Math.abs( parseInt( _base ) ), _max ), _min );

			$this.val( _value );

			if ( _base != _value ) {
				$this.addClass( 'rfbwp-blink' );
				setTimeout( function() {
					$this.removeClass( 'rfbwp-blink' );
				}, 1000 );
			}

			$options.each( function() {
				var $this = $( this );

				_options[ $this[ 0 ].name.replace( 'rfbwp_', '' ) ] = $this.val();
			} );
		} );
		$options.each( function() {
			var $this = $( this );

			_options[ $this[ 0 ].name.replace( 'rfbwp_', '' ) ] = $this.val();
		} );

		/* Begin convertion */
		$input_convert.on( 'click', function( event ) {
			event.preventDefault();
			
			$window.bind( 'beforeunload', function(e) {				
				return mpcthPDFLocalize.refreshBlockMessage;
				e.preventDefault();					
			});
			
			_uploader_data.submit();
		} );

		/* Restart form */
		$restart_form.on( 'click', function( event ) {
			event.preventDefault();

			restart_form();
		} );
	} );

	/* PDF Wizard toggle */
	$( '.wrap' ).on( 'click', 'a.convert-book', function( e ) {
		e.preventDefault();

		$( '#field-rfbwp_books' ).hide();

		$( '.wrap' ).addClass( 'rfbwp-mode-pdf-wizard' );
		$( window ).trigger( 'rfbwp-init-pdf-wizard' );

		$( '#field-rfbwp_books' ).fadeIn( '250' );
	});
	$( '.wrap' ).on( 'click', 'a.cancel-convert-book', function( e ) {
		e.preventDefault();

		$( '#field-rfbwp_books' ).hide();

		$( '.wrap' ).removeClass( 'rfbwp-mode-pdf-wizard' );

		$( '#field-rfbwp_books' ).fadeIn( '250' );
	});
	$( window ).on( 'rfbwp-pdf-convert-end', function() {
		$( '.wrap a.cancel-convert-book' ).click();

		setTimeout( function() {
			rfbwp_refresh_bookshelf();
		}, 250 );
	} );
	/* END - PDF Wizard toggle */
} );