<?php
/* PAGE MARKUP */
function rfbwp_display_pdf_wizard() {
	ob_start();
	?>
	<div id="rfbwp_pdf_wizard" class="rfbwp-pdf-wizard-wrap">
		<div id="rfbwp_error_box" class="rfbwp-error-box" data-error="">
			<div class="rfbwp-error-server rfbwp-error">
				<h3><?php _e( 'Server connection issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server didn\'t respond to request.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-size rfbwp-error">
				<h3><?php _e( 'PDF size issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'PDF size is larger then 50MB.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-upload rfbwp-error">
				<h3><?php _e( 'PDF upload issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'PDF couldn\'t be uploaded to the server.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-pages rfbwp-error">
				<h3><?php _e( 'PDF pages issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'PDF have too few pages. It should have at least 3 pages.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-info rfbwp-error">
				<h3><?php _e( 'PDF data issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t extract PDF data for convertion.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-convertion rfbwp-error">
				<h3><?php _e( 'PDF convertion issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t convert PDF to images.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-images rfbwp-error">
				<h3><?php _e( 'Images list issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t list converted images.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-import rfbwp-error">
				<h3><?php _e( 'Images import issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t import converted images.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-generate rfbwp-error">
				<h3><?php _e( 'Imported images issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Too few images imported. Flipbook should have at least 3 pages.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
		</div>
		<div id="rfbwp_step_options" class="rfbwp-step">
			<h3><?php _e( 'Step 1: <em>Set Flipbook options</em>', 'rfbwp-pdf' ); ?></h3>
			<label for="rfbwp_name"><?php _e( 'Flipbook name:', 'rfbwp-pdf' ); ?></label>
			<input id="rfbwp_name" type="text" name="rfbwp_name" placeholder="<?php _e( 'Flipbook name', 'rfbwp-pdf' ); ?>">

			<div class="rfbwp-col-left">
				<h5><?php _e( 'Normal page', 'rfbwp-pdf' ); ?></h5>
				<label for="rfbwp_width">
					<?php _e( 'Max width:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_width" class="rfbwp-validate-option" type="text" name="rfbwp_width" placeholder="<?php _e( 'Width', 'rfbwp-pdf' ); ?>" value="600"
						data-default="600"
						data-min="100"
						data-max="1000">
					<span>px</span>
				</label>
				<label for="rfbwp_height">
					<?php _e( 'Max height:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_height" class="rfbwp-validate-option" type="text" name="rfbwp_height" placeholder="<?php _e( 'Height', 'rfbwp-pdf' ); ?>" value="600"
						data-default="600"
						data-min="100"
						data-max="1000">
					<span>px</span>
				</label>
				<label for="rfbwp_quality">
					<?php _e( 'Quality:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_quality" class="rfbwp-validate-option" type="text" name="rfbwp_quality" placeholder="<?php _e( 'Quality', 'rfbwp-pdf' ); ?>" value="75"
						data-default="75"
						data-min="1"
						data-max="100">
					<span>%</span>
				</label>
			</div>

			<div id="rfbwp_zoom_wrap" class="rfbwp-col-right">
				<h5><?php _e( 'Zoom page', 'rfbwp-pdf' ); ?></h5>
				<label for="rfbwp_zoom" class="rfbwp-zoom">
					<input id="rfbwp_zoom" type="checkbox" name="rfbwp_zoom" checked="checked">
					<?php _e( 'Enabled', 'rfbwp-pdf' ); ?>
				</label>
				<label for="rfbwp_zoom_width">
					<?php _e( 'Max width:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_zoom_width" class="rfbwp-validate-option" type="text" name="rfbwp_zoom_width" placeholder="<?php _e( 'Width', 'rfbwp-pdf' ); ?>" value="1200"
						data-default="1200"
						data-min="100"
						data-max="2000">
					<span>px</span>
				</label>
				<label for="rfbwp_zoom_height">
					<?php _e( 'Max height:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_zoom_height" class="rfbwp-validate-option" type="text" name="rfbwp_zoom_height" placeholder="<?php _e( 'Height', 'rfbwp-pdf' ); ?>" value="1200"
						data-default="1200"
						data-min="100"
						data-max="2000">
					<span>px</span>
				</label>
				<label for="rfbwp_zoom_quality">
					<?php _e( 'Quality:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_zoom_quality" class="rfbwp-validate-option" type="text" name="rfbwp_zoom_quality" placeholder="<?php _e( 'Quality', 'rfbwp-pdf' ); ?>" value="90"
						data-default="90"
						data-min="1"
						data-max="100">
					<span>%</span>
				</label>
			</div>
		</div>
		<div id="rfbwp_step_pdf" class="rfbwp-step">
			<h3><?php _e( 'Step 2: <em>Select PDF</em>', 'rfbwp-pdf' ); ?></h3>
			<div class="rfbwp-link">
				<input id="rfbwp_pdf" type="file" name="files[]">
				<span id="rfbwp_pdf_overlay" class="rfbwp-file-name" data-text="<?php _e( 'Click to select file', 'rfbwp-pdf' ); ?>"><?php _e( 'Click to select file', 'rfbwp-pdf' ); ?></span>
			</div>
		</div>
		<div id="rfbwp_step_convert" class="rfbwp-step">
			<h3><?php _e( 'Step 3: <em>Start convertion</em>', 'rfbwp-pdf' ); ?></h3>
			<a href="#" id="rfbwp_convert" class="rfbwp-link"><?php _e( 'Convert PDF', 'rfbwp-pdf' ); ?></a>
		</div>
		<div id="rfbwp_step_info" class="rfbwp-step">
			<h3><?php _e( 'Convertion in progress', 'rfbwp-pdf' ); ?></h3>
			<div id="rfbwp_uploading" class="rfbwp-progress">
				<span><?php _e( 'Uploading: ' ); ?></span>
				<span class="rfbwp-value">0%</span>
				<span class="rfbwp-time" data-text="<?php _e( '( calculating... )', 'rfbwp-pdf' ); ?>"><?php _e( '( calculating... )', 'rfbwp-pdf' ); ?></span>
				<span class="rfbwp-line"></span>
			</div>
			<div id="rfbwp_converting" class="rfbwp-progress">
				<span><?php _e( 'Converting: ' ); ?></span>
				<span class="rfbwp-value">0%</span>
				<span class="rfbwp-time" data-text="<?php _e( '( calculating... )', 'rfbwp-pdf' ); ?>"><?php _e( '( calculating... )', 'rfbwp-pdf' ); ?></span>
				<span class="rfbwp-line"></span>
			</div>
			<div id="rfbwp_importing" class="rfbwp-progress">
				<span><?php _e( 'Importing: ' ); ?></span>
				<span class="rfbwp-value">0%</span>
				<span class="rfbwp-time" data-text="<?php _e( '( calculating... )', 'rfbwp-pdf' ); ?>"><?php _e( '( calculating... )', 'rfbwp-pdf' ); ?></span>
				<span class="rfbwp-line"></span>
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
}

/*----------------------------------------------------------------------------*\
	AJAX REQUESTS
\*----------------------------------------------------------------------------*/

/* Importing single image */
add_action( 'wp_ajax_rfbwp_import_single_image', 'rfbwp_import_single_image' );
function rfbwp_import_single_image() {
	if ( ! isset( $_REQUEST[ 'image_path' ] ) && ! isset( $_REQUEST[ 'fb_name' ] ) )
		return;

	$zoom = '';
	if ( isset( $_REQUEST[ 'zoom' ] ) )
		$zoom = '(zoom) ';

	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	$uploaded_file  = wp_upload_bits( $_REQUEST[ 'fb_name' ] . ' - ' . $zoom . basename( $_REQUEST[ 'image_path' ] ), null, file_get_contents( $_REQUEST[ 'image_path' ] ) );
	$wp_upload_dir  = wp_upload_dir();
	$file_path      = $wp_upload_dir[ 'basedir' ] . str_replace( $wp_upload_dir[ 'baseurl' ], '', $uploaded_file[ 'url' ] );
	$parent_post_id = 0;
	$filetype       = wp_check_filetype( basename( $file_path ), null );
	$file_data      = array(
		'guid'           => $wp_upload_dir[ 'url' ] . '/' . basename( $file_path ),
		'post_mime_type' => $filetype[ 'type' ],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file_path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	$file_id       = wp_insert_attachment( $file_data, $file_path, $parent_post_id );
	$file_metadata = wp_generate_attachment_metadata( $file_id, $file_path );
	wp_update_attachment_metadata( $file_id, $file_metadata );

	$file_info = array(
		'url'    => $uploaded_file[ 'url' ],
		'width'  => $file_metadata[ 'width' ],
		'height' => $file_metadata[ 'height' ]
	);

	echo json_encode( $file_info );

	die(0);
}

/* Creating new flipbook */
add_action( 'wp_ajax_rfbwp_create_new_flipbook', 'rfbwp_create_new_flipbook' );
function rfbwp_create_new_flipbook() {
	if ( ! isset( $_REQUEST[ 'flipbook' ] ) )
		return;

	$base_flipbook = array(
		'rfbwp_fb_force_open' => '0',
		'rfbwp_fb_border_size' => '0',
		'rfbwp_fb_border_color' => '',
		'rfbwp_fb_border_radius' => '0',
		'rfbwp_fb_outline' => '0',
		'rfbwp_fb_outline_color' => '',
		'rfbwp_fb_inner_shadows' => '1',
		'rfbwp_fb_edge_outline' => '0',
		'rfbwp_fb_edge_outline_color' => '',
		'rfbwp_fb_fs_color' => '#ededed',
		'rfbwp_fb_fs_opacity' => '95',
		'rfbwp_fb_fs_icon_color' => '1',
		'rfbwp_fb_toc_display_style' => '0',
		'rfbwp_fb_heading_font' => '0',
		'rfbwp_fb_heading_family' => 'default',
		'rfbwp_fb_heading_fontstyle' => 'regular',
		'rfbwp_fb_heading_size' => '24',
		'rfbwp_fb_heading_line' => '',
		'rfbwp_fb_heading_color' => '#2b2b2b',
		'rfbwp_fb_content_font' => '0',
		'rfbwp_fb_content_family' => 'default',
		'rfbwp_fb_content_fontstyle' => 'regular',
		'rfbwp_fb_content_size' => '24',
		'rfbwp_fb_content_line' => '',
		'rfbwp_fb_content_color' => '#2b2b2b',
		'rfbwp_fb_num_font' => '0',
		'rfbwp_fb_num_family' => 'default',
		'rfbwp_fb_num_fontstyle' => 'regular',
		'rfbwp_fb_num_size' => '24',
		'rfbwp_fb_num_line' => '',
		'rfbwp_fb_num_color' => '#2b2b2b',
		'rfbwp_fb_toc_font' => '0',
		'rfbwp_fb_toc_family' => 'default',
		'rfbwp_fb_toc_fontstyle' => 'regular',
		'rfbwp_fb_toc_size' => '24',
		'rfbwp_fb_toc_line' => '',
		'rfbwp_fb_toc_color' => '#2b2b2b',
		'rfbwp_fb_zoom_border_size' => '10',
		'rfbwp_fb_zoom_border_color' => '#ECECEC',
		'rfbwp_fb_zoom_border_radius' => '10',
		'rfbwp_fb_zoom_outline' => '1',
		'rfbwp_fb_zoom_outline_color' => '#D0D0D0',
		'rfbwp_fb_sa_thumb_cols' => '3',
		'rfbwp_fb_sa_thumb_border_size' => '1',
		'rfbwp_fb_sa_thumb_border_color' => '#878787',
		'rfbwp_fb_sa_vertical_padding' => '10',
		'rfbwp_fb_sa_horizontal_padding' => '10',
		'rfbwp_fb_sa_border_size' => '10',
		'rfbwp_fb_sa_border_color' => '#F6F6F6',
		'rfbwp_fb_sa_border_radius' => '10',
		'rfbwp_fb_sa_outline' => '1',
		'rfbwp_fb_sa_outline_color' => '#D6D6D6',
		'rfbwp_fb_nav_menu_type' => '0',
		'rfbwp_fb_nav_menu_position' => 'bottom',
		'rfbwp_fb_nav_stack' => '0',
		'rfbwp_fb_nav_text' => '0',
		'rfbwp_fb_nav_toc' => '1',
		'rfbwp_fb_nav_toc_order' => '1',
		'rfbwp_fb_nav_toc_index' => '2',
		'rfbwp_fb_nav_toc_icon' => 'fa fa-th-list',
		'rfbwp_fb_nav_zoom' => '1',
		'rfbwp_fb_nav_zoom_order' => '2',
		'rfbwp_fb_nav_zoom_icon' => 'fa fa-search-plus',
		'rfbwp_fb_nav_zoom_out_icon' => 'fa fa-search-minus',
		'rfbwp_fb_nav_ss' => '1',
		'rfbwp_fb_nav_ss_order' => '3',
		'rfbwp_fb_nav_ss_delay' => '2000',
		'rfbwp_fb_nav_sap_icon_next' => 'fa fa-chevron-down',
		'rfbwp_fb_nav_sap_icon_prev' => 'fa fa-chevron-up',
		'rfbwp_fb_nav_ss_icon' => 'fa fa-play',
		'rfbwp_fb_nav_ss_stop_icon' => 'fa fa-pause',
		'rfbwp_fb_nav_sap' => '1',
		'rfbwp_fb_nav_sap_order' => '4',
		'rfbwp_fb_nav_sap_icon' => 'fa fa-th',
		'rfbwp_fb_nav_sap_icon_close' => 'fa fa-times',
		'rfbwp_fb_nav_fs' => '1',
		'rfbwp_fb_nav_fs_order' => '5',
		'rfbwp_fb_nav_fs_icon' => 'fa fa-expand',
		'rfbwp_fb_nav_fs_close_icon' => 'fa fa-compress',
		'rfbwp_fb_nav_arrows' => '1',
		'rfbwp_fb_nav_arrows_toolbar' => '1',
		'rfbwp_fb_nav_prev_icon' => 'fa fa-chevron-left',
		'rfbwp_fb_nav_next_icon' => 'fa fa-chevron-right',
		'rfbwp_fb_nav_general' => '1',
		'rfbwp_fb_nav_general_v_padding' => '15',
		'rfbwp_fb_nav_general_h_padding' => '15',
		'rfbwp_fb_nav_general_margin' => '20',
		'rfbwp_fb_nav_general_fontsize' => '22',
		'rfbwp_fb_nav_general_bordersize' => '0',
		'rfbwp_fb_nav_general_shadow' => '0',
		'rfbwp_fb_nav_default' => '1',
		'rfbwp_fb_nav_default_color' => '#2b2b2b',
		'rfbwp_fb_nav_default_background' => '',
		'rfbwp_fb_nav_hover' => '1',
		'rfbwp_fb_nav_hover_color' => '#22b4d8',
		'rfbwp_fb_nav_hover_background' => '',
		'rfbwp_fb_nav_border_default' => '1',
		'rfbwp_fb_nav_border_color' => '',
		'rfbwp_fb_nav_border_radius' => '2',
		'rfbwp_fb_nav_border_hover' => '1',
		'rfbwp_fb_nav_border_hover_color' => '',
		'rfbwp_fb_nav_border_hover_radius' => '2',
		'rfbwp_fb_num' => '0',
		'rfbwp_fb_num_hide' => '1',
		'rfbwp_fb_num_style' => '0',
		'rfbwp_fb_num_background' => '',
		'rfbwp_fb_num_border' => '0',
		'rfbwp_fb_num_border_color' => '',
		'rfbwp_fb_num_border_size' => '2',
		'rfbwp_fb_num_border_radius' => '2',
		'rfbwp_fb_num_v_position' => 'top',
		'rfbwp_fb_num_h_position' => 'center',
		'rfbwp_fb_num_v_padding' => '12',
		'rfbwp_fb_num_h_padding' => '10',
		'rfbwp_fb_num_v_margin' => '12',
		'rfbwp_fb_num_h_margin' => '10',
		'rfbwp_fb_hc' => '',
		'rfbwp_fb_hc_fco' => '',
		'rfbwp_fb_hc_fci' => '',
		'rfbwp_fb_hc_fcc' => '',
		'rfbwp_fb_hc_bco' => '',
		'rfbwp_fb_hc_bci' => '',
		'rfbwp_fb_hc_bcc' => ''
	);

	$fb_table = 'rfbwp_options';
	$fb_options = get_option( $fb_table );
	$fb_book_id = count( $fb_options['books'] );

	$new_flipbook = $_REQUEST[ 'flipbook' ];
	$new_flipbook = array_merge( $base_flipbook, $new_flipbook );

	$new_sh_id = strtolower( str_replace( ' ', '_', $new_flipbook[ 'rfbwp_fb_name' ] ) );
	foreach ( $fb_options[ 'books' ] as $book ) {
		if ( $book[ 'rfbwp_fb_name' ] != '' ) {
			$sh_id = strtolower( str_replace( ' ', '_', $book[ 'rfbwp_fb_name' ] ) );

			if ( $new_sh_id == $sh_id )
				$new_flipbook[ 'rfbwp_fb_name' ] = $new_flipbook[ 'rfbwp_fb_name' ] . ' (' . $fb_book_id . ')';
		}
	}

	unregister_setting( $fb_table, $fb_table, 'mp_validate_options' );

	$fb_options[ 'books' ][ $fb_book_id ] = $new_flipbook;

	update_option( $fb_table, $fb_options );

	register_setting( $fb_table, $fb_table, 'mp_validate_options' );

	die(0);
}
