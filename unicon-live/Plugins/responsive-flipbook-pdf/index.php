<?php

/**
Plugin Name: PDF Wizard - Responsive FlipBook WP Extension
Plugin URI: http://codecanyon.net/user/mpc
Description: This plugin extend Responsive Flip Book Wordpress Plugin with PDF support.
Version: 1.3
Author: MassivePixelCreation
Author URI: http://codecanyon.net/user/mpc
Text Domain: rfbwp-pdf
Domain Path: /languages/
**/

add_action( 'plugins_loaded',	'rfb_pdf_localization' );
function rfb_pdf_localization() {
	load_plugin_textdomain( 'rfbwp-pdf', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'admin_init', 'rfb_pdf_init', 99999 );
function rfb_pdf_init() {
	if( !defined( 'MPC_PLUGIN_FILE' ) )
		return;

	$plugin_path = plugin_basename( MPC_PLUGIN_FILE );

	if( is_plugin_inactive( $plugin_path ) )
		return;

	define( 'RFB_PDF_PLUGIN_ROOT', plugin_dir_url( __FILE__ ) );

	add_action( 'rfbwp/scripts',	'rfb_pdf_enqueue_scripts' );

	add_filter( 'rfbwp/addNewBook',       'rfb_pdf_convert_button' );
	add_filter( 'rfbwp/afterTable',       'rfb_pdf_display_wizard' );
	add_filter( 'rfbwp/addNewBookBottom', 'rfb_pdf_convert_button' );
	add_filter( 'rfbwp/addNewBookBottom', 'rfb_pdf_cancel_button' );

	require_once( plugin_dir_path( __FILE__ ) . '/pdf_wizard.php' );
}

function rfb_pdf_enqueue_scripts() {
	/* PDF Wizard */
	wp_enqueue_style( 'rfbwp-pdf-css', RFB_PDF_PLUGIN_ROOT . '/css/pdf_wizard.css' );
	wp_enqueue_script( 'rfbwp-upload-js', RFB_PDF_PLUGIN_ROOT . '/js/jquery.fileupload.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'rfbwp-pdf-js', RFB_PDF_PLUGIN_ROOT . '/js/pdf_wizard.min.js', array( 'jquery', 'rfbwp-upload-js' ) );

	wp_localize_script( 'rfbwp-pdf-js', 'mpcthPDFLocalize', array(
		'refreshBlockMessage' => __( "Are you sure you want to end conversion? If the process don't finish your FlipBook wont be created.", 'rfbwp-pdf' )
	) );
}

function rfb_pdf_convert_button( $content ) {
	return '<a class="convert-book mpc-button revert" href="#"><i class="dashicons dashicons-book"></i> ' . __( 'Convert PDF', 'rfbwp-pdf' ) .'</a>' . $content;
}

function rfb_pdf_display_wizard( $content ) {
	return rfbwp_display_pdf_wizard();
}

function rfb_pdf_cancel_button( $content ) {
	return $content . '<a class="cancel-convert-book mpc-button revert close" href="#"><i class="dashicons dashicons-no"></i> ' . __( 'Cancel', 'rfbwp-pdf' ) .'</a>';
}

/* Notices */
add_action( 'admin_notices', 'rfbwp_pdf_urlfopen_notice' );
function rfbwp_pdf_urlfopen_notice( ) {
	$rfbwp_version = get_option( 'rfbwp_version' );

	if( $rfbwp_version <= '2.0' )
		return;

	if( ini_get( 'allow_url_fopen' ) )
		return;

	$dismiss = get_option( 'rfbwp_pdf_urlfopen_notice' );

	if( $dismiss )
		return;
	?>
	<div class="rfbwp-notice rfbwp-notice-error rfbwp-pdf-urlfopen-notice">
		<strong><?php _e( 'We found a problem: ', 'rfbwp-pdf' ); ?></strong>
			<?php _e( 'PDF Wizard cannot work at your website due to your server configuration. Please contact with your hosting provider and ask to enable <strong><em>allow_url_fopen</em></strong> inside php.ini.', 'rfbwp-pdf' ); ?>

		<!--<a href="#" class="rfbwp-notice-dismiss" data-notice="rfbwp_pdf_urlfopen_notice"><i class="dashicons dashicons-no-alt"></i></a>-->
	</div>
	<?php
}