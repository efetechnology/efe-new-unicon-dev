<?php

function wp_enqueue_scripts_2_func() {  
  wp_enqueue_script( 'tab-page-js', get_stylesheet_directory_uri() . '/asset/jquery.tabpager.js', array(), false, true );
  wp_enqueue_style( 'custom_stylesheet-2', get_stylesheet_directory_uri() . '/custom-2.css' );  
  wp_enqueue_script( 'custom_script-2', get_stylesheet_directory_uri() . '/custom-2.js', array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'wp_enqueue_scripts_2_func' );

/**** Add by Rain ***/
add_filter('admin_footer', 'disable_notice_acf'); 
function disable_notice_acf () {
	echo '<style>
		#acf-upgrade-notice{
 display: none !important;
}
</style>';
}

// Shortcode carrer human page
add_shortcode( 'career_human' , function( $attr ) {
  ob_start();
  set_query_var( 'type', $attr['type'] );
  get_template_part( 'custom-template/shortcode', 'career_human' );
  $render = ob_get_contents();
  ob_end_clean();
  return $render;
} );

// Shortcode carrer handbook page
add_shortcode( 'career_handbook' , function( $attr ) {
  ob_start();
  get_template_part( 'custom-template/shortcode', 'career_handbook' );
  $render = ob_get_contents();
  ob_end_clean();
  return $render;
} );


// Shortcode About Unicon tab success
add_shortcode( 'about_unicon_success' , function( $attr ) {
  ob_start();
  get_template_part( 'custom-template/shortcode', 'about_unicon_success' );
  $render = ob_get_contents();
  ob_end_clean();
  return $render;
} ); 

// Shortcode Why select Unicons Tab people Unicons
add_shortcode( 'why_unicons_people_unicons' , function( $attr ) {
  ob_start();
  get_template_part( 'custom-template/shortcode', 'why_unicons_people_unicons' );
  $render = ob_get_contents();
  ob_end_clean();
  return $render;
} );