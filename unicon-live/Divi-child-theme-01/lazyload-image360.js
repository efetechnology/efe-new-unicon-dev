(function($) {
	$(document).ready(function(){
		$(".tab-image-360").click(function(){
			var container = $(this).parents(".group-detail").eq(0).find(".image-360-container").eq(0);
			if (!container.hasClass("loaded") && !container.hasClass("loading")) {
				container.addClass("loading");
				$.ajax({
					method: "POST",
					dataType: "json",
					url: location.origin + "/wp-admin/admin-ajax.php",
					data: {
						action: 'load_image360',
						project_id: container.data("id")
					},
					success: function(data, status, jq) {
						container.removeClass("loading");
						if (data.success) {
							setTimeout(function(){
								var panorama, viewer;
								panorama = new PANOLENS.ImagePanorama( data.url );
								viewer = new PANOLENS.Viewer( { output: 'console', container: container[0] } );
								viewer.add( panorama );
								container.addClass('loaded');
							}, 1000);
						}
					},
					error: function(jq, text, errthrow) {
						container.removeClass("loading");
						container.html("Unable to load this image, please click on tab to try again.");
						console.log(text);
					}
				});
			}
		});
		$(".media_fullwidth.image_360_info .small_post_item").click(function(){
			console.log('click');
			$(".media_fullwidth.image_360").addClass("loading");
			$.ajax({
				method: "POST",
				dataType: "json",
				url: location.origin + "/wp-admin/admin-ajax.php",
				data: {
					action: 'load_image360_post',
					post_id: $(this).data('image-360')
				},
				success: function(data, status, jq) {
					$(".media_fullwidth.image_360").removeClass("loading");
					$(".image_360_info .big_post_title h1").html(data.title);
					$(".image_360_info .big_post_excerpt strong").html(data.excerpt);
					$(".image_360_info .big_post_content").html(data.content);
					
					$(".media_fullwidth.image_360").html('');
					var panorama, viewer;
					panorama = new PANOLENS.ImagePanorama( data.url );
					viewer = new PANOLENS.Viewer( { output: 'console', container: $(".media_fullwidth.image_360")[0] } );
					viewer.add( panorama );
				},
				error: function(jq, text, errthrow) {
					$(".media_fullwidth.image_360").removeClass("loading");
					$(".media_fullwidth.image_360").html("Unable to load this image, please click on tab to try again.");
					console.log(text);
				}
			});
		});
	});
})(jQuery);