<?php

/**
 * Enqueue
 */
function wp_enqueue_scripts_4_func() {
    wp_enqueue_style( 'custom_stylesheet-4', get_stylesheet_directory_uri() . '/custom-4.css' );
    wp_enqueue_script( 'custom_script-4', get_stylesheet_directory_uri() . '/custom-4.js', array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'wp_enqueue_scripts_4_func' );

/**
 * Shortcode [media_fullwidth]
 */
add_shortcode( 'media_fullwidth' , function( $attr ) {
    ob_start();
    set_query_var( 'type', $attr['type'] );
    get_template_part( 'custom-template/shortcode', 'media_fullwidth' );
    $render = ob_get_contents();
    ob_end_clean();
    return $render;
} );