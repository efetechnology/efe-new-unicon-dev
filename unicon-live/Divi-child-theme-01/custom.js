(function($) {

    /**
     * Object storing all function that will be excuted
     *
     * IMPORTANT: all functions in this class will be called by default. Just put function into this.
     */
    var Executor = {};

    /**
     * Helper or library functions, will not be call be default
     */
    var Helper = {};

    /**
     * Global class
     * Use to store all variables that we might use throught many functions
     */
    var Global = {};

    /**********************************************************************************************/
    // Pleae put all functions below this line

    /**
     * Get URL query variables and return it as associate array
     */
    Helper.getUrlVars = function() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    /**
     * jQuery extend function, make elements have the same height
     */
    $.fn.sameHeight = function() {
        if (this.length <= 1) return this;
        var max = 0,
            this_;
        this
            .each(function() {
                this_ = $(this);
                if (this_.outerHeight() > max) max = this_.outerHeight();
            });
        this
            .each(function() {
                if (this_.outerHeight() != max) $(this).css('height', max + 'px');
            });
        return this;
    }

    /**
     * Cache Variables
     * You can declare all variables that we might need to user in many functions
     */
    Global.queryVars = Helper.getUrlVars();
    Global.screenWidth = $(window).width();
    Global.mainMenu = $('#top-menu');
    Global.rightMenu = $('#right-menu');
    Global.menuSearchInput = $('#menu-search');
    Global.heroFullWidthSlider = $('#hero-fullwidth-slider');

    /**
     * Main Menu
     * Style the sub menu have the same top
     */
    Executor.submenuSameTop = function() {

        if (Global.screenWidth < 1024) return;

        var mainSubmenu = Global.mainMenu.find('.sub-menu');

        var submenuPaddingTop = 0;
        var submenuBorderTop = 0;
        var spaceTop = submenuPaddingTop + submenuBorderTop + 'px';

        var transformTop;

        // Make sub menus have the same top
        mainSubmenu.each(function() {
            $(this).find('> li').each(function(index) {
                transformTop = '-' + index + '00%';
                $(this).find('.sub-menu').css('top', 'calc(' + transformTop + ' - ' + spaceTop + ')');
            });
        });

    }

    /**
     * Main Menu
     * Set description box width on menu box
     */
    Executor.descriptionMenuWidth = function() {
        if (Global.screenWidth < 1024) return;
        var top_menu = $("#top-menu");
        var max_width = top_menu.width();
        $(".menu-item-object-menu_description").each(function() {
            var width = max_width - ($(this).offset().left - top_menu.offset().left) - 40;
            if (width > 200)
                $(this).find("table").eq(0).width(width);
        });
    };

    /**
     * Main Menu
     * Style the sub menu have the same height
     */
    /*
    Executor.submenuSameHeight = function() {

    	if ( Global.screenWidth < 1024 ) return;

    	var firstLevelSubmenu = Global.mainMenu.find('> li > .sub-menu');
    	var highest, this_;

    	firstLevelSubmenu.each( function(){
    		highest = 0;
    		this_ = $(this);
    		this_ = this_.add( this_.find( '.sub-menu' ) );
    		this_
    			// find highest sub-menu
    			.each( function(){ highest = $( this ).height() > highest ? $( this ).height() : highest; } )
    			// set the same for sub menus
    			.css( 'height' , highest + 'px' )
    			// set height for text table inside sub menu, to make the text and image vertical middle.
    			.find( 'li table' ).css( 'height' , highest + 'px' );
    	} );
    }
    */

    /**
     * MAIN MENU
     * Move the flags before search icon
     */
    Executor.moveFlagsBeforeSearchIcon = function() {

        if (!Global.mainMenu.length) return;

        Global.mainMenu.find('.special-menu-item.search-icon')
            .insertBefore(
                Global.mainMenu.find('.pll-parent-menu-item')
            );
    }

    /**
     * Move the right menu (languages and search icon)
     * in to the dropdown menu
     */
    Executor.rightMenuToMobileMenu = function() {

        if (Global.screenWidth >= 1024) return;

        Global.rightMenu.appendTo(Global.mainMenu);
    }

    /**
     * Write the value to menu search input
     */
    Executor.valueSearchInput = function() {
        if (Global.queryVars.s)
            Global.menuSearchInput.val(Global.queryVars.s);
    }

    /**
     * Excute marquee
     */
    Executor.marquee = function() {
        $('.marquee').marquee();
    }

    /**
     * Run carousel effect for client_logo_carousel shortcode
     */
    Executor.clientLogoCarousel = function() {

        $('.client_logo_carousel').slick({

            arrows: true,
            slidesToShow: 8,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            pauseOnHover: false,
            pauseOnFocus: false,
            responsive: [{
                    breakpoint: 1024,
                    settings: { slidesToShow: 4 }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        arrows: true
                    }
                },
            ]
        });
    }

    /**
     * 
     */
    Executor.sameHeightColumns = function() {

        if (Global.screenWidth < 1024) return;

        $('.have_same_height_columns .same_height_column').sameHeight();

    }

    /**
     * Set media_publications items info have the same height
     */
    Executor.mediaPublicationSameHeight = function() {

        if (Global.screenWidth < 1024) return;

        $('.media_publications .item_info').sameHeight();

    }

    /**
     * Display the slick slide but not visible
     * for slick detect the width
     */
    Helper.readyForRunSlickInsideTab = function(tabContain) {

        var unbind = false;

        tabContain
            .addClass('tab_not_default_has_slick_slide')
            .attrchange({
                callback: function() {

                    if (unbind) return;

                    if ($(this).css('opacity') === 0) return;

                    $(this).removeClass('tab_not_default_has_slick_slide');

                    unbind = true;
                }
            });
    }

    /**
     * Shortcode media_internal_magazine carousel
     */
    Executor.media_internal_magazineCarousel = function() {

        var media_internal_magazine = $('.media_internal_magazine');

        if (!media_internal_magazine.length) return;

        var tabContain = media_internal_magazine.parent();

        Helper.readyForRunSlickInsideTab(tabContain);

        media_internal_magazine.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: { slidesToShow: 2 }
                },
                {
                    breakpoint: 768,
                    settings: { slidesToShow: 1 }
                }
            ],
            prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next slick-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
        })

        var nextBtn = media_internal_magazine.find('.slick-next.slick-arrow');
        var prevBtn = media_internal_magazine.find('.slick-prev.slick-arrow');
        var dots = media_internal_magazine.find('.slick-dots');

        prevBtn.insertBefore(dots);
        nextBtn.insertAfter(dots);
    }

    /**
     * Media module
     * Image carousel gallery
     */
    Executor.mediaImageCarousel = function() {

        var view = $('.image-view');
        var nav = $('.image-nav');
        var tab = view.parents(".et_pb_tab ").eq(0);
        tab.css("display", "block");
        if (!view.length || !nav.length) return;

        var tabContain = view.closest('.et_pb_tab');

        Helper.readyForRunSlickInsideTab(tabContain);
        $(window).on('load', function() {
            view.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.image-nav',
                lazyLoad: 'ondemand',
            });

            nav.slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                vertical: true,
                verticalSwiping: true,
                asNavFor: '.image-view',
                centerMode: true,
                focusOnSelect: true,
                initialSlide: 2,
                //lazyLoad: 'progressive',
                prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-angle-up" aria-hidden="true"></i></button>',
                nextArrow: '<button type="button" class="slick-next slick-arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></button>',
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 5,
                            vertical: false,
                            verticalSwiping: false,
                            prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                            nextArrow: '<button type="button" class="slick-next slick-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                            vertical: false,
                            verticalSwiping: false,
                            prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                            nextArrow: '<button type="button" class="slick-next slick-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
                        }
                    }
                ],
            });
            tab.css("display", "none");
        });
    };

    /**
     * Main Services carousel
     */
    Executor.mainServicesCarousel = function() {
        $(document).ready(function() {
            $(".home .main_services > *").removeClass();
            $(".home .main_services").slick({
                slidesToShow: 3,
                centerPadding: '10px',
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    };
    /**
     * Set the same height for post list columns
     * inside the media content module
     */
    Executor.postListInMediaModuleSameHeight = function() {

        if (Global.screenWidth < 1024) return;

        var height = $('.media_fullwidth').find('.big_post').outerHeight();

        $('.media_fullwidth').find('.small_post_list').css('height', height + 'px');
    }

    /**
     * Set the same height for post list columns
     * inside the media content module
     */
   /* Executor.smallPostListScrollBar = function() {
        $('.media_fullwidth').find('.small_post_list').mCustomScrollbar({ theme: "custom-red" });
    }
*/
    /**
     * Set post list custom scroll bar
     */
    Executor.customScrollBar = function() {
        $('.small_post_list.custom_scroll_bar').mCustomScrollbar({ theme: "custom-red-right-bar" });
    }

    /**
     * Show 360 view when user click on 360 image
     */
    Executor.show360view = function() {
        $('.image_360_wraper').find('img').click(function() {
            $(this).fadeOut(400, function() {
                $('iframe#360_view')
                    .attr('src', 'http://flyingcam-vietnam.com/view360/nhietdienduyenhai/')
                    .css({
                        width: '100%',
                        height: '600px',
                    })
                    .fadeIn();
            });
        });
    };

    Executor.linkScrollToSlide = function() {
        $(".video-nav .small_post_item").click(function() {
            $(".latest_video_slider .slick-dots li").eq(jQuery(this).data("order")).click();
        });
    };

    Executor.collapseFooter = function() {
        $(".footer-collapse+div").each(function() {
            $(this).attr("data-height", $(this).height());
            if ($(window).width() < 992) {
                $(this).height("0px");
            }
        });
        $(".footer-collapse").addClass('footer-collapsed');
        $(".footer-collapse").click(function() {
            $(this).toggleClass('footer-collapsed');
            if ($(window).width() < 992) {
                var div_ = $(this).next("div");
                if ($(this).hasClass("footer-collapsed")) {
                    div_.css("height", "0px");
                } else {
                    div_.css("height", div_.data("height") + "px");
                }
            }
        });
    };

    Executor.recommendLetterSlider = function() {
        $(".recommend-letter img").each(function() {
            $(this).wrap("<div></div>");
        });
        $(".recommend-letter").clone().addClass("recommend-letter-thumbnail").removeClass("recommend-letter").insertAfter(".recommend-letter");
        $(".recommend-letter-thumbnail div:first-child").addClass("active");
        $(".recommend-letter").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            speed: 300,
            autoplay: true,
            dots: true,
            adaptiveHeight: true,
            autoplaySpeed: 4000
        });
        $(".recommend-letter").on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $(".recommend-letter-thumbnail div").eq(currentSlide).removeClass("active");
            $(".recommend-letter-thumbnail div").eq(nextSlide).addClass("active");
        });
        $(".recommend-letter-thumbnail div").click(function() {
            $(".recommend-letter .slick-dots li").eq($(this).index()).click();
        });
    };

    Executor.careerPositionForm = function() {
        if ($(window).width() > 992) {
            $("<div class=\"col-left\"></div>").insertBefore("#field_2_1");
            $("<div class=\"col-right\"></div>").insertBefore("#field_2_4");
            $("#field_2_1, #field_2_2, #field_2_3").appendTo(".col-left");
            $("#field_2_4, #field_2_5").appendTo(".col-right");
        }
    };

    Executor.jobFromHomepage = function() {
        var job = String(Global.queryVars.id);
        $(".leader").ready(function() {
            $(".career-position-wrap img[id=" + job + "]").click();
        });


    };

    Executor.keyPersonnelCollapse = function() {
        function toggleCollapseInfo(jq) {
            var _content = jq.find(".content").eq(0);
            if ($(_content).text().replace(/\s/g, "") !== "") {
                if (jq.hasClass("collapsed")) {
                    _content.height(_content.data("height"));
                } else {
                    _content.height(0);
                }
                jq.toggleClass("collapsed");
            }
        }
        $(window).on("load", function() {
            var _content = $(".leader .col-2 .content");
            var tab = _content.parents(".et_pb_tab_1").eq(0);
            tab.css("display", "block");
            _content.each(function() {
                $(this).attr("data-height", $(this).height());
            });
            tab.css("display", "none");
            _content.height(0);
        });
        $(".leader .col-2").addClass("collapsed");
        $(".leader .col-2").click(function() {
            if ($(this).hasClass("collapsed")) {
                $(".leader .col-2").each(function() {
                    if (!$(this).hasClass("collapsed")) {
                        toggleCollapseInfo($(this));
                    }
                });
            }
            toggleCollapseInfo($(this));
        });
    };

    Executor.getStat = function() {
        $("#stock-info").ready(function() {
            $.ajax({
                url: window.location.origin + "/wp-admin/admin-ajax.php",
                data: {
                    action: 'get_stat'
                },
                success: function(data, status, jqXHR) {
                    if (data.success) {
                        var figure = data.figure;
                        var date = data.date;
                        var deviate = data.deviate;
                        $("#stock-info #stock-num").html(figure);
                        $("#stock-info #stock-date").html(date);
                        $("#stock-info #stock-deviation").html(deviate);
                    }
                },
                error: function(jq, text, errthrow) {
                    console.log(text);
                }
            });
        });

    };

    Executor.pllMobile = function() {
        $(window).on('load', function() {
            $(".pll-mobile").click(function() {
                $(this).toggleClass("active");
            });
            $(".pll-mobile > *").click(function(event) {
                event.stopPropagation();
            });
        });
    };

    Executor.removeBlankSlideDescription = function() {
        $(document).ready(function() {
            $("#hero-fullwidth-slider .et_pb_slider_container_inner").each(function() {
                if ($(this).text().replace(/\s/g, "") === "") {
                    $(this).remove();
                }
            });
            if (Global.screenWidth < 767) {
                $("#hero-fullwidth-slider .et_pb_slide").each(function() {
                    var content = $(this).text().replace(/\s/g, "");
                    if (content === "") {
                        $(this).css("height", "50vw");

                    }
                });
            }
        });
    };

    Executor.alignScrollTopIcon = function() {
        var height = $(".et_social_networks").height();
        $("span.et_pb_scroll_top.et-pb-icon").css("top", "calc(50% + " + height + "px)");
        $("span.et_pb_scroll_top.et-pb-icon").css("margin-bottom", $(".et_social_networks li").eq(0).css("margin-bottom") + "px)");
    };
    Executor.mobileTab = function() {
        $(document).ready(function() {
            $(".mobile-collapse-tab > ul.et_pb_tabs_controls li").click(function() {
                if ($(window).width() > 767) return;
                if (!$(this).parent().hasClass("active")) {
                    $(this).parent().children().each(function() {
                        $(this).animate({
                            "top": $.map($(this).prevAll(), function(value, index) { return value; }).reduce(function(sum, elem) { return sum + $(elem).height(); }, 0)
                        }, 100);
                    });
                } else {
                    $(this).parent().children().each(function() {
                        $(this).css("top", "");
                    });
                }
                $(this).parent().toggleClass("active");
            });
        });

    };
    Executor.clientexpand = function() {
        $(document).ready(function() {
            var clientid = Global.queryVars.clientid;
            if (clientid) {
                if ($('.detail .item')) {
                    $('.slider-logo .item[item_id=item-' + clientid + ']:first').click();
                }
            }
        });
    };
    // Pleae put all functions above this line
    /**********************************************************************************************/
    // Call all functions inside Executor
    $(function() { for (func in Executor) Executor[func](); });
})(jQuery);


/**
 * Divi theme animate for any elements
 */
(function($) {
    var $animation_elements = $('.et-waypoint'),
        $window = $(window);

    function check_if_in_view() {
        var window_height = $window.height(),
            window_top_position = $window.scrollTop(),
            window_bottom_position = (window_top_position + window_height);

        $animation_elements.each(function() {
            var $element = $(this),
                element_height = $element.outerHeight(),
                element_top_position = $element.offset().top,
                element_bottom_position = (element_top_position + element_height);

            //check to see if this element is within viewport
            if (((element_bottom_position - 30) >= window_top_position) && ((element_top_position + 30) <= window_bottom_position)) {
                $element.addClass('et-animated');
            } else {
                /* Uncomment if we need animate both way */
                // $element.removeClass('et-animated');
            }
        });
    }

    $window.on('scroll resize', check_if_in_view);
    $("body").append("<textarea id=\"clipboard_content\"></textarea>");
    $(document).ready(function() {
        $("img, canvas").contextmenu(function(e) {
            e.preventDefault();
        });
    });
    $("body *:not(#clipboard_content)").on("copy", function(e) {
        //e.preventDefault();
        $("#clipboard_content").html(window.getSelection().toString());
        $("#clipboard_content").select();
        document.execCommand('copy');
    });
    $('#tabs_video_360 .et_pb_all_tabs .et-pb-active-slide').css('display', 'block');
})(jQuery);