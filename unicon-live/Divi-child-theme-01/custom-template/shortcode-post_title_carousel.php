<div class="post_title_carousel marquee" data-duration='10000' data-gap='10' data-pauseOnHover="true">
	<?php
	/**
	 * Featured post
	 */
	$query = new WP_Query( array( "posts_per_page" => -1,
								  "post_status" => "publish",
								  "post_type" => "post",
								  "meta_key" => "_is_featured",
								  "meta_value" => "yes"
								)
						  );
	$featured_id = array();
	while ( $query->have_posts() ): $query->the_post(); $featured_id[] = get_the_ID();
	?>
    <div class="item">
        <a href="<?php the_permalink(); ?>"><span class="title_symbol">►</span> <?php the_title(); ?></a>
    </div>
    <?php endwhile; ?>
    <?php wp_reset_query();
	
	/**
	 * Company News post exclude featured.
	 */
	$cat_id = 0;
	switch ( pll_current_language() ) {
		case 'vi':
			$cat_id = 36;
			break;
		case 'en';
			$cat_id = 38;
			break;
		case 'zh':
			$cat_id = 40;
			break;
		case 'ja':
			$cat_id = 42;
			break;
		};
	$query = new WP_Query( array( "posts_per_page" => 5,
								  "post_status" => "publish",
								  "cat" => $cat_id,
								  "post_type" => "post",
								  "post__not_in" => $featured_id
								)
						  );
	while ( $query->have_posts() ): $query->the_post();
	?>
    <div class="item">
        <a href="<?php the_permalink(); ?>"><span class="title_symbol">►</span> <?php the_title(); ?></a>
    </div>
    <?php endwhile; ?>
    <?php wp_reset_query();
	
	/**
	 * Project news post exclude featured.
	 */
    switch ( pll_current_language() ) {
        case 'vi':
            $cat_id = 66;
            break;
        case 'en';
            $cat_id = 68;
            break;
        case 'zh':
            $cat_id = 70;
            break;
        case 'ja':
            $cat_id = 72;
            break;
        };
    $query = new WP_Query( array( "posts_per_page" => 5,
                                  "post_status" => "publish",
                                  "cat" => $cat_id,
                                  "post_type" => "post",
								  "post__not_in" => $featured_id
                                )
                          );
    ?>
    <?php while ( $query->have_posts() ): $query->the_post(); ?>
    <div class="item">
        <a href="<?php the_permalink(); ?>"><span class="title_symbol">►</span> <?php the_title(); ?></a>
    </div>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</div>