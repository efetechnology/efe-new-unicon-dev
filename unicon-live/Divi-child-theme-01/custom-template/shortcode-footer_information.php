<img id="footer-logo" src="/wp-content/uploads/2017/03/unicons-white.png">
<p style="font-size: 15px;"><strong>
<?php
    echo inline_trans(get_option('title_logo_vn'), get_option('title_logo_en'), get_option('title_logo_zh'), get_option('title_logo_ja'));
?></strong></p>
<div id="footer-information" class="et_pb_row">
    <div class="et_pb_column et_pb_column_1_2">
        <div>
            <strong class="info-title footer-collapse">
            <?php echo inline_trans(get_option('headquarters_vn'), get_option('headquarters_en'), get_option('headquarters_zh'), get_option('headquarters_ja')); ?>
                
            </strong>
            <div>
                <?php echo inline_trans(get_option('contacts_headquarters_vn'), get_option('contacts_headquarters_en'), get_option('contacts_headquarters_zh'), get_option('contacts_headquarters_ja')); ?>
            </div>
        </div>
    </div>
    <div class="et_pb_column et_pb_column_1_2">
        <div>
            <strong class="info-title footer-collapse">
            <?php echo inline_trans(get_option('rep_office_vn'), get_option('rep_office_en'), get_option('rep_office_zh'), get_option('rep_office_ja')); ?> 
            </strong>
            <div>
                <?php echo inline_trans(get_option('contacts_rep_office_vn'), get_option('contacts_rep_office_en'), get_option('contacts_rep_office_zh'), get_option('contacts_rep_office_ja')); ?> 
            </div>
        </div>
    </div>
</div>