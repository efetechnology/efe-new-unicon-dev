<?php

// <div class="et_pb_column et_pb_column_1_3">
//         <a href="#">
//             <div class="image-wrap">
//                 <img src="http://unicons.efe.com.vn/wp-content/uploads/2017/03/Landmark-81-1-300x200.jpg">
//             </div>
//             <p>Landmark</p>
//         </a>
//     </div>

$logos = array(
            array(
                'name' =>'Vingroup 1',
                'logo'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/VinPearl.png',
                'projects' => array(
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv1.png','description'=>'Thiết kế và thi công'),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv2.png','description'=>'Tổng thầu xây dựng '),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv3.png','description'=>'Thi công cơ điện')
                )
            ),
            array(
                'name' =>'Vingroup 2',
                'logo'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/VinPearl.png',
                'projects' => array(
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv1.png','description'=>'Thiết kế và thi công'),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv2.png','description'=>'Tổng thầu xây dựng '),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv3.png','description'=>'Thi công cơ điện')
                )
            ),
            array(
                'name' =>'Vingroup 3',
                'logo'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/VinPearl.png',
                'projects' => array(
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv1.png','description'=>'Thiết kế và thi công'),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv2.png','description'=>'Tổng thầu xây dựng '),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv3.png','description'=>'Thi công cơ điện')
                )
            ),
            array(
                'name' =>'Vingroup 4',
                'logo'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/VinPearl.png',
                'projects' => array(
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv1.png','description'=>'Thiết kế và thi công'),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv2.png','description'=>'Tổng thầu xây dựng '),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv3.png','description'=>'Thi công cơ điện')
                )
            ),
            array(
                'name' =>'Vingroup 5',
                'logo'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/VinPearl.png',
                'projects' => array(
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv1.png','description'=>'Thiết kế và thi công'),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv2.png','description'=>'Tổng thầu xây dựng '),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv3.png','description'=>'Thi công cơ điện')
                )
            ),
             array(
                'name' =>'Vingroup 6',
                'logo'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/VinPearl.png',
                'projects' => array(
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv1.png','description'=>'Thiết kế và thi công'),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv2.png','description'=>'Tổng thầu xây dựng '),
                    array('image'=>'http://unicons.efe.com.vn/wp-content/uploads/2017/03/dv3.png','description'=>'Thi công cơ điện')
                )
            ),
    
);

?>

<div class="client_partner_logo_carousel">
    <?php foreach ( $logos as $logo ) : ?>
    <div class="item">
        <a class="client_partner_logo_popup" href="<?php echo $logo['logo'] ?>">
             <img src="<?php echo $logo['logo']; ?>">
        </a>
        <div class="logo_project_description" >
             <h3>Dự án thi công cho Chủ đầu tư <?php echo $logo['name']; ?></h3>
            <?php foreach ( $logo['projects'] as $project ) : ?>
                <ul class="project_desc_detail">
                    <li><img src="<?php echo $project['image'] ?>"></li>
                    <li><p> <?php echo $project['description'] ?> </p></li>
                </ul>
            <?php endforeach; ?>
        </div>
       
    </div>
    <?php endforeach; ?>
</div>