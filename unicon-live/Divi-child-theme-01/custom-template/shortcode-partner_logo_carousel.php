<article>
	<div>
		<div class="slider-logo">
			<?php
			$args = array(
				'post_type'    => 'partner',
				'posts_per_page' => -1					
				);
			$query = new WP_Query( $args );
			?>
			<?php  if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post(); $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');  ?>
				<div class="item">
					<?php $link = get_field('link_partner');
					if ( !empty($link) && $link != '#' ) {
						echo '<a href="' . $link . '" target="__blank"><img src="'.$featured_img_url.'"></a>';
					}
					else {
						echo '<img src="' . $featured_img_url . '">';
					}
					?>
					
				</div>
			<?php endwhile; endif; ?>				
		</div>
	</div>
</article>	