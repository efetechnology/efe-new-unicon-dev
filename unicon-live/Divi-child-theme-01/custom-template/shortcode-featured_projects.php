<div class="featured_projects">
    <?php
        $args = array(
            'posts_per_page' => -1,
            'post_type' => 'project',
            'post_status' => 'publish',
            'meta_key' => '_is_featured',
            'meta_value' => 'yes'
        );
        $post_features = get_posts($args);
        if ( $post_features ) {
            foreach ( $post_features as $post ) :
                setup_postdata( $post );
                $terms = wp_get_post_terms( $post->ID, 'project_category');
                foreach ($terms as $key => $term) {
                    if($term->parent){
                        $childrent_lug = $term->slug;
                        $parent_id = $term->parent;
                        break;
                    }
                }
                $parent = get_term_by('id', $parent_id, 'project_category');
                $parent_slug = $parent->slug;
                ?>
                    <div>
                        <?php
                            //print_r($parent);
                            echo '<a href="/'.$parent_slug.'/?projects='.$post->ID.'&tab='.$childrent_lug.'">';
                        ?>
                            <div class="image-wrap">
                                <?php the_post_thumbnail( 'small_crop' ); ?>
                            </div>
                            <p><span class="overflow-text-center"><?php the_title(); ?></span></p>
                        </a>
                    </div>
            <?php
            endforeach; 
            wp_reset_postdata();
        }
    ?>
</div>

<script>
    jQuery(document).ready(function(){
        jQuery(".featured_projects").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: true,
            
            autoplay: true,
            
            autoplaySpeed: 3000,
            centerPadding: 0,
            arrows: true,
            dots: false,
            prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;">Next</button>',
            responsive: [
                    {
                      breakpoint: 767,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    },
                  ]
        });
    });
</script>