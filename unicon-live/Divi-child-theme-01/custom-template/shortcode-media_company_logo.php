<?php $logo = get_post(pll_get_post(23743)); ?>
<div class="et_pb_row et_pb_equal_columns media_company_logo media_tab">
    <div class="et_pb_column et_pb_column_1_2 media_item">
        <div class="content-group">
            <div class="featured_image">
                <a href="">
                    <div class="image-wrap et_pb_animation_left et-waypoint">
                        <?php echo get_the_post_thumbnail( $logo, 'medium' ); ?>
                    </div>
                </a>
            </div>
            <div class="item_info">
                <div class="item-title">
                    <h2><?php echo get_post_meta($logo->ID, 'title_custom')[0];?></h2>
                </div>
                <div class="content">
                    <p><?php echo $logo->post_excerpt; ?></p>
                    <label><input type="radio" name="choose_logo_type" class="logo-link-select" value="<?php echo get_option( 'logo_jpeg' );?>" checked>
                    <?php echo get_post_meta($logo->ID, 'JPEG')[0];?></label><br>
                    <label><input type="radio" name="choose_logo_type" class="logo-link-select" value="<?php echo get_option( 'logo_png' );?>">
                    <?php echo get_post_meta($logo->ID, 'PNG')[0];?></label><br>
                    <label><input type="radio" name="choose_logo_type" class="logo-link-select" value="<?php echo get_option( 'logo_psd' );?>">
                    <?php echo get_post_meta($logo->ID, 'PSD')[0];?></label><br>
                    <label><input type="radio" name="choose_logo_type" class="logo-link-select" value="<?php echo get_option( 'logo_ai' );?>">
                    <?php echo get_post_meta($logo->ID, 'AI')[0];?></label><br>
                </div>
            </div>
        </div>
        <div class="button-group">
            <a id="logo-link-button" class="button" href="<?php echo get_option( 'logo_jpeg' );?>" download><?php pll_e('Tải về')?></a>
        </div>
    </div>
<?php $guide = get_post(pll_get_post(23754)); ?>
    <div class="et_pb_column et_pb_column_1_2 media_item">
        <div class="content-group">
            <div class="featured_image">
                <a href="">
                    <div class="image-wrap et_pb_animation_left et-waypoint">
                        <?php echo get_the_post_thumbnail( $guide, 'medium' ); ?>
                    </div>
                </a>
            </div>
            <div class="item_info">
                <div class="item-title">
                    <h2><?php echo get_post_meta($guide->ID, 'title_custom')[0];?></h2>
                </div>
                <div class="content">
                    <p><?php echo $guide->post_excerpt; ?></p>
                    <label><input type="radio" name="choose_instruction" class="choose_instruction_select" value="<?php echo get_option( 'manual_vi' );?>" checked>
                    <?php echo get_post_meta($guide->ID, 'Guild-vn')[0];?></label><br>
                    <label><input type="radio" name="choose_instruction" class="choose_instruction_select" value="<?php echo get_option( 'manual_en' );?>">
                    <?php echo get_post_meta($guide->ID, 'Guild-en')[0];?></label><br>
                    <label><input type="radio" name="choose_instruction" class="choose_instruction_select" value="<?php echo get_option( 'font_roboto' );?>">
                    <?php echo get_post_meta($guide->ID, 'Font-roboto')[0];?></label><br>
                    <label><input type="radio" name="choose_instruction" class="choose_instruction_select" value="<?php echo get_option( 'font_rutan' );?>">
                    <?php echo get_post_meta($guide->ID, 'Font-rutan')[0];?></label><br>
                </div>
            </div>
        </div>
        <div class="button-group">
            <a id="choose_instruction_button" class="button" href="<?php echo get_option( 'manual_vi' );?>"><?php pll_e('Tải về')?></a>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){
        $(".logo-link-select").on("change", function(){
           $("#logo-link-button").attr("href", $(".logo-link-select:checked").val()); 
        });
        $(".choose_instruction_select").on("change", function(){
           $("#choose_instruction_button").attr("href", $(".choose_instruction_select:checked").val()); 
        });
    });
</script>