<div id="why-unicons-people-tab">
	<?php if(!empty(get_field('text_graph'))) : ?>
	<div class="et_pb_row">
		<div class="intro-first">
			<?php echo get_field('text_graph'); ?>
		</div>
	</div>
	<?php endif; ?>
	<?php if(!empty(get_field('list_people'))) : ?>
	<div class="et_pb_row leader">
		<?php foreach(get_field('list_people') as $list): ?>
		<div class="col-2 double-role">
			<div class="avatar"><img class="alignnone" src="<?php echo $list['image']; ?>" /></div>
			<div class="main">
				<div class="title">
					<?php echo $list['full_name']; ?>
				</div>
				<div class="sub-title">
					<?php echo $list['position']; ?>
				</div>
				<div class="content">
					<?php echo $list['introduce']; ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
</div>