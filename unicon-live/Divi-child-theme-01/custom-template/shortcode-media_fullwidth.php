<?php if ( $type == 'video' ) : ?>
<?php
	$video = 'video';
$args = array(
    'posts_per_page' => -1,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => $video,
    'post_status' => 'publish',
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()){
	?>
		<div id="video">
		<div class="media_fullwidth video">
			<div class="media_fullwidth" style="background: #454545;">
				<div class="content_wraper">
					<div class="et_pb_row et_pb_row_2">
						<!-- Column left -->
						<div class="et_pb_column et_pb_column_2_3  et_pb_column_2 big_post">
							<div class="video_wraper et_pb_animation_right et-waypoint">
								<?php echo do_shortcode('[video]'.get_field('video_url', $the_query->posts[0]->ID).'[/video]'); ?>
							</div>
						</div> <!-- .et_pb_column -->
						<!-- Column right -->
						<div class="et_pb_column et_pb_column_1_3  et_pb_column_3 small_post_list et_pb_animation_right et-waypoint">
	<?php

	while ($the_query->have_posts()) : $the_query->the_post();
		?>
			<div class="small_post_item clearfix">
				<div class="small_post_image">
					<img width="300px" src="<?php echo get_the_post_thumbnail_url(); ?>">
				</div>
				<div class="small_post_info">
					<h3><?php echo get_the_title();?></h3>
				</div>
				<div class="video_url hide"><?php echo get_field('video_url'); ?></div>
				<div class="post_excerpt hide"><p><strong><?php echo the_excerpt(); ?></strong></p></div>
				<div class="post_content hide"><?php echo the_content(); ?></div>
			</div>
		<?php
	endwhile;
	wp_reset_query();
	?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="media_fullwidth">
			<div class="content_wraper">
				<div class="et_pb_row et_pb_row_">
					<div class="et_pb_column et_pb_column_3_3  et_pb_column_2" style="margin-right: 0;">
						<div class="big_post_title">
							<h1 class="et_pb_animation_left et-waypoint"><?php echo $the_query->posts[0]->post_title;?></h1>
						</div>
						<div class="big_post_excerpt et_pb_animation_left et-waypoint">
							<p>
								<strong><?php echo $the_query->posts[0]->post_excerpt; ?></strong>
							</p>
							<?php echo $the_query->posts[0]->post_content; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	<?php
}
?>
<?php elseif ( $type == 'image' ) : ?>
<?php
$images = 'images';
$args = array(
    'posts_per_page' => 10,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => $images,
    'post_status' => 'publish',
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()){
	?>
	<div id="images">
		<div class="media_fullwidth image">
			<div class="media_fullwidth" style="background: #454545;">
				<div class="content_wraper">
					<div class="et_pb_row et_pb_row">
						<div class="et_pb_column et_pb_column_2_3  et_pb_column_2 big_post">
							<div class="image_wraper">
								<?php
									while ($the_query->have_posts()) : $the_query->the_post();
										?>
											<div>
												<div class="image-view">
													<?php foreach(get_field('images_slider') as $slider): ?>
														<div>
															<img data-lazy="<?php echo $slider['url']; ?>">
														</div>
													<?php endforeach; ?>
													<?php foreach(get_field('images_slider') as $slider): ?>
														<div>
															<img data-lazy="<?php echo $slider['url']; ?>">
														</div>
													<?php endforeach; ?>
												</div>
												<div class="image-nav">
													<?php foreach(get_field('images_slider') as $slider): ?>
														<div>
															<img data-lazy="<?php echo wp_get_attachment_image_src($slider['id'], 'medium')[0]; ?>">
														</div>
													<?php endforeach; ?>
												</div>
											</div>
										<?php
									endwhile;
								?>
							</div>
						</div>
						<div class="et_pb_column et_pb_column_1_3  et_pb_column_3 small_post_list et_pb_animation_right et-waypoint">
						<div class="nav_parent">
	<?php
	while ($the_query->have_posts()) : $the_query->the_post();
		?>
			<div class="small_post_item clearfix">
				<div class="small_post_image">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>">
				</div>
				<div class="small_post_info">
					<h3><?php echo get_the_title();?></h3>
					<p><?php echo get_field('excerpt_image'); ?></p>
				</div>
				<div class="post_excerpt hide"><p><?php echo the_excerpt(); ?></p></div>
				<div class="post_content hide"><?php echo the_content(); ?></div>
			</div>
		<?php
	endwhile;
	wp_reset_query();
	?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="media_fullwidth">
			<div class="content_wraper">
				<div class="et_pb_row et_pb_row_">
					<div class="et_pb_column et_pb_column_3_3  et_pb_column_2" style="margin-right: 0;">
						<div class="big_post_title">
							<h1 class="et_pb_animation_left et-waypoint"><?php echo $the_query->posts[0]->post_title;?></h1>
						</div>
						<div class="big_post_excerpt et_pb_animation_left et-waypoint">
							<p>
								<strong><?php echo $the_query->posts[0]->post_excerpt; ?></strong>
							</p>
							<?php echo $the_query->posts[0]->post_content; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	<?php
}
?>
<?php elseif ( $type == 'image_360') : ?>
	<?php
	$image_360 = 'image360';
	$post_list = get_posts( array( 'post_type' => $image_360, 'posts_per_page' => 10));
	$first_url = wp_get_attachment_image_src( get_field('select_image_360', $post_list[0]->ID ), 'full')[0];
	?>
	<div class="media_fullwidth image_360">
		<script class="remove">
			jQuery(document).ready(function($){
				var parent = $(".media_fullwidth.image_360").parents(".et_pb_tab").eq(0);
				parent.css("display", "block");
				var panorama, viewer;
				panorama = new PANOLENS.ImagePanorama( '<?php echo $first_url?>' );
				viewer = new PANOLENS.Viewer( { output: 'console', container: $(".media_fullwidth.image_360")[0] } );
				viewer.add( panorama );
				parent.css("display", "none");
				var parent_360 = $("#tabs_video_360  .media_fullwidth.image_360").parents(".et_pb_tab").eq(0);
				parent_360.css("display", "block");
				jQuery("script.remove").remove();
			});
		</script>
	</div>
	<div class="media_fullwidth image_360_info">
		<div class="content_wraper">
			<div class="et_pb_row et_pb_row_2">
				<div class="et_pb_column et_pb_column_2_3  et_pb_column_2">
					<div class="big_post_title">
						<h1 class="et_pb_animation_left et-waypoint"><?php echo $post_list[0]->post_title ?></h1>
					</div>
					<div class="big_post_excerpt et_pb_animation_left et-waypoint">
						<p>
							<strong><?php echo $post_list[0]->post_excerpt; ?></strong>
						</p>
					</div>
					<div class="big_post_content et_pb_animation_left et-waypoint">
						<?php echo wpautop($post_list[0]->post_content); ?>
					</div>
				</div> <!-- .et_pb_column -->
				<a style="display: none;" href="/thu-vien/" class="view_all_1 xs-bottom-view-all"><?php echo inline_trans("Xem tất cả", "View all", "查看全部", "すべて見る") ?></a>
				<div class="et_pb_column et_pb_column_1_3  et_pb_column_3 small_post_list et_pb_animation_right et-waypoint" >
					<?php foreach($post_list as $image_post) : ?>
						<div class="small_post_item clearfix" data-image-360="<?php echo $image_post->ID; ?>">
							<div class="small_post_image">
								<?php if ( has_post_thumbnail( $image_post->ID ) ) { ?>
									<img src="<?php echo get_the_post_thumbnail_url( $image_post->ID, 'small_crop' ) ?>">
								<?php } else { ?>
									<img src="<?php echo wp_get_attachment_image_src( get_field('select_image_360', $image_post->ID ), 'small_crop')[0]; ?>">
								<?php } ?>
							</div>
							<div class="small_post_info">
									<h3><?php echo $image_post->post_title; ?></h3>
								<p><?php echo $image_post->post_excerpt; ?></p>
							</div>
						</div>
					<?php endforeach;; ?>
				</div> <!-- .et_pb_column -->
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if ( $type == 'video_home' ) : ?>
<?php
	$video_home = 'video';
$args = array(
    'posts_per_page' => -1,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => $video_home,
    'post_status' => 'publish',
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()){
	?>
		<div id="video">
		<div class="media_fullwidth video">
			<div class="media_fullwidth light-gray-bg">
				<div class="content_wraper">
					<div class="et_pb_row et_pb_row_12">
						<div class="et_pb_column et_pb_column_4_4  et_pb_column_2 big_post">
							<div class="video_wraper et_pb_animation_right et-waypoint">
								<?php echo do_shortcode('[video]'.get_field('video_url', $the_query->posts[0]->ID).'[/video]'); ?>
							</div>
						</div> <!-- .et_pb_column -->
					</div>
				</div>
			</div>
		</div>
		<div class="media_fullwidth">
			<div class="content_wraper">
				<div class="et_pb_row et_pb_row_2">
					<div class="et_pb_column et_pb_column_2_3  et_pb_column_2 et_pb_column_2" style="margin-right: 0;">
						<div class="big_post_title">
							<h1 class="et_pb_animation_left et-waypoint"><?php echo $the_query->posts[0]->post_title;?></h1>
						</div>
						<div class="big_post_excerpt et_pb_animation_left et-waypoint">
							<p>
								<strong><?php echo $the_query->posts[0]->post_excerpt; ?></strong>
							</p>
							<?php echo $the_query->posts[0]->post_content; ?>
						</div>
					</div>
					<a style="margin-right: 40px;" href="/video/" class="view_all_1 xs-bottom-view-all">
					<?php echo inline_trans("Xem tất cả", "View all", "查看全部", "すべて見る") ?></a>
					<!-- Column right -->
					<div class="et_pb_column et_pb_column_1_3  et_pb_column_3 small_post_list et_pb_animation_right et-waypoint">
							<?php
							while ($the_query->have_posts()) : $the_query->the_post();
								?>
									<div class="small_post_item clearfix">
										<div class="small_post_image">
											<img width="300px" src="<?php echo get_the_post_thumbnail_url(); ?>">
										</div>
										<div class="small_post_info">
											<h3><?php echo get_the_title();?></h3>
										</div>
										<div class="video_url hide"><?php echo get_field('video_url'); ?></div>
										<div class="post_excerpt hide"><p><strong><?php echo the_excerpt(); ?></strong></p></div>
										<div class="post_content hide"><?php echo the_content(); ?></div>
									</div>
								<?php
							endwhile;
							wp_reset_query();
							?>
						</div>
				</div>
			</div>
		</div>
		</div>
	<?php
}
endif;
?>
