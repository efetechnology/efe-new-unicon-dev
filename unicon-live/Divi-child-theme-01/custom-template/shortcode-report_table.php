<?php
if ( !empty( $report_list = get_field('report_list' ) ) ) {
	?>
<table>
	<tbody>
		<tr>
			<td height="30"><strong><?php echo inline_trans('Năm', 'Year', '年', '年'); ?></strong></td>
			<td><strong><?php echo inline_trans('Đơn vị tính', 'Unit', '单元', '単位'); ?></strong></td>
			<td><strong><?php echo inline_trans('Doanh thu', 'Revenue', '収入', '収入'); ?></strong></td>
			<td><strong><?php echo inline_trans('Lợi nhuận', 'Profit', '利润', '利益'); ?></strong></td>
		</tr>
		<?php
				foreach( $report_list as $report) {
					?>
		<tr>
			<td>
				<?php echo $report['year']; ?>
			</td>
			<td>
				<?php echo $report['unit']; ?>
			</td>
			<td>
				<?php echo $report['revenue']; ?>
			</td>
			<td>
				<?php echo $report['profit']; ?>
			</td>
		</tr>
		<?php
				}
				?>
	</tbody>
</table>
<?php
}