<?php
$tax = get_query_var( 'taxonomy' );
$terms = get_terms( $tax, array(
				'orderby'    => 'count',
				'hide_empty' => 0,				
			) );
?>
<article>
	<div style="padding-bottom: 0;">
		<div class="slider-logo">							
			<?php $i = 1; foreach($terms as $detail) :?>
			<div item_id="item-<?php echo $detail->term_id; ?>" class="item">
				<?php $meta_image = get_wp_term_image($detail->term_id); ?>
				<img src="<?php echo $meta_image; ?>">
			</div>
			<?php $i++; endforeach; ?>								
		</div>
	</div>
	<div class="detail">
		<?php $j = 1; foreach($terms as $detail) :?>
			<?php
				$args_tax = array(											
					'post_type' => 'project',
					'posts_per_page' => 3,         
					'tax_query' => array(															
						array(
							'taxonomy' => $tax,  
							'field' => 'term_id',          
							'terms' => $detail->term_id,         
						)
					)
				);
			?>
			<div item_id="item-<?php echo $detail->term_id; ?>" class="item">	
				<div class="arrow"></div>							
				<div class="project-detail et_pb_row">
					<h3>Dự án <?php echo $detail->name; ?></h3>
					<div id="wrap-column">	
						<?php $query = new WP_Query( $args_tax );
						if($query->have_posts()):
						while ( $query->have_posts() ) : $query->the_post(); $featured_img_url = get_the_post_thumbnail_url($post->ID, 'small_crop'); ?>
							<div class="column-3" id="column">
								<div class="content">
									<img src="<?php echo $featured_img_url; ?>">
									<p style="text-align: center; font-size: 20px; font-weight: bold; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><?php the_title(); ?></p>
								</div>												
							</div>						
						<?php endwhile;  endif; ?>
					</div>																			
				</div>
			</div>
		<?php $j++; endforeach; ?>		
	</div>
</article>	<!-- End KHÁCH SẠN - KHU NGHỈ DƯỠNG -->