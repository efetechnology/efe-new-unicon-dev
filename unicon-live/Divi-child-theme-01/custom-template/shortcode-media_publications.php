<div class="et_pb_row et_pb_equal_columns media_publications media_tab">
	<?php $brochure = get_post(pll_get_post(19702)); ?>
    <div class="et_pb_column et_pb_column_1_2 media_item">
        <div class="et_pb_row et_pb_equal_columns">
        	<div class="et_pb_column et_pb_column_1_2 featured_image">
        		<a href="<?php echo get_permalink( $brochure ); ?>">
                    <div class="image-wrap et_pb_animation_left et-waypoint">
                        <?php echo get_the_post_thumbnail( $brochure, 'medium' ); ?>
                    </div>
                </a>
        	</div>
        	<div class="et_pb_column et_pb_column_1_2 item_info">
				<div class="content-group">
					<div class="item-title">
						<h2><?php echo get_post_meta($brochure->ID, 'title_custom')[0];?></h2>
					</div>
					<p><?php echo $brochure->post_excerpt; ?></p>
				</div>
        		<div class="button-group">
                    <a class="button" href="<?php echo get_permalink( $brochure ); ?>"><?php pll_e('Xem')?></a>

					<?php

                    if (get_field( 'link_download', $brochure->ID ) != '' ) {
                        $download_link = get_field( 'link_download', $brochure->ID );
                        ?>
            		<a class="button" href="<?php echo $download_link; ?>" download><?php pll_e('Tải về')?></a>
					<?php } ?>
                </div>
        	</div>
        </div>
    </div>
	<?php $profile = get_post(pll_get_post(19718)); ?>
    <div class="et_pb_column et_pb_column_1_2 media_item">
        <div class="et_pb_row et_pb_equal_columns">
        	<div class="et_pb_column et_pb_column_1_2 featured_image">
                <a href="<?php echo get_permalink( $profile ); ?>">
                    <div class="image-wrap et_pb_animation_left et-waypoint">
                        <?php echo get_the_post_thumbnail( $profile, 'medium' ); ?>
                    </div>
                </a>
        	</div>
        	<div class="et_pb_column et_pb_column_1_2 item_info">
				<div class="content-group">
					<div class="item-title">
						<h2><?php echo get_post_meta($profile->ID, 'title_custom')[0];?></h2>
					</div>
					<p><?php echo $profile->post_excerpt; ?></p>
				</div>
        		<div class="button-group">
                    <a class="button" href="<?php echo get_permalink( $profile ); ?>"><?php pll_e('Xem'); ?></a>
            		<a class="button" href="<?php echo get_field( 'link_download', $profile->ID );?>" download><?php pll_e('Tải về'); ?></a>
                </div>
        	</div>
        </div>
    </div>
</div>
