<?php $stock = get_option('stock_info');?>
<table id="stock-info">
    <tr>
        <td>CTD</td>
        <td>
            <p>
                <span id="stock-deviation"><?php echo $stock['deviate']?></span><br>
                <span id="stock-date"><?php echo $stock['date']?></span>
            </p>
        </td>
        <td>
            <span id="stock-num"><?php echo $stock['figure']?></span>
        </td>
    </tr>
</table>
<div id="social-info">
    <p><strong><?php echo inline_trans(get_option('title_social_vn'), get_option('title_social_en'), get_option('title_social_zh'), get_option('title_social_js')); ?> </strong></p>
    <a href="<?php the_option('unicons_facebook'); ?>" class="social-icon facebook" target="_blank">
        <i class="fa fa-facebook" aria-hidden="true"></i>
    </a>
    <a href="<?php the_option('unicons_linkedin'); ?>" class="social-icon linkedin" target="_blank">
        <i class="fa fa-linkedin" aria-hidden="true"></i>
    </a>
    <a href="<?php the_option('unicons_youtube'); ?>" class="social-icon youtube" target="_blank">
        <i class="fa fa-youtube" aria-hidden="true"></i>
    </a>
    <a href="<?php the_option('unicons_google_plus'); ?>" class="social-icon google-plus" target="_blank">
        <i class="fa fa-google-plus" aria-hidden="true"></i>
    </a>
    <a href="<?php the_option('unicons_portal'); ?>" class="social-icon google-plus" target="_blank">
        <img src="/wp-content/uploads/2017/07/portal-icon.jpg">
    </a>
</div>
<p><strong><?php echo inline_trans(get_option('members_vn'), get_option('members_en'), get_option('members_zh'), get_option('members_ja')); ?></strong></p>
<p id="group-logos">
    <a href="http://www.coteccons.vn/" target="_blank"><img class="footer-small-logo" src="/wp-content/uploads/2017/03/Coteccons-white-300x127.png"></a>
    <a href="http://www.unicons.vn" target="_blank"><img class="footer-small-logo" src="/wp-content/uploads/2017/03/unicons-white-300x86.png"></a>
    <a href="http://www.ricons.vn/" target="_blank"><img class="footer-small-logo" src="/wp-content/uploads/2017/03/Ricons-white-300x118.png"></a>
</p>