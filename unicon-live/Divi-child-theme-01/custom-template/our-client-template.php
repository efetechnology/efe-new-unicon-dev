<?php
/*
* Template name: Out clients and partners
*/
get_header();
?>

<div id="main-content" class="out-clients-wrap">
	<?php 
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			the_content();
			$tax_dact = get_terms( 'du_an_cao_tang', array(
				'orderby'    => 'count',
				'hide_empty' => 0,				
			) );
			$tax_dacn = get_terms( 'du_an_cong_nghiep', array(
				'orderby'    => 'count',
				'hide_empty' => 0,				
			) );
			$tax_ksnn = get_terms( 'khach_san_nghi_duong', array(
				'orderby'    => 'count',
				'hide_empty' => 0,				
			) );
			$tax_gd = get_terms( 'giao_duc', array(
				'orderby'    => 'count',
				'hide_empty' => 0,
			) );
		?>	
		<article>
			<div class="et_pb_row" style="padding-bottom: 0;">
				<div class="client_logos_title">DỰ ÁN CAO TẦNG - THƯƠNG MẠI</div>
				<div class="slider-logo">							
					<?php $i = 1; foreach($tax_dact as $detail) :?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">
						<?php $meta_image = get_wp_term_image($detail->term_id); ?>
						<img src="<?php echo $meta_image; ?>">
					</div>
					<?php $i++; endforeach; ?>								
				</div>
			</div>
			<div class="detail">
				<?php $j = 1; foreach($tax_dact as $detail) :?>
					<?php
						$args_tax = array(											
							'post_type' => 'project',   
							'posts_per_page' => 3,      
							'tax_query' => array(															
								array(
									'taxonomy' => 'du_an_cao_tang',  
									'field' => 'term_id',          
									'terms' => $detail->term_id,         
								)
							)
						);
					?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">	
						<div class="arrow"></div>							
						<div class="project-detail et_pb_row">
							<h3>Dự án <?php echo $detail->name; ?></h3>
							<div id="wrap-column">	
								<?php $query = new WP_Query( $args_tax );
								if($query->have_posts()):
								while ( $query->have_posts() ) : $query->the_post(); $featured_img_url = get_the_post_thumbnail_url($post->ID, 'small_crop'); ?>																	
									<div class="column-3" id="column">
										<div class="content">
											<img src="<?php echo $featured_img_url; ?>">
											<p style="text-align: center; font-size: 20px; font-weight: bold; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><?php the_title(); ?></p>
										</div>												
									</div>						
								<?php endwhile;  endif; ?>
							</div>																			
						</div>
					</div>
				<?php $j++; endforeach; ?>		
			</div>
		</article>	<!-- End DỰ ÁN CAO TẦNG - THƯƠNG MẠI -->

		<article>
			<div class="et_pb_row" style="padding-bottom: 0;">
				<div class="client_logos_title">DỰ ÁN CÔNG NGHIỆP</div>
				<div class="slider-logo">							
					<?php $i = 1; foreach($tax_dacn as $detail) :?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">
						<?php $meta_image = get_wp_term_image($detail->term_id); ?>
						<img src="<?php echo $meta_image; ?>">
					</div>
					<?php $i++; endforeach; ?>								
				</div>
			</div>
			<div class="detail">
				<?php $j = 1; foreach($tax_dacn as $detail) :?>
					<?php
						$args_tax = array(											
							'post_type' => 'project', 
							'posts_per_page' => 3,        
							'tax_query' => array(															
								array(
									'taxonomy' => 'du_an_cong_nghiep',  
									'field' => 'term_id',          
									'terms' => $detail->term_id,         
								)
							)
						);
					?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">	
						<div class="arrow"></div>							
						<div class="project-detail et_pb_row">
							<h3>Dự án <?php echo $detail->name; ?></h3>
							<div id="wrap-column">	
								<?php $query = new WP_Query( $args_tax );
								if($query->have_posts()):
								while ( $query->have_posts() ) : $query->the_post(); $featured_img_url = get_the_post_thumbnail_url($post->ID, 'small_crop'); ?>
									<div class="column-3" id="column">
										<div class="content">
											<img src="<?php echo $featured_img_url; ?>">
											<p style="text-align: center; font-size: 20px; font-weight: bold; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><?php the_title(); ?></p>
										</div>												
									</div>						
								<?php endwhile;  endif; ?>
							</div>																			
						</div>
					</div>
				<?php $j++; endforeach; ?>		
			</div>
		</article>	<!-- End DỰ ÁN CÔNG NGHIỆP -->

		<article>
			<div class="et_pb_row" style="padding-bottom: 0;">
				<div class="client_logos_title">KHÁCH SẠN - KHU NGHỈ DƯỠNG</div>
				<div class="slider-logo">							
					<?php $i = 1; foreach($tax_ksnn as $detail) :?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">
						<?php $meta_image = get_wp_term_image($detail->term_id); ?>
						<img src="<?php echo $meta_image; ?>">
					</div>
					<?php $i++; endforeach; ?>								
				</div>
			</div>
			<div class="detail">
				<?php $j = 1; foreach($tax_ksnn as $detail) :?>
					<?php
						$args_tax = array(											
							'post_type' => 'project',
							'posts_per_page' => 3,         
							'tax_query' => array(															
								array(
									'taxonomy' => 'khach_san_nghi_duong',  
									'field' => 'term_id',          
									'terms' => $detail->term_id,         
								)
							)
						);
					?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">	
						<div class="arrow"></div>							
						<div class="project-detail et_pb_row">
							<h3>Dự án <?php echo $detail->name; ?></h3>
							<div id="wrap-column">	
								<?php $query = new WP_Query( $args_tax );
								if($query->have_posts()):
								while ( $query->have_posts() ) : $query->the_post(); $featured_img_url = get_the_post_thumbnail_url($post->ID, 'small_crop'); ?>
									<div class="column-3" id="column">
										<div class="content">
											<img src="<?php echo $featured_img_url; ?>">
											<p style="text-align: center; font-size: 20px; font-weight: bold; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><?php the_title(); ?></p>
										</div>												
									</div>						
								<?php endwhile;  endif; ?>
							</div>																			
						</div>
					</div>
				<?php $j++; endforeach; ?>		
			</div>
		</article>	<!-- End KHÁCH SẠN - KHU NGHỈ DƯỠNG -->

		<article>
			<div class="et_pb_row" style="padding-bottom: 0;">
				<div class="client_logos_title">GIÁO DỤC</div>
				<div class="slider-logo">							
					<?php $i = 1; foreach($tax_gd as $detail) :?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">
						<?php $meta_image = get_wp_term_image($detail->term_id); ?>
						<img src="<?php echo $meta_image; ?>">
					</div>
					<?php $i++; endforeach; ?>								
				</div>
			</div>
			<div class="detail">
				<?php $j = 1; foreach($tax_gd as $detail) :?>
					<?php
						$args_tax = array(											
							'post_type' => 'project',    
							'posts_per_page' => 3,
							'tax_query' => array(															
								array(
									'taxonomy' => 'giao_duc',  
									'field' => 'term_id',          
									'terms' => $detail->term_id,         
								)
							)
						);
					?>
					<div item_id="item-<?php echo $detail->term_id; ?>" class="item">	
						<div class="arrow"></div>							
						<div class="project-detail et_pb_row">
							<h3>Dự án <?php echo $detail->name; ?></h3>
							<div id="wrap-column">	
								<?php $query = new WP_Query( $args_tax );
								if($query->have_posts()):
								while ( $query->have_posts() ) : $query->the_post(); $featured_img_url = get_the_post_thumbnail_url($post->ID, 'small_crop'); ?>
									<div class="column-3" id="column">
										<div class="content">
											<img src="<?php echo $featured_img_url; ?>">
											<p style="text-align: center; font-size: 20px; font-weight: bold; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><?php the_title(); ?></p>
										</div>												
									</div>						
								<?php endwhile;  endif; ?>
							</div>																			
						</div>
					</div>
				<?php $j++; endforeach; ?>		
			</div>
		</article>	<!-- End  GIÁO DỤC -->

	<?php }	} ?> 

<article>
	<div class="et_pb_row">		
		<div class="client_logos_title">ĐỐI TÁC CỦA CHÚNG TÔI</div>
		<div class="slider-logo">
			<?php
			$args = array(
				'post_type'    => 'partner',
				'posts_per_page' => -1					
				);
			$query = new WP_Query( $args );
			?>
			<?php  if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post(); $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');  ?>
				<div class="item">
					<?php $link = get_field('link_partner');
					if ( !empty($link) && $link != '#' ) {
						echo '<a href="' . $link . '" target="__blank"><img src="'.$featured_img_url.'"></a>';
					}
					else {
						echo '<img src="' . $featured_img_url . '">';
					}
					?>
					
				</div>
			<?php endwhile; endif; ?>				
		</div>
	</div>
</article>	
</div>

<?php get_footer(); ?>