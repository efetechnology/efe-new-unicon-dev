<?php
/*
* Template Name: Projects
*/
get_header();
?>
<div id="main-content" class="project-wrap">
	<?php 
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			the_content();
		}
	}
	?>
		<div id="form-search">
			<div class="et_pb_row">
			<?php
				$taxonomy = 'project_category';
				$year = get_term_by('slug', 'du-an-theo-nam', $taxonomy);
				$year_id = $year->term_id;
				$term_children_year = get_term_children( $year_id, $taxonomy );

				$area = get_term_by('slug', 'du-an-theo-vung-mien', $taxonomy);
				$area_id = $area->term_id;
				$term_children_area = get_term_children( $area_id, $taxonomy );
			?>
				<form action="/result/" method="get">
					<span><?php echo inline_trans('Sắp xếp theo','Search by', '搜索', 'で検索'); ?>:</span>
					<select name="y" onchange="this.form.submit()">
					<option value=""><?php echo inline_trans('Năm', 'Year', '年', '年'); ?></option>	
					<?php foreach($term_children_year as $child) : $term = get_term_by( 'id', $child, $taxonomy ); ?>						
						<option value="<?php echo $term->slug; ?>"><?php pll_e($term->name); ?></option>	
					<?php endforeach; ?>
				</select>
					<select name="a" onchange="this.form.submit()">
					<option value=""><?php echo inline_trans('Vùng miền', 'Region', '地区', '領域'); ?></option>
					<?php foreach($term_children_area as $child) : $term = get_term_by( 'id', $child, $taxonomy ); ?>						
						<option value="<?php echo $term->slug; ?>"><?php pll_e( $term->name ); ?></option>	
					<?php endforeach; ?>
				</select>
					<input type="text" name="k" placeholder="<?php echo inline_trans('Tìm kiếm', 'Keywords', '关键字', 'キーワード'); ?>" />
				</form>
			</div>
		</div>
		<!-- End form search -->
		<div id="wrap-page">
			<?php
				if( in_array(get_the_ID(), array(10157, 19918, 19919, 19920) ) ){
					$id_cate = 44; // Dự án đang thực hiện
				}else if( in_array( get_the_ID(), array(11337, 19910, 19911, 19912) ) ){
					$id_cate = 49; // Dự án đã thực hiện
				}else if( in_array( get_the_ID(), array(11339, 19896, 19897, 19898) ) ){
					$id_cate = 54; // Dự án theo năm
				}else{
					$id_cate = 62; // Dự án theo vùng miền
				}
			?>
			<div id="tab">
				<?php
					$categories = get_terms( 'project_category', array(
						'orderby'    => 'term_id',
						'order' => 'ASC',
						'hide_empty' => 0,
						'parent' => $id_cate
					));
				?>
				<div class="et_pb_row">
					<?php if(!empty($categories)) : ?>
					<ul>
						<?php foreach($categories as $cate) : ?>
						<li slug="<?php echo $cate->slug ?>">
							<?php pll_e( $cate->name );  ?>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
			<?php if(!empty($categories)) : ?>
			<div class="content-tab">
				<div class="et_pb_row">
					<div class="wrap">
						<?php foreach($categories as $cate) : ?>
						<?php
							echo '<div id="'.$cate->term_id.'" class="contents">';
							
							
							
							if ( isset($_GET['tab']) && $_GET['tab'] == $cate->slug ) {
								$featured_query = new WP_Query( array( 'post_type' => 'project', 'p' => $_GET['projects'] ) );
								while ( $featured_query->have_posts() ) : $featured_query->the_post(); ?>
								<div class="column-3 item <?php echo 'project_'.$post->ID; ?>">
									<div class="content">
										<img src="<?php if ( has_post_thumbnail() ) echo get_the_post_thumbnail_url($post->ID,'small_crop'); else echo wp_get_attachment_image_src(21872, 'small_crop')[0] ?>">
										<h4>
											<?php the_title(); ?>
										</h4>
										<div class="arrow"></div>
									</div>
									<div class="group-detail" style="background-image: url(<?php the_field('bg_description'); ?>)">
										<div class="et_pb_row">
											<h3 class="title-project">
												<?php the_title(); ?>
											</h3>
											<span class="close"><?php echo inline_trans('Đóng', 'Close', '关', '閉じる'); ?></span>
											<div class="tab-detail">
												<ul>
													<li id="tab-dt-01" class="active"><?php echo inline_trans('Thông tin dự án', 'Project information', '项目信息', 'プロジェクト情報');?> </li>
													<li id="tab-dt-02" class="tab-project-image"><?php echo inline_trans('Hình ảnh dự án', 'Project images', '项目图像', 'プロジェクト画像'); ?></li>
													<li id="tab-dt-03" class="tab-image-360"><?php echo inline_trans('Ảnh 360<sup>o</sup>', '360<sup>o</sup> image', '360<sup>o</sup>形象', '360<sup>o</sup>画像'); ?></li>
													<div class="clear"></div>
												</ul>
											</div>
											<div class="tab-detail-content">
												<div id="tab-dt-01">
													<div class="column-3">
														<?php if(!empty(get_field('location'))) :?>
														<div class="group et_pb_animation_left et-waypoint et-animated">
															<div class="row-title"><?php echo inline_trans('Địa điểm', 'Location', '位置', 'ロケーション'); ?></div>
															<div class="value-info">
																<?php the_field('location'); ?>
															</div>
														</div>
														<?php endif; ?>
														<?php if(!empty(get_field('customer'))) :?>
														<div class="group et_pb_animation_left et-waypoint et-animated">
															<div class="row-title"><?php echo inline_trans('Khách hàng', 'Client', '客户', 'クライアント'); ?></div>
															<div class="value-info">
																<?php the_field('customer'); ?>
															</div>
														</div>
														<?php endif; ?>
														<?php if(!empty(get_field('general_contractors'))) :?>
														<div class="group et_pb_animation_left et-waypoint et-animated">
															<div class="row-title"><?php echo inline_trans('Tổng thầu', 'General contractor', '总承包人', 'ゼネコン'); ?></div>
															<div class="value-info">
																<?php the_field('general_contractors'); ?>
															</div>
														</div>
														<?php endif; ?>
														<?php if(!empty(get_field('construction_item'))) :?>
														<div class="group et_pb_animation_left et-waypoint et-animated">
															<div class="row-title"><?php echo inline_trans('Hạng mục thi công', 'Construction items', '建设项目', '建設項目'); ?></div>
															<div class="value-info">
																<?php the_field('construction_item'); ?>
															</div>
														</div>
														<?php endif; ?>
														<?php if(!empty(get_field('contract_value'))) :?>
														<div class="group et_pb_animation_left et-waypoint et-animated">
															<div class="row-title"><?php echo inline_trans('Giá trị hợp đồng', 'Contract value', '合同价值', '契約価値'); ?></div>
															<div class="value-info">
																<?php the_field('contract_value'); ?>
															</div>
														</div>
														<?php endif; ?>
														<?php if(!empty(get_field('construction_time'))) :?>
														<div class="group et_pb_animation_left et-waypoint et-animated">
															<div class="row-title"><?php echo inline_trans('Thời gian thi công', 'Construction time', '施工时间', '建設時間'); ?></div>
															<div class="value-info">
																<?php the_field('construction_time'); ?>
															</div>
														</div>
														<?php endif; ?>
														<?php if(!empty(get_field('content'))) :?>
														<div class="content et_pb_animation_left et-waypoint et-animated">
															<?php the_field('content'); ?>
														</div>
														<?php endif; ?>
													</div>
													<div class="column-9">
														<a href="<?php echo (get_field('bg_description')?get_field('bg_description'):( has_post_thumbnail() ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : wp_get_attachment_image_src(21872, 'full')[0])); ?>" class="et_pb_lightbox_image">
															<img src="<?php echo (get_field('bg_description')?get_field('bg_description'):( has_post_thumbnail() ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : wp_get_attachment_image_src(21872, 'full')[0])); ?>" class="et_pb_animation_right et-waypoint et-animated">
														</a>
													</div>
												</div>
												<div id="tab-dt-02">
													<?php if(!empty(get_field('image_project'))) { ?>
													<div id="image-project">
														<?php foreach(get_field('image_project') as $item) : ?>
														<div class="item-image">
															<a href="<?php echo wp_get_attachment_image_src( $item['ID'], 'full' )[0]; ?>" class="et_pb_lightbox_image">
																			<img src="<?php echo wp_get_attachment_image_src( $item['ID'], 'small_crop' )[0]; ?>" />	
																		</a>
														</div>
														<?php endforeach; ?>
													</div>
													<?php }else{ ?>
													<div style="padding-top: 15px;">
														<h3 style="color: #fff;font-size: 17px;"><?php echo inline_trans('Chưa cập nhật nội dung...', 'Contents have not updated yet...', '内容尚未更新...', 'コンテンツはまだ更新されていません...'); ?></h3>
													</div>
													<?php } ?>
												</div>
												<div id="tab-dt-03">
												<?php 
													$image_post = get_posts( array( 'connected_type' => 'project_2_image360',
																				    'connected_items' => get_the_ID(),
																					'post_type' => 'image360'
																				   )) ;
													if ( !empty($image_post) ) {
														echo "<div class=\"image-360-container\" data-id=\"".get_the_ID()."\"></div>";
													} else {
														echo inline_trans('Đang cập nhật......', 'Updating......', '更新......', '更新中......');
													}
												?>
												</div>
											</div>
										</div>
										<!-- End row -->
									</div>
								</div>
								<?php endwhile; 
							};
							
							
							
							$args_array = array(											
								'post_type' => 'project',
								'posts_per_page' => 27,
								'post__not_in' => array($_GET['projects']),
								'tax_query' => array(															
									array(
										'taxonomy' => 'project_category',  
										'field' => 'term_id',          
										'terms' => $cate->term_id,         
									)
								)
							);							
							$query = new WP_Query( $args_array );
							if($query->have_posts()):
							$i = 1;
							while ( $query->have_posts() ) : $query->the_post(); ?>
							<div class="column-3 item <?php echo 'project_'.$post->ID; ?>">
								<div class="content">
									<img src="<?php if ( has_post_thumbnail() ) echo get_the_post_thumbnail_url($post->ID,'small_crop'); else echo wp_get_attachment_image_src(21872, 'small_crop')[0] ?>">
									<h4>
										<?php the_title(); ?>
									</h4>
									<div class="arrow"></div>
								</div>
								<div class="group-detail" style="background-image: url(<?php the_field('bg_description'); ?>)">
									<div class="et_pb_row">
										<h3 class="title-project">
											<?php the_title(); ?>
										</h3>
										<span class="close"><?php echo inline_trans('Đóng', 'Close', '关', '閉じる'); ?></span>
										<div class="tab-detail">
											<ul>
												<li id="tab-dt-01" class="active"><?php echo inline_trans('Thông tin dự án', 'Project information', '项目信息', 'プロジェクト情報');?> </li>
												<li id="tab-dt-02" class="tab-project-image"><?php echo inline_trans('Hình ảnh dự án', 'Project images', '项目图像', 'プロジェクト画像'); ?></li>
												<li id="tab-dt-03" class="tab-image-360"><?php echo inline_trans('Ảnh 360<sup>o</sup>', '360<sup>o</sup> image', '360<sup>o</sup>形象', '360<sup>o</sup>画像'); ?></li>
												<div class="clear"></div>
											</ul>
										</div>
										<div class="tab-detail-content">
											<div id="tab-dt-01">
												<div class="column-3">
													<?php if(!empty(get_field('location'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title"><?php echo inline_trans('Địa điểm', 'Location', '位置', 'ロケーション'); ?></div>
														<div class="value-info">
															<?php the_field('location'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('customer'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title"><?php echo inline_trans('Khách hàng', 'Client', '客户', 'クライアント'); ?></div>
														<div class="value-info">
															<?php the_field('customer'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('general_contractors'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title"><?php echo inline_trans('Tổng thầu', 'General contractor', '总承包人', 'ゼネコン'); ?></div>
														<div class="value-info">
															<?php the_field('general_contractors'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('construction_item'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title"><?php echo inline_trans('Hạng mục thi công', 'Construction items', '建设项目', '建設項目'); ?></div>
														<div class="value-info">
															<?php the_field('construction_item'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('contract_value'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title"><?php echo inline_trans('Giá trị hợp đồng', 'Contract value', '合同价值', '契約価値'); ?></div>
														<div class="value-info">
															<?php the_field('contract_value'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('construction_time'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title"><?php echo inline_trans('Thời gian thi công', 'Construction time', '施工时间', '建設時間'); ?></div>
														<div class="value-info">
															<?php the_field('construction_time'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('content'))) :?>
													<div class="content et_pb_animation_left et-waypoint et-animated">
														<?php the_field('content'); ?>
													</div>
													<?php endif; ?>
												</div>
												<div class="column-9">
													<a href="<?php echo (get_field('bg_description')?get_field('bg_description'):( has_post_thumbnail() ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : wp_get_attachment_image_src(21872, 'full')[0])); ?>" class="et_pb_lightbox_image">
														<img src="<?php echo (get_field('bg_description')?get_field('bg_description'):( has_post_thumbnail() ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : wp_get_attachment_image_src(21872, 'full')[0])); ?>" class="et_pb_animation_right et-waypoint et-animated">
													</a>
												</div>
											</div>
											<div id="tab-dt-02">
												<?php if(!empty(get_field('image_project'))) { ?>
												<div id="image-project">
													<?php foreach(get_field('image_project') as $item) : ?>
													<div class="item-image">
														<a href="<?php echo wp_get_attachment_image_src( $item['ID'], 'full' )[0]; ?>" class="et_pb_lightbox_image">
																		<img src="<?php echo wp_get_attachment_image_src( $item['ID'], 'small_crop' )[0]; ?>" />	
																	</a>
													</div>
													<?php endforeach; ?>
												</div>
												<?php }else{ ?>
												<div style="padding-top: 15px;">
													<h3 style="color: #fff;font-size: 17px;"><?php echo inline_trans('Chưa cập nhật nội dung...', 'Contents have not updated yet...', '内容尚未更新...', 'コンテンツはまだ更新されていません...'); ?></h3>
												</div>
												<?php } ?>
											</div>
											<div id="tab-dt-03">
												<?php 
													$image_post = get_posts( array( 'connected_type' => 'project_2_image360',
																				    'connected_items' => get_the_ID(),
																					'post_type' => 'image360'
																				   )) ;
													if ( !empty($image_post) ) {
														echo "<div class=\"image-360-container\" data-id=\"".get_the_ID()."\"></div>";
													} else {
														echo inline_trans('Đang cập nhật......', 'Updating......', '更新......', '更新中......');
													}
												?>
											</div>
										</div>
									</div>
									<!-- End row -->
								</div>
							</div>
						<?php $i++ ;								
							if($i%10==0){ $i = 1; } ?>
							<?php endwhile; 
							else:
								echo '
									<div style="margin-left: 20px; padding-bottom: 20px;">
										<h3 style="color: #fff;font-size: 17px;">'.inline_trans('Chưa cập nhật nội dung...', 'Contents have not updated yet...', '内容尚未更新...', 'コンテンツはまだ更新されていません...').'</h3>
									</div>	
								';
							endif;	
							echo '</div>';							
							?>
								<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
</div>
<script>
	(function($) {
		$(document).ready(function() {
			if (findGetParameter('tab')) {
				setTimeout(function() {
					$("li[slug="+findGetParameter('tab')+"]").click();
				}, 300);
				if (findGetParameter('projects')){
					setTimeout(function() {
						$('.project_'+findGetParameter('projects')+' > .content').click();
					}, 300);
				}
			}
		});
	})(jQuery)
	function findGetParameter(parameterName) {
		var result = null,
			tmp = [];
		var items = location.search.substr(1).split("&");
		for (var index = 0; index < items.length; index++) {
			tmp = items[index].split("=");
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		}
		return result;
	}
</script>
<?php get_footer(); ?>