<?php
$type = get_query_var('type');
$human_list = ($type=="leader")?get_field("human_list"):get_field("manager_list");
if (!empty($human_list)) {
?>
<div class="leader <?php echo ($type=="manager"?$type:""); ?>">
	<?php
	foreach($human_list as $human) {
		?>
	<div class="col-2 <?php if(count($human['role_list']) == 2 || $type == "manager" ) echo "double-role" ?>">
		<div class="avatar"><img class="alignnone" src="<?php echo $human['image']?>" /></div>
		<div class="main">
			<div class="title"><?php echo $human['name'] ?></div>
			<div class="sub-title">
				<?php				
				if ($type == "manager"){
					echo $human['role'];
				}
				else {
					$first = true;
					foreach($human['role_list'] as $role_item) {
						if (!$first) {
							echo "<br>";
						}
						else {
							$first = false;
						}
						echo $role_item['role_name'];
					}
				}
				?>
			</div>
			<div class="content">
				<?php if ( $type == "leader" ) echo $human['description']; ?>
			</div>
		</div>
	</div>
	<?php
	}
	?>
</div>
<?php }
?>