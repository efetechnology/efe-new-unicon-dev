<!-- <div class="homepage_careers">
	<?php //for ( $i = 1 ; $i < 8 ; $i++ ) : ?>
	<div class="job-item">
		<img src="/wp-content/uploads/2017/03/careers-demo-image.jpg">
		<div class="info">
			<p>Vị trí tuyển dụng</p>
			<h2 class="position">Giám sát xây dựng</h2>
			<p>
				Yêu cầu chung
				<br>
				- Trình độ: Tốt nghiệp Đại học chính quy trở lên
			</p>
			<p><a class="link" href="#">Tìm hiểu thêm</a></p>
		</div>
	</div>
	<?php //endfor; ?>
</div>
 -->


<div class="homepage_careers">
	<?php
 	$loop = new WP_Query( array( 'post_type' => 'career_position', 'ignore_sticky_posts' => 1, 'post_status' => 'publish') );
	if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post();
	 ?>
	<div class="job-item">
		<img src="<?php 	echo has_post_thumbnail()?get_the_post_thumbnail_url($post->ID, 'full'):'/wp-content/uploads/2017/03/careers-demo-image.jpg';?>">
		<div class="info">
		<div class="info-inner">
			<p><?php echo inline_trans('Vị trí tuyển dụng', 'Position', '位置', 'ポジション'); ?></p>
			<h2 class="position"><?php echo get_the_title(); ?></h2>
			<p>
				<?php echo inline_trans('Yêu cầu chung', 'Requirements', '要求', '要件'); ?>
			</p>
			<?php the_content(); ?>
			</div>
			<p><a class="link" href="/<?php echo get_post( pll_get_post(8990) )->post_name.'/?id=job-'.$post->ID; ?>"><?php echo inline_trans('Tìm hiểu thêm', 'Find more', '寻找更多', 'もっと見つけます'); ?></a></p>
		</div>
	</div>
	<?php
		 endwhile;

	    endif;
	    wp_reset_postdata();

	 ?>
</div>


























<script type="text/javascript">
	jQuery(function(){
		jQuery('.homepage_careers').slick({

			slidesToShow: 2,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			responsive: [
			    {
			      breakpoint: 1024,
			      settings: { slidesToShow: 2, }
			    },
			    {
			      breakpoint: 768,
			      settings: {
					slidesToShow: 1,
					arrows: false,
					dots: true,
					adaptiveHeight: true
					}
			    }
		  	],
		  	prevArrow: '<span type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;">&#8249;</span>',
		  	nextArrow: '<span type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;">&#8250;</span>',
		});
	});
</script>
