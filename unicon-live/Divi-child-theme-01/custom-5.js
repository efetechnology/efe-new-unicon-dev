(function($){ $(document).ready(function(){
	var $controller = $("<div></div>").addClass("controller");
	var $play_button = $("<i></i> ").addClass("fa");
	var	$mute_button = $play_button.clone();
	$controller.append($play_button.addClass('fa-pause'));
	$controller.append($mute_button.addClass('fa-volume-off'));
	$(".et_pb_section_video_bg").append($controller.clone());
	$(".controller i.fa").each(function(){
		if ($(this).hasClass('fa-pause')) {
			$(this).click(function(){
				$(this).parents(".et_pb_section_video_bg").find(".mejs-playpause-button button").click();
				if ($(this).hasClass("fa-pause")) {
					$(this).removeClass("fa-pause");
					$(this).addClass("fa-play");
				}
				else {
					$(this).removeClass("fa-play");
					$(this).addClass("fa-pause");
				}
			});
		}
		else if ($(this).hasClass('fa-volume-off')) {
			$(this).click(function(){
				$(this).parents(".et_pb_section_video_bg").find(".mejs-volume-button button").click();
				if ($(this).hasClass("fa-volume-up")) {
					$(this).removeClass("fa-volume-up");
					$(this).addClass("fa-volume-off");
				}
				else {
					$(this).removeClass("fa-volume-off");
					$(this).addClass("fa-volume-up");
				}
			});
		}
	});
	$("video").each(function(){
		$(this).get(0).addEventListener('play', function(){
			$(this).parents('.et_pb_section_video_bg').find('.fa-play').removeClass('fa-play').addClass('fa-pause');
		}, false);
		$(this).get(0).addEventListener('pause', function(){
			$(this).parents('.et_pb_section_video_bg').find('.fa-pause').removeClass('fa-pause').addClass('fa-play');
		}, false);
	});
	$("video").each(function(){
		this.muted = true;
	});
}); })(jQuery)