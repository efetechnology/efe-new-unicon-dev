<?php

get_header();

?>

<div id="main-content">

<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="container">
			<div id="content-area" class="clearfix">
				<div class="entry-content">
					<div class="et_main_title">
						<h1 class="entry-title test" ><?php the_title(); ?></h1>
						<span class="et_project_categories"><?php echo get_the_term_list( get_the_ID(), 'project_category', '', ', ' ); ?></span>
					</div>
					<div class="content-viewer">
						<?php
						$pdf_id = get_field('id_view_pdfs');
						if(!empty($pdf_id)) {
							if (preg_match("/\[.*\]/", $pdf_id)) {
								echo do_shortcode($pdf_id);
							}
							else {
								echo do_shortcode( '[responsive-flipbook id="' . $pdf_id . '"]' );
							}
						}
						
						?>
					</div>
					<?php
						the_content();
					?>
				</div> <!-- .entry-content -->
			</div>
		</div>
	</article> <!-- .et_pb_post -->

<?php endwhile; ?>

</div> <!-- #main-content -->

<?php get_footer(); ?>