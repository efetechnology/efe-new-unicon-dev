<?php

/**
 * Site setting page
 * This file contains code for 
 */

function setting_config(){

    $config = array(
        'admin_menu' => array(
            'menu_name'  => 'Site Settings', /* Name display in menu */
            'page_title' => 'Unicons Site Settings', /* Name display in browser tab */
            'capability' => 'manage_options',
            'menu_slug'  => 'site-setting', 
            'function'   => 'site_output_setting_page', /* Function to show setting page */
            'icon'       => get_stylesheet_directory_uri() . '/images/U-20x20.png', /* Icon for admin menu */
        ),
        'inputs' => array( /* List of input, format: 'setting_name' => 'Label' */
            'unicons_facebook' => 'Facebook',
            'unicons_linkedin' => 'Linkedin',
            'unicons_youtube' => 'YouTube',
            'unicons_google_plus' => 'Google Plus',
            'unicons_portal' => 'Unicons Portal',
        ),
        'footers-vn' => array( /* List of input, format: 'setting_name' => 'Label' */
            'title_logo_vn' => 'Title Logo',
            'headquarters_vn' => 'Headquarters',
            'contacts_headquarters_vn' => 'Contacts Headquarter',
            'rep_office_vn' => 'Representative Office',
            'contacts_rep_office_vn' => 'Contacts Representative Office',
            'title_social_vn' => 'Title Social Network',
            'members_vn' => 'Members Group',
            'copyright_vn' => 'Text Copyright',
        ),
        'footers-en' => array( /* List of input, format: 'setting_name' => 'Label' */
            'title_logo_en' => 'Title Logo',
            'headquarters_en' => 'Headquarters',
            'contacts_headquarters_en' => 'Contacts Headquarter',
            'rep_office_en' => 'Representative Office',
            'contacts_rep_office_en' => 'Contacts Representative Office',
            'title_social_en' => 'Title Social Network',
            'members_en' => 'Members Group',
            'copyright_en' => 'Text Copyright',
        ),
        'footers-zh' => array( /* List of input, format: 'setting_name' => 'Label' */
            'title_logo_zh' => 'Title Logo',
            'headquarters_zh' => 'Headquarters',
            'contacts_headquarters_zh' => 'Contacts Headquarter',
            'rep_office_zh' => 'Representative Office',
            'contacts_rep_office_zh' => 'Contacts Representative Office',
            'title_social_zh' => 'Title Social Network',
            'members_zh' => 'Members Group',
            'copyright_zh' => 'Text Copyright',
        ),
        'footers-ja' => array( /* List of input, format: 'setting_name' => 'Label' */
            'title_logo_ja' => 'Title Logo',
            'headquarters_ja' => 'Headquarters',
            'contacts_headquarters_ja' => 'Contacts Headquarter',
            'rep_office_ja' => 'Representative Office',
            'contacts_rep_office_ja' => 'Contacts Representative Office',
            'title_social_ja' => 'Title Social Network',
            'members_ja' => 'Members Group',
            'copyright_ja' => 'Text Copyright',
        ),
    );
    return $config;
}

/**
 *
 */
add_action("admin_menu", "custom_setting_admin_menu");
function custom_setting_admin_menu () {

    $config = setting_config();
    extract( $config['admin_menu'] );
    add_menu_page($page_title, $menu_name, $capability, $menu_slug, $function, $icon, 99);

    //call register settings function
    add_action( 'admin_init', 'register_site_settings' );
}

/**
 * Register setting
 */
function register_site_settings() {
    $config = setting_config();
    foreach ( $config['inputs'] as $setting_name => $label ) {
       register_setting( 'site-setting-group', $setting_name );
    }
    foreach ( $config['footers-vn'] as $setting_name => $label ) {
        register_setting( 'site-setting-group', $setting_name );
    }
    foreach ( $config['footers-en'] as $setting_name => $label ) {
        register_setting( 'site-setting-group', $setting_name );
    }
    foreach ( $config['footers-zh'] as $setting_name => $label ) {
        register_setting( 'site-setting-group', $setting_name );
    }
    foreach ( $config['footers-ja'] as $setting_name => $label ) {
        register_setting( 'site-setting-group', $setting_name );
    }
}

/**
 * Render inputs in admin setting page
 */
function site_output_setting_page() {
    $config = setting_config();
?>
    <div class="wrap">

        <h1><?php echo $config['admin_menu']['page_title']; ?></h1>
        
        <?php settings_errors(); ?>
        
        <form method="post" action="options.php">
            
            <?php settings_fields( 'site-setting-group' ); ?>
            <?php do_settings_sections( 'site-setting-group' ); ?>

            <table class="form-table">
                <?php foreach ( $config['inputs'] as $setting_name => $label ) : ?>
                    <tr valign="top">
                        <th scope="row"><?php echo $label; ?></th>
                        <td><input type="text" name="<?php echo $setting_name; ?>" value="<?php echo esc_attr( get_option($setting_name) ); ?>" class="regular-text" /></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <div class="st-footer">
                <h2>Setting Footer</h2>
                <div id="tab-footer">
                    <ul>
                        <li>Vietnamese</li>
                        <li>English</li>
                        <li>Chinese</li>
                        <li>Japanese</li>
                    </ul>
                </div>
                <div class="content-tab">
                        <div class="contents">
                            <table>
                                <?php foreach ( $config['footers-vn'] as $setting_name => $label ) : ?>
                                    <tr valign="top">
                                        <th scope="row"><?php echo $label; ?></th>
                                        <td><input type="text" name="<?php echo $setting_name; ?>" value="<?php echo esc_attr( get_option($setting_name) ); ?>" class="regular-text" /></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <div class="contents">
                            <table>
                                <?php foreach ( $config['footers-en'] as $setting_name => $label ) : ?>
                                    <tr valign="top">
                                        <th scope="row"><?php echo $label; ?></th>
                                        <td><input type="text" name="<?php echo $setting_name; ?>" value="<?php echo esc_attr( get_option($setting_name) ); ?>" class="regular-text" /></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <div class="contents">
                            <table>
                                <?php foreach ( $config['footers-zh'] as $setting_name => $label ) : ?>
                                    <tr valign="top">
                                        <th scope="row"><?php echo $label; ?></th>
                                        <td><input type="text" name="<?php echo $setting_name; ?>" value="<?php echo esc_attr( get_option($setting_name) ); ?>" class="regular-text" /></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <div class="contents">
                            <table>
                                <?php foreach ( $config['footers-ja'] as $setting_name => $label ) : ?>
                                    <tr valign="top">
                                        <th scope="row"><?php echo $label; ?></th>
                                        <td><input type="text" name="<?php echo $setting_name; ?>" value="<?php echo esc_attr( get_option($setting_name) ); ?>" class="regular-text" /></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                </div>
            </div>
            <?php submit_button(); ?>
        
        </form>

    </div>
<?php }