/* Project page */
(function($) {
	$(document).ready(function() {
		var this_ = '';
		var height_li = '';
		var height_expander = '';
		$('.custom_tab_project li a').click(function() {

			$(".project-detail-tab").appendTo('.save-detail');
			/*$( ".project-detail-tab .tab-infomation .et_pb_tabs_controls " ).removeClass('et_pb_tab_active');
			$( ".project-detail-tab .tab-infomation .et_pb_tabs_controls " ).add('et_pb_tab_active');*/
			this_ = $(this);
			var element = this_.next().next().next();
			height_li = $(this).parent().height();
			if (!this_.parent().hasClass('og-expanded')) {
				var prevNowPlaying = setInterval(function() {
					$('.og-expander').addClass("overlay overlay-blue");
					var element = this_.next().next().next();
					if (element.hasClass('og-expander')) {
						clearInterval(prevNowPlaying);
						//$( ".tab-infomation" ).clone().appendTo(this_.next().next().children());
						$(".project-detail-tab").appendTo(this_.next().next().next().children());


						/*var height = height_expander + height_li;
						this_.parent().css("height", height);*/
						$('.custom_tab_project .og-expander').css('height', 'auto');
						var autoHeight = $('.custom_tab_project .og-expander').height();
						$('.custom_tab_project .og-expander').animate({
							'height': autoHeight + "px"
						}, 600);
						var height = autoHeight + height_li;
						$('.custom_tab_project .og-expander').parent().css("height", height + 25);

						$('.og-close').html("Đóng");
						$('.og-expander .og-close').click(function() {
							$(".project-detail-tab").appendTo('.save-detail');
						});
					}
				}, 1000);
			} else {
				$(".project-detail-tab").appendTo('.save-detail');
			}
		});
		$('.section-inomation li a').click(function() {
			$('.custom_tab_project .og-expander').css("height", "auto");
			var setheight = setInterval(function() {
				var height_expander_new = $('.og-expander').height();
				if (height_expander_new != height_expander) {
					var height = height_expander_new + height_li;
					height_expander = height_expander_new;
					this_.parent().css("height", height + 25);
					clearInterval(setheight);
				}
			}, 1000);
		});
		$('.custom_tab_project .et_pb_all_tabs li')
			.find('span')
			.each(function() {
				var this_1 = $(this);
				this_1.insertAfter(this_1.closest('li').find('a'))
			})
			.closest('li').find('figure').remove();

		//Set Img hover
		var imgs_project = $('.custom_tab_project .et_pb_all_tabs li').find('a').find('img');
		imgs_project.each(function() {

			var this_2 = $(this);

			$('<div class="image-wrap"></div>').prependTo(this_2.parent()).append(this_2);
		})
		/*Change link youtube*/
		function linkifyYouTubeURLs(text) {
			var re = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*?[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
			return text.replace(re, 'https://www.youtube.com/embed/$1?feature=oembed&amp;wmode=opaque');
		}
		/*Tab video in page /thu-vien/*/
		$(window).on("load", function() {
			$('#video .small_post_image, #video .small_post_info').click(function() {
				var title = $(this).parent().find('.small_post_info').text();
				var video_url = $(this).parent().find('.video_url').text();
				var post_excerpt = $(this).parent().find('.post_excerpt').html();
				var post_content = $(this).parent().find('.post_content').html();
				var code_link = linkifyYouTubeURLs(video_url);
				$('#video .media_fullwidth .big_post_title h1').html(title);
				$('#video .media_fullwidth .big_post_excerpt').html(post_excerpt + post_content);
				$('#video .media_fullwidth .video_wraper iframe').attr('src', code_link);
			});
			/*Tab Images in page /thu-vien/*/
			$('#images .small_post_image, #images .small_post_info').click(function() {
				var title = $(this).parent().find('.small_post_info h3').text();
				var post_excerpt = $(this).parent().find('.post_excerpt').html();
				var post_content = $(this).parent().find('.post_content').html();
				$('#images .media_fullwidth .big_post_title h1').html(title);
				$('#images .media_fullwidth .big_post_excerpt').html('<strong>' + post_excerpt + '</strong>' + post_content);
			});
			var slider_parent = $('#images .image_wraper');
			var nav_parent = $('#images .nav_parent');
			if (!slider_parent.length || !nav_parent.length) return;
			var tab = nav_parent.parents('.et_pb_tab').eq(0);
			tab.css("display", "block");
			slider_parent.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.nav_parent',
				lazyLoad: 'ondemand',
			});
			nav_parent.slick({
				slidesToShow: 10,
				vertical: true,
				asNavFor: '.image_wraper',
				centerMode: true,
				focusOnSelect: true,
				lazyLoad: 'ondemand',
				arrows: false,
				adaptiveHeight: true,
			});
			if ($(window).width() < 1024 && $(window).width() > 767) {
				nav_parent.find(">:first-child").css("height", "");
				nav_parent.find(">:first-child>:first-child").css("height", "");
			}
			tab.css("display", "none");
		});
	});

})(jQuery)