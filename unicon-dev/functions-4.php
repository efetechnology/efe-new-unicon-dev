<?php

/**
 * Enqueue
 */
function wp_enqueue_scripts_4_func() {
    wp_enqueue_style( 'custom_stylesheet-4', get_stylesheet_directory_uri() . '/custom-4.css' );
    wp_enqueue_script( 'custom_script-4', get_stylesheet_directory_uri() . '/custom-4.js', array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'wp_enqueue_scripts_4_func' );