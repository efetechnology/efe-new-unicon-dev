<?php

/**
 * Test
 */
function wp_enqueue_scripts_3_func() {
    wp_enqueue_style( 'custom_stylesheet-3', get_stylesheet_directory_uri() . '/custom-3.css' );
    wp_enqueue_script( 'custom_script-3', get_stylesheet_directory_uri() . '/custom-3.js', array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'wp_enqueue_scripts_3_func' );

/**
 * Add Divi Builder to Custom Post Type
 */
function my_et_builder_post_types( $post_types ) {
    $post_types[] = 'career_position';
    $post_types[] = 'student';
     
    return $post_types;
}
add_filter( 'et_builder_post_types', 'my_et_builder_post_types' );

/**
 * Add Divi Setting box to Custom Post Type
 */
add_action('add_meta_boxes', 'divicolt_add_meta_box');
function divicolt_add_meta_box()
{
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'editor') and function_exists('et_single_settings_meta_box_student')) {
            $obj= get_post_type_object( $post_type );
            add_meta_box('et_settings_meta_box', sprintf(__('Divi %s Settings', 'Divi'), $obj->labels->singular_name), 'et_single_settings_meta_box_student', $post_type, 'side', 'high');
        }
    }
}

add_action('admin_head', 'divicolt_admin_js');
function divicolt_admin_js()
{
    $s = get_current_screen();
    if (!empty($s->post_type) and $s->post_type != 'page' and $s->post_type != 'post') {
        ?>
        <script>
            jQuery(function ($) {
                $('#et_pb_layout').insertAfter($('#et_pb_main_editor_wrap'));
            });
        </script>
        <?php
    }
}

/**
 * Shortcode [documnet_companny]
 */
add_shortcode( 'documnet_companny', function($atts) {
    if (empty (get_field('tai_lieu_nhan_su'))) return "";
    ob_start();
    ?>
    <div class="container company_document">
    	<table class="table">
  <thead>
    <tr>
      
      <th>Tài liệu</th>
      <th>Ngày</th>
      <th>Tải về</th>
    </tr>
  </thead>
  <tbody>
    <?php
   	 if( have_rows('tai_lieu_nhan_su') ):
   	 	$i = 1;

   	 	while ( have_rows('tai_lieu_nhan_su') ) : the_row();
                // display a sub field value
                $tai_lieu = get_sub_field('tai_lieu');
                $date = get_sub_field('ngay');
                $url = get_sub_field('tai_ve');
    ?>

      <tr class="<?php echo ($i%2 != 0)?'odd_td':''; ?>" >
		     <!--  <td scope="row"><b><?php  //echo $i; ?></b></td> -->
		      <td><?php  echo $tai_lieu?$tai_lieu:""; ?></td>
		      <td><?php  echo $date?$date:""; ?></td>
		      <td><a href='<?php  echo $url['url']?$url['url']:""; ?>' download>Tải về <i class="fa fa-download" aria-hidden="true"></i></a></td>
    </tr>
    <?php  $i++;  endwhile;    ?>
		    </tbody>
		</table>
		</div>

    <?php       else :
                    // no rows found
                endif;

    return ob_get_clean();
    } );

//Custom pagination
function custom_pagination($numpages = '', $pagerange = '', $paged='', $page_url='') {
  if (empty($pagerange)) {
    $pagerange = 2;
  }
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }
  if ($page_url=='') {
	$page_url = $_SERVER[REQUEST_URI];
	$offset = strpos($page_url, '?' );
	if ( $offset !== false) {
	  $page_url = substr($page_url, 0, $offset - 1 );
	}
	$offset = strpos($page_url, '/page/' );
	if ( $offset !== false) {
	  $page_url = substr($page_url, 0, $offset );
	}
	if ( substr( $page_url, -1) == '/' ) {
	  $page_url = substr($page_url, 0, strlen( $page_url ) - 1 );
	}
  }
  else {
	$page_url = '/' . $page_url;
  }
  $pagination_args = array(
    'base'            => site_url() . $page_url . '%_%',
    'format'          => '/page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __("<div class=\"\"><i class=\"fa fa-chevron-left\"></i></div>"),
    'next_text'       => __("<div class=\"\"><i class=\"fa fa-chevron-right\"></i></div>"),
    'type'            => 'plain',
    'add_args'        => isset($_GET['s'])?$_GET['s']:false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      // echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}





function et_single_settings_meta_box_student( $post ) {
	$post_id = get_the_ID();

	wp_nonce_field( basename( __FILE__ ), 'et_settings_nonce' );

	$page_layout = get_post_meta( $post_id, '_et_pb_page_layout', true );

	$side_nav = get_post_meta( $post_id, '_et_pb_side_nav', true );

	$project_nav = get_post_meta( $post_id, '_et_pb_project_nav', true );

	$post_hide_nav = get_post_meta( $post_id, '_et_pb_post_hide_nav', true );
	$post_hide_nav = $post_hide_nav && 'off' === $post_hide_nav ? 'default' : $post_hide_nav;

	$show_title = get_post_meta( $post_id, '_et_pb_show_title', true );

	$page_layouts = array(
		'et_right_sidebar'   => esc_html__( 'Right Sidebar', 'Divi' ),
		'et_left_sidebar'    => esc_html__( 'Left Sidebar', 'Divi' ),
		'et_full_width_page' => esc_html__( 'Full Width', 'Divi' ),
	);

	$layouts = array(
		'light' => esc_html__( 'Light', 'Divi' ),
		'dark'  => esc_html__( 'Dark', 'Divi' ),
	);
	$post_bg_color  = ( $bg_color = get_post_meta( $post_id, '_et_post_bg_color', true ) ) && '' !== $bg_color
		? $bg_color
		: '#ffffff';
	$post_use_bg_color = get_post_meta( $post_id, '_et_post_use_bg_color', true )
		? true
		: false;
	$post_bg_layout = ( $layout = get_post_meta( $post_id, '_et_post_bg_layout', true ) ) && '' !== $layout
		? $layout
		: 'light'; ?>

	<p class="et_pb_page_settings et_pb_page_layout_settings">
		<label for="et_pb_page_layout" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Page Layout', 'Divi' ); ?>: </label>

		<select id="et_pb_page_layout" name="et_pb_page_layout">
		<?php
		foreach ( $page_layouts as $layout_value => $layout_name ) {
			printf( '<option value="%2$s"%3$s>%1$s</option>',
				esc_html( $layout_name ),
				esc_attr( $layout_value ),
				selected( $layout_value, $page_layout, false )
			);
		} ?>
		</select>
	</p>
	<p class="et_pb_page_settings et_pb_side_nav_settings" style="display: none;">
		<label for="et_pb_side_nav" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Dot Navigation', 'Divi' ); ?>: </label>

		<select id="et_pb_side_nav" name="et_pb_side_nav">
			<option value="off" <?php selected( 'off', $side_nav ); ?>><?php esc_html_e( 'Off', 'Divi' ); ?></option>
			<option value="on" <?php selected( 'on', $side_nav ); ?>><?php esc_html_e( 'On', 'Divi' ); ?></option>
		</select>
	</p>
	<p class="et_pb_page_settings">
		<label for="et_pb_post_hide_nav" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Hide Nav Before Scroll', 'Divi' ); ?>: </label>

		<select id="et_pb_post_hide_nav" name="et_pb_post_hide_nav">
			<option value="default" <?php selected( 'default', $post_hide_nav ); ?>><?php esc_html_e( 'Default', 'Divi' ); ?></option>
			<option value="no" <?php selected( 'no', $post_hide_nav ); ?>><?php esc_html_e( 'Off', 'Divi' ); ?></option>
			<option value="on" <?php selected( 'on', $post_hide_nav ); ?>><?php esc_html_e( 'On', 'Divi' ); ?></option>
		</select>
	</p>

<?php if ( 'student' === $post->post_type ) : ?>
	<p class="et_pb_page_settings et_pb_single_title" style="display: none;">
		<label for="et_single_title" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Post Title', 'Divi' ); ?>: </label>

		<select id="et_single_title" name="et_single_title">
			<option value="on" <?php selected( 'on', $show_title ); ?>><?php esc_html_e( 'Show', 'Divi' ); ?></option>
			<option value="off" <?php selected( 'off', $show_title ); ?>><?php esc_html_e( 'Hide', 'Divi' ); ?></option>
		</select>
	</p>

	<p class="et_divi_quote_settings et_divi_audio_settings et_divi_link_settings et_divi_format_setting et_pb_page_settings">
		<label for="et_post_use_bg_color" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Use Background Color', 'Divi' ); ?></label>
		<input name="et_post_use_bg_color" type="checkbox" id="et_post_use_bg_color" <?php checked( $post_use_bg_color ); ?> />
	</p>

	<p class="et_post_bg_color_setting et_divi_format_setting et_pb_page_settings">
		<input id="et_post_bg_color" name="et_post_bg_color" class="color-picker-hex" type="text" maxlength="7" placeholder="<?php esc_attr_e( 'Hex Value', 'Divi' ); ?>" value="<?php echo esc_attr( $post_bg_color ); ?>" data-default-color="#ffffff" />
	</p>

	<p class="et_divi_quote_settings et_divi_audio_settings et_divi_link_settings et_divi_format_setting">
		<label for="et_post_bg_layout" style="font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Text Color', 'Divi' ); ?>: </label>
		<select id="et_post_bg_layout" name="et_post_bg_layout">
	<?php
		foreach ( $layouts as $layout_name => $layout_title )
			printf( '<option value="%s"%s>%s</option>',
				esc_attr( $layout_name ),
				selected( $layout_name, $post_bg_layout, false ),
				esc_html( $layout_title )
			);
	?>
		</select>
	</p>
<?php endif;
}

function save_student_post_title_setting( $post_id ) {
	wp_mail('namnguyen.cs13@gmail.com', 'saved', 'Done');
	$show_title = sanitize_text_field( $_POST['et_single_title'] );
	update_post_meta( $post_id, '_et_pb_show_title', $show_title );
}
add_action( 'save_post_student', 'save_student_post_title_setting',10,1 );
