<div class="media_tab media_internal_magazine">
<?php
    $args = array(
        'posts_per_page' => -1,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'tap_san_noi_bo',
        'post_status' => 'publish',
    );
    $the_query = new WP_Query($args);
    if ($the_query->have_posts()){
        while ($the_query->have_posts()) : $the_query->the_post();
            ?>
                <div class="media_item">
                    <div class="featured_image">
                        <a href="<?php echo get_permalink(); ?>">
                            <div class="image-wrap">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                            </div>
                        </a>
                    </div>
                    <div class="item_info">
                        <div class="item-title">
                            <a href="<?php echo get_permalink(); ?>">
                                <h2><?php echo get_the_title();?></h2>                    
                            </a>
                            <span>Ngày phát hành: <?php echo get_field('release_date');?></span>
                        </div>
                        <div class="button-group">
                            <?php //print_r(get_field('id_view_pdfs'));?>
                            <a class="button" href="<?php echo get_permalink(); ?>">Xem</a>
                            <a class="button" href="<?php $file = get_field('link_download'); echo $file['url'];?>" download>Tải về</a>
                            <div class="hide"><?php //echo PdfLightViewer_FrontController::display_pdf_book(array('id' => '18056'));?></div>
                        </div>
                    </div>
                </div>
            <?php
        endwhile;
        wp_reset_query();
    }
?>
</div>