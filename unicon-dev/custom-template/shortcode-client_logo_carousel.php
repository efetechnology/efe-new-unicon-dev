<?php
$client_logos = get_terms( array(
        'taxonomy' => 'du_an_cao_tang',//array('du_an_cao_tang','du_an_cong_nghiep','khach_san_nghi_duong','giao_duc'),
        'orderby'    => 'count',
        'hide_empty' => 0,
    ));
//print_r(count($client_logos));
?>

<div class="client_logo_carousel">
    <?php foreach ( $client_logos as $logo ) : ?>
    <div><a href="/khach-hang-va-doi-tac/?clientid=<?php echo $logo->term_id; ?>"><img src="<?php echo get_wp_term_image($logo->term_id); ?>"></a></div>
    <?php endforeach; ?>
</div>