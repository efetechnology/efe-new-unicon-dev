<?php
if ( !empty( $main_activities = get_field('activities_list' ) ) ) {
	?>
	<div class="content table">
		<table class="history">
			<tbody>
				<?php
				foreach( $main_activities as $act) {
					?>
					<tr style="text-align: justify;">
						<th><span style="color: #000000;"><?php echo $act['year']; ?></span></th>
						<td><span style="color: #000000;"><?php echo $act['content']; ?></span></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
}