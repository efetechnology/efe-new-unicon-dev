<div id="career-handbook">
	<?php if(!empty(get_field('question_answer'))) : ?>
		<div class="question_answer">
			<div class="abilities">
				<div class="section">
				 	<div class="title">Những câu hỏi thường gặp</div>
				</div>
			</div>	
			<?php $i = 1; foreach(get_field('question_answer') as $list) : ?>
				<div class="item">
					<span class="sub">+</span>
					<span class="question"><?php echo $i; ?>/<?php echo $list['questions']; ?></span>
					<div class="answer" <?php if($i==1) echo 'style="display: block;"'; ?>>
						<?php if(empty($list['answer'])) :?>
							<p>Chưa có câu trả lời.</p>
						<?php else: ?>
							<?php echo $list['answer']; ?>
						<?php endif; ?>							
					</div>
				</div>
			<?php $i++; endforeach;  ?>
		</div>		
	<?php endif; ?>
	<?php $posts = get_posts( array( 'post_type' => 'student' ) );
	?>
	<?php if(!empty($posts)) : ?>
		<div class="for_student">
			<div class="abilities">
				<div class="section">
				 	<div class="title">Dành cho sinh viên</div>
				</div>
			</div>			
			<?php foreach( $posts as $post ) : ?>
				<div class="item">
					<div class="left">
						<img src="<?php echo get_the_post_thumbnail_url( $post ); ?>">
					</div>
					<div class="right">
						<a href="<?php echo get_permalink( $post ); ?>">
							<h2><?php echo $post->post_title ; ?></h2>
						</a>
						<?php echo $post->post_excerpt ; ?>
					</div>
				</div>
			<?php endforeach;  ?>
		</div>		
	<?php endif; ?>
</div>