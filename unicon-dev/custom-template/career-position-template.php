<?php
/*
* Template name: Career position
*/
get_header();
?>

<div id="main-content" class="career-position-wrap">
	<?php
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			the_content();
			$query = new WP_Query( array( 'post_type' =>  'career_position',
										  'post_status' => 'publish',
										)
								  );
			if( $query->have_posts() ){ 
				echo '<div class="wrap-item">';
				echo '<div class="et_pb_row">';
				while ( $query->have_posts() ){ $query->the_post(); ?>
				<item class="column-3">
					<img src="<?php the_post_thumbnail_url('full'); ?>" style="max-height: 200px;" id="job-<?php echo get_the_ID(); ?>">
					<h4 style="text-align: center; font-weight: bold;"><?php echo get_the_title(); ?></h4>			
					<div class="arrow"></div>		
					<div class="detail" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">						
						<div class="et_pb_row">
							<div class="top">
								<h4 class="position">Vị trí: <?php echo get_the_title() ;?></h4>
								<h4 class="close view_all_1">Đóng</h4>
							</div>							
							<div class="column-2 first">						
								<div class="item-detail">
									<h6>Mô tả công việc</h6>
									<div class="content">
										<?php echo get_field('description'); ?>
									</div>
								</div>
								<div class="item-detail">
									<h6>Yêu cầu năng lực</h6>
									<div class="content">
										<?php echo get_field('requirement'); ?>
									</div>
								</div>
								<div class="item-detail">
									<h6>Hồ sơ ứng tuyển</h6>
									<div class="content">
										<?php echo get_field('profile'); ?>
									</div>
								</div>
							</div>
							<div class="column-2 last">
								<div class="item-detail">
									<img src="<?php the_post_thumbnail_url('full'); ?>">
									<h6>Hướng dẫn nộp hồ sơ</h6>
									<div class="content">
										<?php echo get_field('instruction'); ?>
									</div>
								</div>
								<div><a class="view_all_1" href="/quy-trinh-tuyen-dung#gform_wrapper_3">Ứng tuyển</a></div>
							</div>
						</div>						
					</div>
				</item>	
				<?php } 
				echo '</div>';					
				echo '</div>';	
			}
		}
	}					
	?>
</div>			

<?php get_footer(); ?>