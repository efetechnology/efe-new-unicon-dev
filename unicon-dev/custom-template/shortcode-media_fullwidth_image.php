<div class="image-view">
	<?php foreach(get_field('gallery_slider') as $slider): ?>
		<div>
			<img data-lazy="<?php echo $slider['url']; ?>">
		</div>
	<?php endforeach; ?>
	<?php foreach(get_field('gallery_slider') as $slider): ?>
		<div>
			<img data-lazy="<?php echo $slider['url']; ?>">
		</div>
	<?php endforeach; ?>
</div>
<div class="image-nav">
	<?php foreach(get_field('gallery_slider') as $slider): ?>
		<div>
			<img data-lazy="<?php echo $slider['url']; ?>">
		</div>
	<?php endforeach; ?>
	<?php foreach(get_field('gallery_slider') as $slider): ?>
		<div>
			<img data-lazy="<?php echo $slider['url']; ?>">
		</div>
	<?php endforeach; ?>
</div>