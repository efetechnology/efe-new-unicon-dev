<?php if ( $type == 'lotrinh' ) : ?>
	<?php if(!empty(get_field('lt_section_one'))) : ?>
		<?php foreach(get_field('lt_section_one') as $list) : ?>
			<div class="et_pb_row">
				<div class="et_pb_column et_pb_column_1_2">
					<div class="et_pb_animation_right et-waypoint">
						<?php echo $list['section_one_text']; ?>
					</div>
				</div> <!-- .et_pb_column -->
				<div class="et_pb_column et_pb_column_1_2">						
					<img src="<?php echo $list['section_one_image']; ?>" class="et_pb_animation_right et-waypoint">						
				</div> <!-- .et_pb_column -->
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if(!empty(get_field('lt_image_large'))) : ?>
		<div class="et_pb_row" style="margin: 30px 0;">		
			<img src="<?php echo get_field('lt_image_large'); ?>" class="et_pb_animation_right et-waypoint">
		</div>
	<?php endif; ?>

	<?php if(!empty(get_field('lt_section_two'))) : ?>
		<div class="et_pb_row">		
			<div class="et_pb_animation_right et-waypoint">
				<?php echo get_field('lt_section_two'); ?>
			</div>
		</div>
	<?php endif; ?>	

<?php elseif ( $type == 'daotao' ) : ?>
	<?php if(!empty(get_field('dt_section'))) : ?>
		<?php foreach(get_field('dt_section') as $list): ?>
			<div class="et_pb_row career-trainning">		
				<img src="<?php echo $list['dt_image_large']; ?>" class="et_pb_animation_right et-waypoint">	
				<div class="content" style="text-align: center;">
					<h3 class="et_pb_animation_right et-waypoint"><?php echo $list['dt_title']; ?></h3>
					<div class="text et_pb_animation_right et-waypoint"><?php echo $list['dt_content']; ?></div>
					<hr />
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

<?php elseif ( $type == 'phucloi') : ?>
	<?php if(!empty(get_field('pl_content_text'))) : ?>
		<div style="margin-bottom: 50px;" class="et_pb_animation_right et-waypoint"><?php echo get_field('pl_content_text'); ?></div>
	<?php endif; ?>
	<?php if(!empty(get_field('pl_images'))) : ?>
		<div class="img-phucloi">
			<?php foreach(get_field('pl_images') as $list): ?>
				<div class="et_pb_row career-welfare">		
					<img src="<?php echo $list['pl_image']; ?>" class="et_pb_animation_right et-waypoint">	
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
<?php endif; ?>

