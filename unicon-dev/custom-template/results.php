<?php
/*
* Template Name: Results
*/
get_header();
?>
<div id="main-content" class="project-wrap">
	<?php 
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			the_content();
		}
	}
	?>
	<div id="form-search">
		<div class="et_pb_row">
		<?php
			$taxonomy = 'project_category';
			$year = get_term_by('slug', 'du-an-theo-nam', $taxonomy);
			$year_id = $year->term_id;
			$term_children_year = get_term_children( $year_id, $taxonomy );

			$area = get_term_by('slug', 'du-an-theo-vung-mien', $taxonomy);
			$area_id = $area->term_id;
			$term_children_area = get_term_children( $area_id, $taxonomy );

			$term_search = array();
			if(isset($_GET['y']) && $_GET['y'] !=''){ $y = $_GET['y']; $term_search[] = $y;}
			if(isset($_GET['a']) && $_GET['a'] !=''){ $a = $_GET['a']; $term_search[] = $a;}
			if(isset($_GET['k']) && $_GET['k'] !='') $k = $_GET['k'];
		?>
			<form action="/result/" method="get">
				<span>Sắp xếp theo:</span>
				<select name="y" onchange="this.form.submit()">
				<option value="">Năm</option>	
				<?php foreach($term_children_year as $child) : $term = get_term_by( 'id', $child, $taxonomy ); ?>						
					<option value="<?php echo $term->slug; ?>" <?php if($y == $term->slug){ echo 'selected'; } ?>><?php echo $term->name; ?></option>	
				<?php endforeach; ?>
			</select>
				<select name="a" onchange="this.form.submit()">
				<option value="">Vùng miền</option>
				<?php foreach($term_children_area as $child) : $term = get_term_by( 'id', $child, $taxonomy ); ?>			
					<option value="<?php echo $term->slug; ?>" <?php if($a == $term->slug){ echo 'selected'; } ?>><?php echo $term->name; ?></option>	
				<?php endforeach; ?>
			</select>
				<input type="text" name="k" value="<?php echo $k; ?>" placeholder="Tìm kiếm" />
			</form>
		</div>
	</div>
	<div id="wrap-page">
		<?php
			if(!$y && !$a && !$k){
				echo '<div class="et_pb_row">Không tìm thấy kết quả tìm kiếm</div>';
				get_footer();
				return;
			}
		?>
		<div class="content-tab">
			<div class="et_pb_row">
				<div class="wrap">
					<div class="contents">
					<?php
						$args = array(
							'post_type' => 'project',
							'posts_per_page' => 27,
							'orderby'    => 'title',
							'order' => 'ASC',
							'post_title_like' => $k,
						);
						if(!empty($term_search)){
							$args['tax_query'] = array(	array(
												'taxonomy' => 'project_category',  
												'field' => 'slug',          
												'terms' => $term_search,
											)
										);
						}
						$contents = new WP_Query( $args );
						if($contents->have_posts()):
						while ( $contents->have_posts() ) : $contents->the_post(); ?>
							<div class="column-3 item <?php echo 'project_'.$post->ID; ?>">
								<div class="content">
									<img src="<?php if ( has_post_thumbnail() ) echo get_the_post_thumbnail_url($post->ID,'small_crop'); else echo wp_get_attachment_image_src(21872, 'small_crop')[0] ?>">
									<h4>
										<?php the_title(); ?>
									</h4>
									<div class="arrow"></div>
								</div>
								<div class="group-detail" style="background-image: url(<?php the_field('bg_description'); ?>)">
									<div class="et_pb_row">
										<h3 class="title-project">
											<?php the_title(); ?>
										</h3>
										<span class="close">Đóng</span>
										<div class="tab-detail">
											<ul>
												<li id="tab-dt-01" class="active">Thông tin dự án</li>
												<li id="tab-dt-02">Hình ảnh dự án</li>
												<li id="tab-dt-03" class="tab-image-360">Ảnh 360</li>
												<div class="clear"></div>
											</ul>
										</div>
										<div class="tab-detail-content">
											<div id="tab-dt-01">
												<div class="column-3">
													<?php if(!empty(get_field('location'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title">Địa điểm</div>
														<div class="value-info">
															<?php the_field('location'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('customer'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title">Khách hàng</div>
														<div class="value-info">
															<?php the_field('customer'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('general_contractors'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title">Tổng thầu</div>
														<div class="value-info">
															<?php the_field('general_contractors'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('construction_item'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title">Hạng mục thi công</div>
														<div class="value-info">
															<?php the_field('construction_item'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('contract_value'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title">Giá trị hợp đồng</div>
														<div class="value-info">
															<?php the_field('contract_value'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('construction_time'))) :?>
													<div class="group et_pb_animation_left et-waypoint et-animated">
														<div class="row-title">Thời gian thi công</div>
														<div class="value-info">
															<?php the_field('construction_time'); ?>
														</div>
													</div>
													<?php endif; ?>
													<?php if(!empty(get_field('content'))) :?>
													<div class="content et_pb_animation_left et-waypoint et-animated">
														<?php the_field('content'); ?>
													</div>
													<?php endif; ?>
												</div>
												<div class="column-9">
													<a href="<?php echo (get_field('bg_description')?get_field('bg_description'):( has_post_thumbnail() ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : wp_get_attachment_image_src(21872, 'full')[0])); ?>" class="et_pb_lightbox_image">
														<img src="<?php echo (get_field('bg_description')?get_field('bg_description'):( has_post_thumbnail() ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : wp_get_attachment_image_src(21872, 'full')[0])); ?>" class="et_pb_animation_right et-waypoint et-animated">
													</a>
												</div>
											</div>
											<div id="tab-dt-02">
												<?php if(!empty(get_field('image_project'))) { ?>
												<div id="image-project">
													<?php foreach(get_field('image_project') as $item) : ?>
													<div class="item-image">
														<a href="<?php echo wp_get_attachment_image_src( $item['ID'], 'full' )[0]; ?>" class="et_pb_lightbox_image">
															<img src="<?php echo wp_get_attachment_image_src( $item['ID'], 'small_crop' )[0]; ?>" />	
														</a>
													</div>
													<?php endforeach; ?>
												</div>
												<?php }else{ ?>
												<div style="padding-top: 15px;">
													<h3 style="color: #fff;font-size: 17px;">Chưa cập nhật nội dung...</h3>
												</div>
												<?php } ?>
											</div>
											<div id="tab-dt-03">
												<?php $image_360 = get_field('360_image');
												if ($image_360) {
													echo "<div class=\"image-360-container\" data-image=\"$image_360\"></div>";
												} else {											
													echo 'Đang cập nhật......';
												}
												?>
											</div>
										</div>
									</div>
									<!-- End row -->
								</div>
							</div>
						<?php endwhile; 
						else:
							echo '<div style="padding: 0 20px;">Không tìm thấy kết quả tìm kiếm</div>';
						endif;
						wp_reset_postdata();?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	(function($) {
		$(document).ready(function() {
			if (findGetParameter('tab')) {
				setTimeout(function() {
					$("li[slug="+findGetParameter('tab')+"]").click();
				}, 300);
				if (findGetParameter('projects')){
					setTimeout(function() {
						$('.project_'+findGetParameter('projects')+' > .content').click();
					}, 300);
				}
			}
		});
	})(jQuery)
	function findGetParameter(parameterName) {
		var result = null,
			tmp = [];
		var items = location.search.substr(1).split("&");
		for (var index = 0; index < items.length; index++) {
			tmp = items[index].split("=");
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		}
		return result;
	}
</script>
<?php get_footer(); ?>