<?php
global $wp_query;
$paged = get_query_var( "paged" ) ? get_query_var( "paged" ) : 1 ;
$numpages = $wp_query->max_num_pages;
$page_name = $paged = get_query_var( "page_name" ) ? get_query_var( "page_name" ) : '' ;

if (function_exists(custom_pagination)) {
custom_pagination($the_query->max_num_pages,"",$paged, $page_name);
}
 
?>
<!-- <div class="custom-pagination">
	<?php if ( $paged > 1 ) { ?>
		<?php previous_posts_link("<div class=\"page-prev\"><i class=\"fa fa-chevron-left\"></i></div>"); ?>
	<?php } ?>
	<div class="current-page"><?php echo $paged;?></div>
	<?php if ( $paged < $wp_query->max_num_pages ) { ?>
		<?php next_posts_link("<div class=\"page-next\"><i class=\"fa fa-chevron-right\"></i></div>"); ?>
	<?php } ?>
</div>  -->

