<div class="home_news">
	<?php
	$cat_id = 0;
	switch ( pll_current_language() ) {
		case 'vi':
			$cat_id = 66;
			break;
		case 'en';
			$cat_id = 68;
			break;
		case 'zh':
			$cat_id = 70;
			break;
		case 'ja':
			$cat_id = 72;
			break;
		};
	?>
	
	<?php
	$count = get_query_var('count_all_post');
	$is_nav = get_query_var('is_nav');
	$page = !empty( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	query_posts( array( 'post_type' => 'post',
						'post_status' => 'publish',
						'posts_per_page' => $count,
						'cat' => $cat_id,
						'paged' => $page
						)
				);
	$first = true;
	?>
	<?php while ( have_posts() ): the_post(); ?>
	<?php if ( $first ) : ?>
		<?php if ( $page == 1 ) : ?>
		<div class="big-item clearfix">
			<div class="et_pb_column et_pb_column_1_2 et_pb_animation_left et-waypoint">
				<a href="<?php the_permalink(); ?>">
					<div class="image-wrap">
						<?php the_post_thumbnail( 'home_news_image' ); ?>
					</div>
				</a>
			</div>
			<div class="et_pb_column et_pb_column_1_2 info et_pb_animation_right et-waypoint">
				<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
				<p class="date"><?php echo get_the_date( 'd/m/Y' ); ?></p>
				<?php the_excerpt(); ?>
				<p class="post-link"><a href="<?php the_permalink(); ?>"><?php pll_e('Đọc thêm');?></a></p>
			</div>
		</div>
		<?php endif; ?>
	<?php $first = false; ?>
	<div class="small-items clearfix et_pb_animation_bottom et-waypoint">
	<?php if ( $page == 1 ) { continue; } ?>
	<?php endif; ?>
    	<div class="et_pb_column et_pb_column_1_4">
	    	<a href="<?php the_permalink(); ?>">
	    		<div class="image-wrap">
	    			<?php the_post_thumbnail('small_crop'); ?>
    			</div>
				<div class="content-wrap" data-title="<?php echo get_the_title(); ?>">
					<h2><?php
					$title = get_the_title();
					if (strlen( $title ) > 60) {
						if (strpos($title, ' ') !== false)
							$title = substr( trim( $title ), 0, strrpos( trim( substr( $title, 0, 80 ) ), " " ) ) . "..." ;
						else
							$title = substr( $title, 0, 25 );
					}
					echo $title;
					?></h2>
					<?php the_excerpt(); ?>
				</div>
    		</a>
	    </div>
	<?php endwhile; ?>
    </div>
	<?php
	if ($is_nav) {
		$page = pll_get_post( 16200 );
		set_query_var('page_name', get_post_field( 'post_name', $page) );
		get_template_part( 'custom-template/custom-pagination' );
	}
	wp_reset_query();
	?>
</div>