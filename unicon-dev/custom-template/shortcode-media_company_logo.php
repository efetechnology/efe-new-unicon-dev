<?php $logo = get_post(inline_trans('23743', '23746')); ?>
<div class="et_pb_row et_pb_equal_columns media_company_logo media_tab">
    <div class="et_pb_column et_pb_column_1_2 media_item">
        <div class="content-group">
            <div class="featured_image">
                <a href="">
                    <div class="image-wrap et_pb_animation_left et-waypoint">
                        <?php echo get_the_post_thumbnail( $logo, 'medium' ); ?>
                    </div>
                </a>
            </div>
            <div class="item_info">
                <div class="item-title">
                    <h2><?php echo get_post_meta($logo->ID, 'title_custom')[0];?></h2>     
                </div>
                <div class="content">
                    <p><?php echo $logo->post_excerpt; ?></p>
                    <label><input type="radio" name="choose_logo_type">
                    <?php echo get_post_meta($logo->ID, 'JPEG')[0];?></label><br>
                    <label><input type="radio" name="choose_logo_type">
                    <?php echo get_post_meta($logo->ID, 'PNG')[0];?></label><br>
                    <label><input type="radio" name="choose_logo_type">
                    <?php echo get_post_meta($logo->ID, 'PSD')[0];?></label><br>
                    <label><input type="radio" name="choose_logo_type"><?php echo get_post_meta($logo->ID, 'AI')[0];?></label><br>
                </div>
            </div>
        </div>
        <div class="button-group">
            <a class="button" href="#"><?php echo inline_trans('Tải về', 'Dowload')?></a>
        </div>
    </div>
<?php $guide = get_post(inline_trans('23754', '23764')); ?>
    <div class="et_pb_column et_pb_column_1_2 media_item">
        <div class="content-group">
            <div class="featured_image">
                <a href="">
                    <div class="image-wrap et_pb_animation_left et-waypoint">
                        <?php echo get_the_post_thumbnail( $guide, 'medium' ); ?>
                    </div>
                </a>
            </div>
            <div class="item_info">
                <div class="item-title">
                    <h2><?php echo get_post_meta($guide->ID, 'title_custom')[0];?></h2>  
                </div>
                <div class="content">
                    <p><?php echo $guide->post_excerpt; ?></p>
                    <label><input type="radio" name="choose_instruction"><?php echo get_post_meta($guide->ID, 'Guild-vn')[0];?></label><br>
                    <label><input type="radio" name="choose_instruction"><?php echo get_post_meta($guide->ID, 'Guild-en')[0];?></label><br>
                    <label><input type="radio" name="choose_instruction"><?php echo get_post_meta($guide->ID, 'Font-roboto')[0];?></label><br>
                    <label><input type="radio" name="choose_instruction"><?php echo get_post_meta($guide->ID, 'Font-rutan')[0];?></label><br>
                </div>
            </div>
        </div>
        <div class="button-group">
            <a class="button" href="#"><?php echo inline_trans('Tải về', 'Dowload')?></a>
        </div>
    </div>
</div>