<?php $timeline = get_field('timeline');
if (!empty( $timeline) ){
?>
<div class="timeline et_pb_module et_pb_tabs custom_tab custom_tab_blue project-tab custom_tab_project same-align-tab et_pb_tabs_3 et_slide_transition_to_1 et_slide_transition_to_2 et_slide_transition_to_0" style="margin-top: 50px;">
	<ul class="et_pb_tabs_controls clearfix" style="height: 51px;" id="about_unicon">
		<?php $count = 3;
		foreach ( $timeline as $year) {
		?>
		<li class="et_pb_tabs_<?php echo $count; if ($count == 3) echo " et_pb_tab_active";?>"><a href="#"><?php echo $year['year']; ?></a></li>
		<?php
		$count++;
		} ?>
	</ul>
	<div class="et_pb_all_tabs">		
		<?php $count = 3;
		foreach ( $timeline as $year) {
			?>
			<div class="et_pb_tab clearfix et_pb_tabs_<?php echo $count; ?> <?php if($count==3){echo 'et-pb-active-slide et_pb_active_content' ;} ?>">
				<div class="content-80">
					<?php echo $year['year_content'];?>
				</div>
			</div>
		<?php
		$count++;
		}
		?>	
	</div>
</div>
<?php } ?>