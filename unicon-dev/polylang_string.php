<?php
pll_register_string( 'text Xem tất cả', 'Xem tất cả' );
pll_register_string( 'text Đọc thêm', 'Đọc thêm' );
pll_register_string( 'text Xem', 'Xem' );
pll_register_string( 'text Download', 'Tải về' );
pll_register_string( 'text Tin tức liên quan', 'Tin tức liên quan' );
pll_register_string( 'text Bản quyền', 'Bản quyền' );

/**
 * String project category
 */
pll_register_string( 'text Nhà ở & Thương mại', 'Nhà ở &amp; Thương mại', 'project_category' );
pll_register_string( 'text Công nghiệp', 'Công nghiệp', 'project_category' );
pll_register_string( 'text Khách sạn & khu nghỉ mát', 'Khách sạn &amp; khu nghỉ mát', 'project_category' );
pll_register_string( 'text Giáo dục', 'Giáo dục', 'project_category' );
pll_register_string( 'text Miền Bắc', 'Miền Bắc', 'project_category' );
pll_register_string( 'text Miền Trung', 'Miền Trung', 'project_category' );
pll_register_string( 'text Miền Nam', 'Miền Nam', 'project_category' );