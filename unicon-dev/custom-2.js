(function($) {
	$(document).ready(function() {
		var imgs = $('.et_pb_code.et_pb_module.logo_expand').find('ul').find('li').find('img');
		var this_;
		imgs.each(function() {
			var this_ = $(this);
			$('<div class="image-wrap"></div>').prependTo(this_.parent()).append(this_);
		});

		var imgs_project = $('.et_pb_section.client_project_content').find('a').find('img');
		imgs_project.each(function() {
			var this_ = $(this);
			$('<div class="image-wrap"></div>').prependTo(this_.parent()).append(this_);
		});

		$('.logo_expand li a').click(function() {
			var this_ = $(this);
			var element = this_.next().next();
			var height_li = $(this).parent().height();
			if (!this_.parent().hasClass('og-expanded')) {
				var prevNowPlaying = setInterval(function() {
					var element = this_.next().next();
					if (element.hasClass('og-expander')) {
						clearInterval(prevNowPlaying);
						$(".client_project_content").clone().appendTo(this_.next().next().children());
						$('.logo_expand .og-expander').css('height', 'auto');
						var autoHeight = $('.logo_expand .og-expander').height();
						$('.logo_expand .og-expander').animate({
							'height': autoHeight + "px"
						}, 600);
						var height = autoHeight + height_li;
						$('.logo_expand .og-expander').parent().css("height", height);
					}
				}, 1000);
			}
		});

		/****** Add by Rain *****/
		// Click Tab to change background color
		$('ul.et_pb_tabs_controls li').click(function() {
			var class_click_tab = $(this).attr('class');
			var bg_color_content_tab = $('div.' + class_click_tab).css("background-color");
			$(this).closest('.project-detail-tab').attr('style', 'background-color:' + bg_color_content_tab + ' !important');
		});

		// Career handbook click question
		$('#career-handbook .question_answer .sub').click(function() {
			$p = $(this);
			if ($p.next().next().is(":visible")) {
				$p.next().next().slideUp();
				$p.css('background', '#333333');
				$p.text('+');
			} else {
				$p.next().next().slideDown();
				$p.css('background', '#e4302c');
				$p.text('-');
			}
		});
		// Our client and partners
		$('.wrap-our-clients  #og-grid.og-grid').slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 1,
			arrows: true
		});

		// Why unicons people unicons tab
		/*
		$('#why-unicons-people-tab .wrap').slick({
			dots: true,
			slidesPerRow: 3,
			rows: 3,
			responsive: [{
					breakpoint: 1024,
					settings: {
						slidesPerRow: 2,
						rows: 3,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesPerRow: 2,
						rows: 3
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesPerRow: 1,
						rows: 3
					}
				}
			]
		});
		var with_pagnation = $('#why-unicons-people-tab ul.slick-dots').width();
		var with_right_pagination = with_pagnation + 60;
		$('#why-unicons-people-tab button.slick-prev').css('right', with_right_pagination + 'px');
		*/
		
		// Our clients and partners
		$('.out-clients-wrap .slider-logo').slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			arrows: true,
			responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 5
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
					}
				}
			]
		});
		$('.out-clients-wrap .slider-logo .item').click(function() {
			var width = $(this).width();
			var left_position = $(this).offset().left;
			left_position = left_position + width / 2;
			var id_click = $(this).attr('item_id');
			$('.out-clients-wrap .slider-logo .item').css('border', '1px solid #ccc');
			$(this).css('border', '1px solid #e43430');
			var $this = $(".out-clients-wrap .detail .item[item_id='" + id_click + "']");
			var space = $(this).offset().top;
			$("html, body").animate({
				scrollTop: space - 60
			}, 1000);
			$(".out-clients-wrap .detail .item").not($this).slideUp();
			var parent_item = $(this).parent().closest('article');
			$(parent_item).find('.arrow').css('left', left_position + 'px');
			$(parent_item).find(".detail .item[item_id='" + id_click + "']").slideToggle();
		});

		// Career position
		$('.career-position-wrap .wrap-item .column-3 img').click(function() {
			var width = $(this).width();
			var parent_ele = $(this).parent();
			var detail = parent_ele.find('div.detail');
			$('.career-position-wrap div.detail').not(detail).slideUp();
			$('.career-position-wrap item').css('margin-bottom', 0 + 'px');
			var left_position = $(this).offset().left;
			left_position = left_position + width / 2;
			$('.career-position-wrap .wrap-item .arrow').hide();
			var space = $(this).offset().top;
			$("html, body").animate({
				scrollTop: space + 125
			}, 1000);
			$(parent_ele).find('.arrow').show();
			$(parent_ele).find('.arrow').css('left', left_position + 'px');
			if (detail.is(":visible")) {
				parent_ele.css('margin-bottom', 0 + 'px');
				detail.slideUp();
			} else {
				parent_ele.css('margin-bottom', detail.height() + 'px');
				detail.slideDown();
			}
		});
		$('.career-position-wrap .wrap-item .close').click(function() {
			$('.career-position-wrap .wrap-item .detail').slideUp();
			$('.career-position-wrap .wrap-item item').css('margin-bottom', 0 + 'px');
			$('.career-position-wrap .wrap-item .arrow').hide();
		});

		// Project page
		$("#wrap-page #tab").tabpager({
			items: 9,
			previous: '<',
			next: '>',
			position: 'bottom',
			contents: 'contents'
		});
		$('.project-wrap .group-detail .tab-detail ul li').click(function() {
			var parent_element = $(this).parent();
			parent_element.find('li').removeClass('active');
			$(this).addClass('active');
			var id = $(this).attr('id');
			var project_elem = $(this).parents(".item").eq(0);
			var content_wrap = $(this).parents('.item').eq(0).find('.tab-detail-content').eq(0);
			content_wrap.children().hide(1000);
			content_wrap.children("[id="+id+"]").show(1000, function() {
				var album = $(this).find('[id=image-project]');
				if (album.length !== 0 &&! album.eq(0).hasClass("slick-initialized")) {
					album.eq(0).slick({
						slidesPerRow: 3,
						rows: 3,
						infinite: false,
						responsive: [
							{
								breakpoint: 992,
								settings: {
									slidesPerRow: 2,
								}
							}
						]
					});
				}
				setTimeout(function() {
					project_elem.animate({"padding-bottom": project_elem.children(".group-detail").height() + 20 + "px"}, 300);
				}, 300);
			});
			
		});
		$('.project-wrap .group-detail .close').click(function() {
			var space = $(this).parent().closest('.item').find('.content').offset().top;
			$("html, body").animate({
				scrollTop: space - 240
			}, 1000);
			var element = $(this).parent().closest('.group-detail').hide(1000);
			$('.project-wrap .content-tab .item').css('padding-bottom', 0 + 'px');
			$(this).parent().closest('.item').css('clear', '');
			$(this).parent().closest('.item').css('padding-bottom', 0 + 'px');
			$('.project-wrap .arrow').hide();
		});
		$('.project-wrap .content-tab .item > .content').click(function() {
			var _parent = $(this).parent();
			_parent.parent().children().css("clear", "");
			var id = _parent.index();
			var newline = -1;
			if ($(window).width() > 991) {
				newline = id - id % 3 + 3;
			}
			else if ($(window).width() > 767 && $(window).width() < 992 ) {
				newline = id - id % 2 + 2 + Math.floor( id / 9) % 2;
			}
			else {
				newline = id + 1;
			}
			console.log( newline );
			_parent.parent().children().css("clear", "");
			_parent.parent().children().css("padding-bottom", "");
			
			$('.project-wrap .arrow').hide();
			$('.project-wrap .group-detail').hide();
			var space = $(this).offset().top;
			$("html, body").animate({
				scrollTop: space + 125
			}, 1000);
			_parent.find('.group-detail').slideDown("slow", function(){	
				var height_content_detail = _parent.find('.group-detail').height();
				_parent.parent().children().eq(newline).css("clear", "both");
				_parent.css('padding-bottom',   height_content_detail + 20 + 'px');
			});
			_parent.find('.arrow').show();
		});



	});
})(jQuery)