<?php get_header(); ?>

<div id="main-content">
	<?php if ( is_search() ): ?>
	<div class="et_pb_section overlay overlay-white et_pb_section_0 et_pb_with_background et_section_regular" style="background-image: url(http://unicons.efe.com.vn/wp-content/uploads/2017/03/Vinhomes-Thang-Long.jpg); background-size: cover; ">
		<div class=" et_pb_row et_pb_row_0">
			<div class="et_pb_column et_pb_column_4_4">
				<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_0">
					<h1 style="color: #195595;"> Search for: <?php echo isset($_GET['s'])?$_GET['s']:""; ?></h1>
	
				</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
		</div> <!-- .et_pb_row -->	
	</div>
	<?php endif; ?>
	<div class="container">
		<div id="content-area" class="clearfix<?php if ( is_search() ) echo " et_pb_custom_blog_" ;?>">
			<?php if ( !is_search() ): ?>
			<div id="left-area">
			<?php endif; ?>
		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format();
					if ( is_search() ) :
					$count = get_post_meta( get_the_ID(), "post_views_count", true);
					?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post clearfix' . $no_thumb_class . $overlay_class ); ?>>
						<?php if ( has_post_thumbnail() ) : ?>
						<div class="custom_blog_image_container">
							<a href="<?php esc_url( the_permalink() ); ?>" class="entry-featured-image-url">
								<?php the_post_thumbnail('home_news_image');?>
							</a>
						</div> <!-- .et_pb_image_container -->
						<?php endif; ?>
						<div class="custom_blog_post_info">
							<div class="post-text">
							<h2 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h2>
							<?php
					printf( '<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s</p>',
							et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' .  et_pb_get_the_author_posts_link() . '</span>' ) ),
							' | ',
							et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( 'd/m/Y' ) ) . '</span>' ) ),
							' | ',
							get_the_category_list(', '),
							
							" | " . ( empty( $count ) ? "0 views" : $count . " views")
					);
		
				echo '';
				global $et_pb_rendering_column_content;
				
				$et_pb_rendering_column_content = true;
				if ( has_excerpt() ) {
					the_excerpt();
				}
		
				$et_pb_rendering_column_content = false;
				?>
							</div>
							<div class="post-link">
							<?php
					$more = sprintf( ' <a href="%1$s" class="more-link" >%2$s</a>' , esc_url( get_permalink() ), esc_html__( 'read more', 'et_builder' ) );
					echo $more;
				?>
							</div>
						</div>
					</article>
					<?php else: ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>

				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_pb_post_main_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					et_divi_post_format_content();

					if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
						if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
							printf(
								'<div class="et_main_video_container">
									%1$s
								</div>',
								$first_video
							);
						elseif ( ! in_array( $post_format, array( 'gallery' ) ) && 'on' === et_get_option( 'divi_thumbnails_index', 'on' ) && '' !== $thumb ) : ?>
							<a href="<?php the_permalink(); ?>">
								<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
							</a>
					<?php
						elseif ( 'gallery' === $post_format ) :
							et_pb_gallery_images();
						endif;
					} ?>

				<?php if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) : ?>
					<?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) : ?>
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php endif; ?>

					<?php
						et_divi_post_meta();

						the_excerpt();
					?>
				<?php endif; ?>
				<?php endif; ?>
					</article> <!-- .et_pb_post -->
			<?php
					endwhile;

					get_template_part( 'custom-template/custom-pagination');
					//if ( function_exists( 'wp_pagenavi' ) )
					//	wp_pagenavi();
					//else
					//	get_template_part( 'includes/navigation', 'index' );
				else :
					get_template_part( 'includes/no-results', 'index' );
				endif;
			?>
			<?php if ( !is_search() ): ?>
			</div> <!-- #left-area -->
			<?php endif;?>

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>